package com.example.lenovo.asia5b.util;


import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.TextAppearanceSpan;

import com.wushi.lenovo.asia5b.R;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;

public class StringUtil {
	public static void main(String[] args) {
		//http://121.8.157.138:9080/iYH/subjectextrafield/subjectextrafieldMobile.do?mAction=add&fieldName=communication&
		
		//subjectId=7f74d83fc0a800f554aa54aa0265f1fe&fieldValue=89af0aa5b6dab37b  zhangjingzhou 15622205671
		//subjectId=b146b6b5c0a800f505d405d47b875dd6&fieldValue=92487a4c78fec442  wangzige
		//subjectId=ac37d4edc0a800f5051e051e50d8d1b6&fieldValue=1560ba3575b114db  songzhongqiu
		//subjectId=84e577a6c0a800f52fb22fb2f18bbf49&fieldValue=2d0785b04fe6340f  suwenhui 15625407671
		//subjectId=d21fdf54c0a800f533363336b8b22478&fieldValue=86bdd17bc3900773  yangkefu
		
		//subjectId=b3b406b1c0a800f5764e764e0979f7e5&fieldValue=8f76b2240bc76a  likaifeng
		//subjectId=7bfb1ab0c0a800f533b333b3193d4b1e&fieldValue=e00db6b67b7faf0b  linzhaozhan
		//subjectId=cc86a2a6c0a800f515451545fdb47ec7&fieldValue=253709e79aa50a61  guoyongxian
		//subjectId=a88a61d9c0a800f5168c168cc9061482&fieldValue=6e928e76da80b02e  chijialong
		//subjectId=7f870612c0a800f554aa54aa38955f46&fieldValue=24b158afa7d7fee2  liurui

		//subjectId=813036e1c0a800f52fb22fb27bfea5bb&fieldValue=7364210f2ce4cc43  zhubing
		//subjectId=d0160ea7c0a800f54bb84bb8210884c8&fieldValue=85c4a1e60774474  panweihua
		//subjectId=cfd47bb3c0a800f539573957608676d7&fieldValue=714a8413f664dd86  zhanghanwei
		//subjectId=80f65545c0a800f52fb22fb21d26142a&fieldValue=b437ff51888a667f  weiyongzhi eeaff12f29e3178
		System.out.println(getCRC32UserID("15018784301"));
	}
    public static String getCRC32UserID(String userId){
    	int mid = userId.length() / 2;
    	String result = "";
	try {
    	CRC32 crc32 = new CRC32();
    	crc32.update(userId.substring(0, mid).getBytes());
    	System.out.println(userId.substring(0, mid));
    	result += java.lang.Long.toHexString(crc32.getValue());
    	crc32.reset();
    	crc32.update(userId.substring(mid,userId.length()).getBytes());
    	System.out.println(userId.substring(mid,userId.length()));
    	result += java.lang.Long.toHexString(crc32.getValue());
		return result;
	} catch (Exception e) {
		return result;
	}
    }

	/**
	 * @param str
	 * @return 传入价格返回格式化后的价格（前面价钱字符小，后面文字大）
	 */
	public static SpannableString formatString(Context context, String str) {
		SpannableString laterStr ;
		String nowPriceTxt = String.format(context.getResources().getString(R.string.index_item_price), str);
		SpannableString ssNowPriceTxt = new SpannableString(nowPriceTxt);
		ssNowPriceTxt.setSpan(new TextAppearanceSpan(context, R.style.text_ff3c00_10), 0, 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
//		ssNowPriceTxt.setSpan(new TextAppearanceSpan(context, R.style.text_ff3c00_18), 1, ssNowPriceTxt.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		laterStr = ssNowPriceTxt;
		return laterStr;
	}

	/**
	 * 保留一位
	 * @param dou
	 * @return
	 */
	public static double changeDouble(Double dou){
		NumberFormat nf=new DecimalFormat( "0.0 ");
		dou = Double.parseDouble(nf.format(dou));
		return dou;
	}

	/**
	 * * 验证手机号码
     */
	public static  boolean checkMobileNumber(String mobileNumber) {
		boolean flag = false;
		try {
			Pattern regex = Pattern
					.compile("^((13[0-9])|(15[^4,\\D])|(18[0,2,3,5-9]))\\d{8}$");
			Matcher matcher = regex.matcher(mobileNumber);
			flag = matcher.matches();
		} catch (Exception e) {
			flag = false;
		}
		return flag;
	}

	// 判断是否Email
	public static  boolean checkEmail(String email) {
		String str = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
		Pattern p = Pattern.compile(str);
		Matcher m = p.matcher(email);

		return m.matches();
	}

	/**
	 * 根据类型ID返回类型
	 * @param context
	 * @param process_type
	 * @return
	 */
	public static String walletType( Context context,String process_type){
		String statu = "";
		if(TextUtils.equals("0",process_type)){
			statu = context.getResources().getString(R.string.cz);
		}else if(TextUtils.equals("1",process_type)){
			statu = context.getResources().getString(R.string.access_account);
		}else if(TextUtils.equals("2",process_type)){
			statu = context.getResources().getString(R.string.predelivery);
		}else if(TextUtils.equals("3",process_type)){
			statu = context.getResources().getString(R.string.hbfh);
		}else if(TextUtils.equals("4",process_type)){
			statu = context.getResources().getString(R.string.xdzf);
		}else if(TextUtils.equals("5",process_type)){
			statu = context.getResources().getString(R.string.th_2);
		}else if(TextUtils.equals("6",process_type)){
			statu = context.getResources().getString(R.string.qhqx);
		}else if(TextUtils.equals("7",process_type)){
			statu = context.getResources().getString(R.string.zdkk);
		}else if(TextUtils.equals("8",process_type)){
			statu = context.getResources().getString(R.string.bcj);
		}else if(TextUtils.equals("9",process_type)){
			statu = context.getResources().getString(R.string.qxdg);
		}else if(TextUtils.equals("10",process_type)){
			statu = context.getResources().getString(R.string.nghqx);
		}else if(TextUtils.equals("11",process_type)){
			statu = context.getResources().getString(R.string.bcj);
		}else if(TextUtils.equals("12",process_type)){
			statu = context.getResources().getString(R.string.zjzhye);
		}else if(TextUtils.equals("13",process_type)){
			statu = context.getResources().getString(R.string.jlzj);
		}else if(TextUtils.equals("14",process_type)){
			statu = context.getResources().getString(R.string.fxzj);
		}else if(TextUtils.equals("15",process_type)){
			statu = context.getResources().getString(R.string.ystq);
		}
		return statu;
	}


	/**
	 * 根据类型ID返回类型
	 * @param context
	 * @param process_type
	 * @return
	 */
	public static String accountDetailsBillType( Context context,String process_type){
		String statu = "";
		if(TextUtils.equals("0",process_type)){
			statu = context.getResources().getString(R.string.receipt);
		}else if(TextUtils.equals("1",process_type)){
			statu = context.getResources().getString(R.string.zzpz);
		}else if(TextUtils.equals("2",process_type)){
			statu = "";
		}else if(TextUtils.equals("3",process_type)){
			statu = "";
		}else if(TextUtils.equals("4",process_type)){
			statu = context.getResources().getString(R.string.fp);
		}else if(TextUtils.equals("5",process_type)||TextUtils.equals("6",process_type)||TextUtils.equals("9",process_type)
				||TextUtils.equals("10",process_type)||TextUtils.equals("12",process_type)){
			statu = context.getResources().getString(R.string.djd);
		}else if(TextUtils.equals("7",process_type)||TextUtils.equals("8",process_type)||TextUtils.equals("11",process_type)){
			statu = context.getResources().getString(R.string.jjd);
		}
		return statu;
	}

	/**
	 * 包裹状态
	 * @param statuid
	 * @param fillStatu
	 * @param cancle_type
	 * @return
	 */
	public static String orderStatu(Context context,String statuid,String fillStatu,String cancle_type,String order_status,String express_code){
		String statu = null;
		if(TextUtils.equals("0",statuid)){
			if (order_status.equals("6")) {
				statu = context.getResources().getString(R.string.ygq);
			} else {
				statu = context.getResources().getString(R.string.ds);
			}
		}else if(TextUtils.equals("1",statuid)){
			statu = context.getResources().getString(R.string.ysh);
		}else if(TextUtils.equals("2",statuid)){
			statu = context.getResources().getString(R.string.yfk);
		}else if(TextUtils.equals("3",statuid)){
			statu = context.getResources().getString(R.string.yph);
		}else if(TextUtils.equals("4",statuid)){
			statu = context.getResources().getString(R.string.fh);
		}else if(TextUtils.equals("5",statuid)){
			if (!TextUtils.isEmpty(express_code)){
				statu = context.getResources().getString(R.string.sjyph);
			} else {
				statu = context.getResources().getString(R.string.ph);
			}
		}else if(TextUtils.equals("6",statuid)){
			statu = context.getResources().getString(R.string.dc);
		}else if(TextUtils.equals("7",statuid)){
			if (!fillStatu.equals("1")&&cancle_type.equals("0")){
				statu = context.getResources().getString(R.string.qhqx);
			} else if (fillStatu.equals("1")){
				statu = context.getResources().getString(R.string.qxdg);
			} else if(cancle_type.equals("1")) {
				statu = context.getResources().getString(R.string.qhqx);
			}
		}else if(TextUtils.equals("8",statuid)){
			statu = context.getResources().getString(R.string.qs);
		}else if(TextUtils.equals("9",statuid)){
			statu = context.getResources().getString(R.string.th);
		}else if(TextUtils.equals("10",statuid)){
			statu = context.getResources().getString(R.string.ywc);
		}else if(TextUtils.equals("11",statuid)){
			statu = context.getResources().getString(R.string.qh);
		}else if(TextUtils.equals("12",statuid)){
			if (!TextUtils.isEmpty(express_code)) {
				statu = context.getResources().getString(R.string.sjyph);
			}else{
				statu = context.getResources().getString(R.string.ph);
			}
		}else if(TextUtils.equals("13",statuid)){
			statu = context.getResources().getString(R.string.bcj);
		}else if(TextUtils.equals("14",statuid)){
			statu = context.getResources().getString(R.string.bcj);
		}else if(TextUtils.equals("15",statuid)){
			statu = context.getResources().getString(R.string.ywc);
		}else if(TextUtils.equals("16",statuid)){
			statu =context.getResources().getString(R.string.ngqx);
		}else if(TextUtils.equals("17",statuid)){
			statu = context.getResources().getString(R.string.ywc);
		}else if(TextUtils.equals("18",statuid)){
			statu = context.getResources().getString(R.string.cxdg);
		}
		return statu;
	}

	/**
	 * 将手机后面8位数设为星星
	 */
	public static String getMobilePhone(String phone) {
		if (TextUtils.isEmpty(phone)){
			phone = "";
		}
		try {
			String mobile_phone = phone;
			String mobile_phone1 = "";
			String mobile_phone2 = "";
			if (!TextUtils.isEmpty(mobile_phone)) {
				if (mobile_phone.length() > 3) {
					mobile_phone1 = phone.substring(0, 3);
					if (phone.length() >7) {
						mobile_phone = mobile_phone.substring(3, 7);
						mobile_phone2 = phone.substring(7, phone.length());
					} else {
						mobile_phone = mobile_phone.substring(3, mobile_phone.length());
					}
					String xing = "";
					for (int i = 0; i < mobile_phone.length(); i++) {
						xing = xing + "*";
					}
					phone = mobile_phone1 + xing +mobile_phone2;
				}
			}
		} catch (Exception e) {
			return "";
		}
		return phone;
	}

	//判断是否纯数字
	public static boolean isNumeric(String str){
		for (int i = 0; i < str.length(); i++){
			if (!Character.isDigit(str.charAt(i))){
				return false;
			}
		}
		return true;
	}

	/**
	 * 根据生日判断 星座
	 * @return
	 */
	public static  String constell(Context context,String birthday){
		String constell = "";
		String[] items = context.getResources().getStringArray(R.array.constellation);
		String[] aa = birthday.split("-");
		int month = Integer.parseInt(aa[1]);
		int day  = Integer.parseInt(aa[2]);
		if (month == 1) {
			if (day >=20){
				constell = items[11];
			} else {
				constell = items[10];
			}
		}else if (month == 2){
			if (day >=19){
				constell = items[12];
			} else {
				constell = items[11];
			}
		}else if (month == 3){
			if (day >=21){
				constell = items[1];
			} else {
				constell = items[12];
			}
		}else if (month == 4){
			if (day >=20){
				constell = items[2];
			} else {
				constell = items[1];
			}
		}else if (month == 5){
			if (day >=21){
				constell = items[3];
			} else {
				constell = items[2];
			}
		}else if (month == 6){
			if (day >=22){
				constell = items[4];
			} else {
				constell = items[3];
			}
		}else if (month == 7){
			if (day >=23){
				constell = items[5];
			} else {
				constell = items[4];
			}
		}else if (month == 8){
			if (day >=23){
				constell = items[6];
			} else {
				constell = items[5];
			}
		}else if (month == 9){
			if (day >=23){
				constell = items[7];
			} else {
				constell = items[6];
			}
		}else if (month == 10){
			if (day >=24){
				constell = items[8];
			} else {
				constell = items[7];
			}
		}else if (month == 11){
			if (day >=23){
				constell = items[9];
			} else {
				constell = items[8];
			}
		}else if (month == 12){
			if (day >=22){
				constell = items[10];
			} else {
				constell = items[9];
			}
		}
		return constell;
	}

}
