package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.lenovo.asia5b.commodity.activity.ClassifyMainSonActivity;
import com.example.lenovo.asia5b.commodity.bean.ClassifyThreeBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.MyGridView;
import com.wushi.lenovo.asia5b.R;

import java.util.List;

/**
 * Created by lenovo on 2017/12/8.
 */

public class ClassifyRVRightAdapter extends RecyclerView.Adapter<ClassifyRVRightAdapter.MyViewHolder>{
//    private MyGridView my_gridview;
    private List<ClassifyThreeBean> mDatas;
    private int two_position = 0;
    private int index;

//    private List
    private Context mContext;
    private LayoutInflater inflater;

    public ClassifyRVRightAdapter(Context context, List<ClassifyThreeBean> datas) {
        this.mContext = context;
        this.mDatas = datas;

        inflater = LayoutInflater.from(mContext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.item_classify_main_two, parent, false);

        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txt_two_tit.setText(mDatas.get(position).getCat_name());

//        holder.my_gridview.setAdapter(new GridViewAdapter());
//        holder.my_gridview.setNumColumns(2);
////                my_gridview.setTag(i);
//        holder.my_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
//                Log.e("aaaaaaaaaaaa","position"+position+""+"-l-"+l+"");
//                Intent intent = new Intent(mContext, ClassifyMainSonActivity.class);
//                 intent.putExtra("catid",mDatas.get(0).childs.get(position).cat_id);
//                intent.putExtra("catname",mDatas.get(0).childs.get(position).cat_name);
//                mContext.startActivity(intent);
//            }
//        });

    }


    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txt_two_tit;
        MyGridView my_gridview;
        MyGridView image_commodity;
        TextView txt_name;

        public MyViewHolder(View view) {
            super(view);
            txt_two_tit = (TextView) view.findViewById(R.id.txt_two_tit);//标题
            my_gridview = (MyGridView) view.findViewById(R.id.my_gridview);

            my_gridview.setAdapter(new GridViewAdapter());
            my_gridview.setNumColumns(2);
//                my_gridview.setTag(i);
            my_gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                    Log.e("aaaaaaaaaaaa","position"+position+""+"-l-"+l+"");
                    Intent intent = new Intent(mContext, ClassifyMainSonActivity.class);
                    intent.putExtra("catid",mDatas.get(position).getCat_id());
                    intent.putExtra("catname",mDatas.get(position).getCat_name());
                    mContext.startActivity(intent);
                }
            });
        }
    }

    class GridViewAdapter extends android.widget.BaseAdapter {

        @Override
        public int getCount() {
//            return mDatas.get(two_position).childs.size();
            return mDatas.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            ViewHolder mHolder;
            if (view == null) {
                mHolder = new ViewHolder();
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.item_hot_sale, viewGroup, false);
                mHolder.image_commodity = (ImageView) view.findViewById(R.id.image_commodity);
                mHolder.txt_name = (TextView) view.findViewById(R.id.txt_name);
                view.setTag(mHolder);
            } else {
                mHolder = (ViewHolder) view.getTag();
            }
            if (mDatas != null) {

//                DownloadPicture.loadNetwork(mDatas.get(two_position).childs.get(position).fileimg, mHolder.image_commodity);
//                mHolder.txt_name.setText(mDatas.get(two_position).childs.get(position).cat_name);
                DownloadPicture.loadNetwork(mDatas.get(position).getFileimg(), mHolder.image_commodity);
                mHolder.txt_name.setText(mDatas.get(position).getCat_name());
            }
            return view;
        }

        private class ViewHolder {
            private ImageView image_commodity;
            private TextView txt_name;
        }
    }

}
