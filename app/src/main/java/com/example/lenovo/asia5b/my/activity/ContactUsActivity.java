package com.example.lenovo.asia5b.my.activity;

import android.os.Bundle;
import android.view.View;

import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/9/21.
 */

public class ContactUsActivity extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        initView();
    }


    public void initView(){
        F.id(R.id.public_title_name).text(getResources().getString(R.string.lxwm));
        F.id(public_btn_left).clicked(this);
        F.id(R.id.ll_service_about).clicked(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;
        }
    }
}
