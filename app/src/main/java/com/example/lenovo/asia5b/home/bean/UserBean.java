package com.example.lenovo.asia5b.home.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/7/7.
 */

public class UserBean implements Serializable{
    public String user_id="";
    public String user_name="";
    public String mobile_phone="";
    public String email="";
    public String alias="";
    public String avatar="";
    public String nickname="";
    public String user_money="";
    public String pay_points="";
    public String paypass="";
    public String is_uname = "";
}
