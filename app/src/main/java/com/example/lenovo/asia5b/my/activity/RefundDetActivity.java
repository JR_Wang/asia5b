package com.example.lenovo.asia5b.my.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import com.example.lenovo.asia5b.my.bean.RefundBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/8/21.
 */

public class RefundDetActivity  extends BaseActivity {
    private RefundBean bean;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_refund_det);
        initView();
    }

    public void initView() {
        F.id(R.id.public_title_name).text(getResources().getString(R.string.thjl));
        F.id(public_btn_left).clicked(this);
        if (getIntent().hasExtra("id")) {
            bean = (RefundBean)getIntent().getSerializableExtra("id");
            F.id(R.id.txt_amount_tit).text("RM"+bean.msg_price);
            String orderStatu = "";
            if(bean.orderStatu.equals("9")) {
                orderStatu = getResources().getString(R.string.ds);
            } else if (bean.orderStatu.equals("15")) {
                orderStatu = getResources().getString(R.string.ytg);
            } else if (bean.orderStatu.equals("17")){
                orderStatu = getResources().getString(R.string.btg);
            }
            F.id(R.id.txt_process_type).text(orderStatu);
            F.id(R.id.txt_order_sn).text(bean.order_sn);
            F.id(R.id.txt_rec_id).text(bean.rec_id);
            F.id(R.id.txt_msg_time).text(DateUtils.timedate(bean.msg_time));
            F.id(R.id.txt_msg_title).text(bean.msg_title);
            String pack_fee = "";
            if (bean.pack_fee.equals("1")) {
                pack_fee = getResources().getString(R.string.ymbx);
            } else{
                pack_fee = getResources().getString(R.string.mmbx);
            }
            F.id(R.id.txt_pack_fee).text(pack_fee);
            if (!TextUtils.isEmpty(bean.content_apply)) {
                F.id(R.id.txt_content_apply).text(bean.content_apply);
            } else {
                F.id(R.id.txt_content_apply).text(getResources().getString(R.string.mybz));
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;
        }
    }
}
