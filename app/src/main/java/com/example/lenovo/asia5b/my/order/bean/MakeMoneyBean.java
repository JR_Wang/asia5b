package com.example.lenovo.asia5b.my.order.bean;

/**
 * Created by lenovo on 2017/7/27.
 */

public class MakeMoneyBean {
    public String rec_id;
    public String order_id;
    public String goods_name;
    public String goods_attr_thumb;
    public String goods_number;
    public String orderStatu;
    public String price_difference;
    public String is_give_difference;
    public String cn_freight;
    public String cn_freight_status;
}
