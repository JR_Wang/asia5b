package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wushi.lenovo.asia5b.R;

import org.apache.http.util.EncodingUtils;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * 信用卡支付界面
 * Created by lenovo on 2017/7/24.
 */

public class CreditCardUrlActivity  extends BaseActivity {
    private WebView webView;
    private String html;
    private String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_url);
        initView();
    }

    public void initView() {
        if (getIntent().hasExtra("html")){
            html = getIntent().getStringExtra("html");
//            html = "merID=5501415283&PAN=5203821214877576&expiryDate=0819&CVV2=403&invoiceNo=TUYEUXO1504319711726&amount=000000000010&secretCode=AS1A5BP01&securityMethod=SHA1&securityKeyReq=JVmnpkm7y9N7+GTqU+b59q7C+gc=&postURL=https://asia5b.com/api/user.php?act=creditPayBack&aid=1971&back=0&secretString=&bankName=&bankCountry=";
        }
        if (getIntent().hasExtra("url")){
            url = getIntent().getStringExtra("url");
        }
        F.id(public_btn_left).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.my_top_up));
        webView = (WebView)findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);//支持javaScript
        webSettings.setDefaultTextEncodingName("utf-8");//设置网页默认编码
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webView.postUrl(url, EncodingUtils.getBytes(html,"BASE64"));
//        webView.setWebChromeClient(new MyWebChromeClient());// 设置浏览器可弹窗
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
//        html = URLEncoder.encode(html,"utf-8");
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                Log.e("========获取当前网址========",url);
                view.loadUrl(url);
//                if (url.contains("FPXPayResult")) {
//                    Intent intent = new Intent();
//                    intent.putExtra("pay_id","pay_id");
//                    CreditCardUrlActivity.this.setResult(0, intent);
//                    finish();
//                }
                return true;
            }
            @Override
            public void onPageStarted(WebView view, String url,
                                      Bitmap favicon) {
                //支付完成后,点返回关闭界面
                if(url.contains("creditPayBack")){
                    Intent intent = new Intent();
                    intent.putExtra("creditCard","creditCard");
                    CreditCardUrlActivity.this.setResult(0, intent);
                    finish();
                } else if (url.contains("FPXPayResult")) {
                    Intent intent = new Intent();
                    intent.putExtra("pay_id","pay_id");
                    CreditCardUrlActivity.this.setResult(0, intent);
                    finish();
                }
                super.onPageStarted(view, url, favicon);
            }


            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);

            }

        });

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;
        }
    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断用户是否点击的是返回键
        if((keyCode == KeyEvent.KEYCODE_BACK) && webView.canGoBack()){
            webView.goBack();
            return  true;
        } else {
            finish();
        }
        return super.onKeyDown(keyCode, event);
    }
}
