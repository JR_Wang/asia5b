package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovo.asia5b.commodity.bean.AllEvaluateDetBean;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;

import com.example.lenovo.asia5b.ui.CircleImageView;
import com.example.lenovo.asia5b.ui.DownloadPicture;

/**
 * Created by lenovo on 2017/7/1.
 */

public class AllEvaluateDetAdapter  extends BaseAdapter

{

    private LayoutInflater mInflater;
    private int cur_pos = 0;// 当前显示的一行
    private Context context;
    private List<AllEvaluateDetBean> lists;


    public AllEvaluateDetAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<AllEvaluateDetBean>();
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return arg0;
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_evaluate_det, null);
            holder.txt_content = (TextView)convertView.findViewById(R.id.txt_content);
            holder.image_head = (CircleImageView)convertView.findViewById(R.id.image_head);
            holder.txt_name = (TextView)convertView.findViewById(R.id.txt_name) ;
            holder.txt_goods_attr = (TextView)convertView.findViewById(R.id.txt_goods_attr) ;
            holder.txt_value = (TextView)convertView.findViewById(R.id.txt_value) ;
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        AllEvaluateDetBean bean = lists.get(position);
        holder.txt_content.setText(bean.content);
        DownloadPicture.loadHearNetwork(bean.avatar,holder.image_head);
        holder.txt_name.setText(bean.user_name);
        holder.txt_goods_attr.setText(bean.add_time);
        int id_value = Integer.parseInt(bean.id_value);
        if (id_value == 0) {
            holder.txt_value.setText(context.getResources().getString(R.string.like));
        } else if (id_value > 999) {
            holder.txt_value.setText("999");
        } else {
            holder.txt_value.setText(bean.id_value);
        }
//        holder.txt_value.setOnClickListener(new ValueOnClick(position));
        return convertView;
    }

    private class ViewHolder {
        TextView txt_content;
        CircleImageView image_head;
        TextView txt_name,txt_goods_attr,txt_value;
    }
    public void addAll(List<AllEvaluateDetBean> lisets){
        this.lists = lisets;
    }
    public void setLists(List<AllEvaluateDetBean> lists) {
        this.lists = lists;
    }

    public List<AllEvaluateDetBean> getLists() {
        return lists;
    }
}
