package com.example.lenovo.asia5b.my.order.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.my.order.bean.LogisticsBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2017/7/22.
 */

public class TimeLineAdapter extends BaseAdapter

    {
        private Context context;
        private List<LogisticsBean> lists = null;
        private LayoutInflater mInflater;

     public TimeLineAdapter(Context context) {
            super();
            this.context = context;
         this.mInflater = LayoutInflater.from(context);
            lists = new ArrayList<LogisticsBean>();
        }

        @Override
        public int getCount() {
            return lists.size();
        }

        @Override
        public Object getItem(int position) {
            return lists.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TimeLineHolder viewHolder = null;
            if (convertView == null) {
                viewHolder = new TimeLineHolder();
                convertView = mInflater.inflate(R.layout.item_logistics, null);
                viewHolder.title = (TextView) convertView.findViewById(R.id.txt_title);
                viewHolder.time = (TextView) convertView.findViewById(R.id.txt_time);
                viewHolder.image_dot = (ImageView)convertView.findViewById(R.id.image_dot);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (TimeLineHolder) convertView.getTag();
            }
            LogisticsBean bean = lists.get(position);
            viewHolder.title.setText(bean.Rem);
            viewHolder.time.setText(bean.Time);
            if (position == 0){
                viewHolder.image_dot.setImageResource(R.drawable.dot_yellow);
            }else{
                viewHolder.image_dot.setImageResource(R.drawable.dot);
            }
            return convertView;

        }

        static class TimeLineHolder{
            private TextView title,time;
            private ImageView image_dot;
        }


        public void setLists(List<LogisticsBean> lists) {
            this.lists = lists;
        }

        public List<LogisticsBean> getLists() {
            return lists;
        }
    }
