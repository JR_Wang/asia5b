package com.example.lenovo.asia5b.my.bean;

import java.util.List;

/**
 * Created by lenovo on 2017/7/14.
 */

public class BankBean {
    public String bank_id;
    public String parent_id;
    public String bank_name;
    public String bank_type;
    public String bank_account = "";
    public String bank_url = "";
    public List<BankBean> chile;
}
