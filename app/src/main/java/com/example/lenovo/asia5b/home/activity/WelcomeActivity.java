package com.example.lenovo.asia5b.home.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;

import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;
import app.MyApplication;

/**
 * 引导页
 * Created by lenovo on 2017/7/17.
 */

public class WelcomeActivity extends Activity {
    public static  final String FIRST_OPEN = "FIRST_OPEN";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent1 =getIntent();
        String url = intent1.getDataString();
        if (!TextUtils.isEmpty(url)){
            if (url.indexOf("id")>-1){
                String[] aa = url.split("=");
                String[] bb = aa[1].split("\\?");
                String cc = bb[0];
                if (cc.indexOf("&")>-1) {
                    String[] dd= cc.split("\\&");
                    cc = dd[0];
                }
                MyApplication.goodsId = cc;
                Log.e("22222222222222222",cc);
            }
        }


        // 判断是否是第一次开启应用
        SharedPreferences languagePre = getSharedPreferences("welcome",
                Context.MODE_PRIVATE);
        boolean isFirstOpen = languagePre.getBoolean(FIRST_OPEN,false );
        // 如果是第一次启动，则先进入功能引导页
        if (!isFirstOpen) {
            Intent intent = new Intent(this, WelcomeGuideActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        setContentView(R.layout.activity_unfinished);
        handler.postDelayed(runnable, 3000);
    }

    Handler handler=new Handler();
    Runnable runnable=new Runnable() {
        @Override
        public void run() {
            //要做的事情
            Intent intent = new Intent(WelcomeActivity.this,MainActivity.class);
            startActivity(intent);
            BaseActivity baseActivity = new BaseActivity() {
                @Override
                public void finishAll() {
                    super.finishAll();
                }
            };
            baseActivity.finishAll();
            finish();
            handler.removeCallbacks(runnable);
        }
    };



}
