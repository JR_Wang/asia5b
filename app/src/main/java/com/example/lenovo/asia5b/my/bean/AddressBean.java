package com.example.lenovo.asia5b.my.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/7/20.
 */

public class AddressBean implements Serializable{
    public  String address_id;
    public  String country_2;
    public  String province;
    public  String city;
    public  String address;
    public  String is_default;
    public  String contact_id;
    public String type = "";
    public  String country_2_id;
    public  String province_id;
}

