package com.example.lenovo.asia5b.util;

import android.util.Log;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 短信验证码
 * Created by admin on 2017/7/13.
 */

public class SMSCode {

    private static final String SMSURL = Setting.sendsms;//短信

    public static void getCode(ICallBack iCallBack,String phone){
        try{
            sendMSM(iCallBack,phone);
        }catch (Exception e){
            Log.e("发送短信error:",e.getMessage());
        }
    }

    private static void sendMSM(ICallBack iCallBack,String phone) throws Exception{
        Map<String,String> map = new HashMap<String,String>();
        map.put("phone",phone);

        Date date = new Date();
        String time = String.valueOf(date.getTime());
        map.put("time",time);

        String[] sortStr = {"phone"+phone,"time"+time};
        String md5 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(Logic.sortToString(sortStr)));
        map.put("sign",md5);
        JsonTools.getJsonAll(iCallBack,SMSURL,map,0);
    }
}
