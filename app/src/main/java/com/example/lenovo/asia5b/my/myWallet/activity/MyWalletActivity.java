package com.example.lenovo.asia5b.my.myWallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.lenovo.asia5b.my.myWallet.fragment.AMTFragment;
import com.example.lenovo.asia5b.my.myWallet.fragment.CreditCardFragment;
import com.example.lenovo.asia5b.my.myWallet.fragment.EBankingFragment;
import com.example.lenovo.asia5b.my.myWallet.fragment.FPXFragment;
import com.example.lenovo.asia5b.util.MoneyUtil;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;

import app.BaseActivity;
import app.FragAdapter;

/**
 * 我的钱包
 * Created by lenovo on 2017/6/22.
 */

public class MyWalletActivity extends BaseActivity implements  MoneyUtil.MoneyInterface{
    private FPXFragment myTopUpFragment;
    private CreditCardFragment recordTopUpFragment;
    private EBankingFragment eBankingFragment;
    private AMTFragment amtFragment;

    private ViewPager vp_content;
    private   MoneyUtil moneyUtil = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wallet);
        initView();
    }
    public void initView() {
        moneyUtil= new MoneyUtil(this);
        vp_content =  (ViewPager)findViewById(R.id.vp_content);
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.my_top_up));
        F.id(R.id.public_txt_righe).text(getResources().getString(R.string.record_top_up));
        F.id(R.id.public_btn_left).clicked(this);
        F.id(R.id.btn_fpx).clicked(this);
        F.id(R.id.btn_credit_card).clicked(this);
        F.id(R.id.btn_e_banking).clicked(this);
        F.id(R.id.btn_atm).clicked(this);
        F.id(R.id.public_txt_righe).clicked(this);
        List<Fragment> fragments=new ArrayList<Fragment>();
        myTopUpFragment = new FPXFragment();
        recordTopUpFragment = new CreditCardFragment();
        eBankingFragment = new EBankingFragment();
        amtFragment = new AMTFragment();
        fragments.add(myTopUpFragment);
        fragments.add(recordTopUpFragment);
        fragments.add(eBankingFragment);
        fragments.add(amtFragment);
        FragAdapter adapter = new FragAdapter(getSupportFragmentManager(), fragments);
        vp_content.setOnPageChangeListener(new MyPagerOnPageChangeListener());
        vp_content.setAdapter(adapter);
        setTabSelection(0);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.public_btn_left:
                finish();
                break;
            case R.id.btn_fpx:
                setTabSelection(0);
                break;
            case R.id.btn_credit_card:
                setTabSelection(1);
                break;
            case R.id.btn_e_banking:
                setTabSelection(2);
                break;
            case R.id.btn_atm:
                setTabSelection(3);
                break;
            case R.id.public_txt_righe:
                intent = new Intent(MyWalletActivity.this,RecordTopUpActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    private void resetBtn() {
        F.id(R.id.btn_fpx).backgroundColor( ContextCompat.getColor(MyWalletActivity.this,R.color.white));
        F.id(R.id.btn_fpx).textColor( ContextCompat.getColor(MyWalletActivity.this,R.color.my_333));

        F.id(R.id.btn_credit_card).backgroundColor( ContextCompat.getColor(MyWalletActivity.this,R.color.white));
        F.id(R.id.btn_credit_card).textColor( ContextCompat.getColor(MyWalletActivity.this,R.color.my_333));

        F.id(R.id.btn_e_banking).backgroundColor( ContextCompat.getColor(MyWalletActivity.this,R.color.white));
        F.id(R.id.btn_e_banking).textColor( ContextCompat.getColor(MyWalletActivity.this,R.color.my_333));

        F.id(R.id.btn_atm).backgroundColor( ContextCompat.getColor(MyWalletActivity.this,R.color.white));
        F.id(R.id.btn_atm).textColor( ContextCompat.getColor(MyWalletActivity.this,R.color.my_333));
    }

    public void setTabSelection(int index) {
            resetBtn();
            vp_content.setCurrentItem(index);
            switch (index) {
                case 0:
                    F.id(R.id.btn_fpx).backgroundColor( ContextCompat.getColor(MyWalletActivity.this,R.color.my_999));
                    F.id(R.id.btn_fpx).textColor(ContextCompat.getColor(MyWalletActivity.this,R.color.white));
                    break;
                case 1:
                    F.id(R.id.btn_credit_card).backgroundColor(ContextCompat.getColor(MyWalletActivity.this,R.color.my_999));
                    F.id(R.id.btn_credit_card).textColor(ContextCompat.getColor(MyWalletActivity.this,R.color.white));
                    break;
                case 2:
                    F.id(R.id.btn_e_banking).backgroundColor(ContextCompat.getColor(MyWalletActivity.this,R.color.my_999));
                    F.id(R.id.btn_e_banking).textColor(ContextCompat.getColor(MyWalletActivity.this,R.color.white));
                    break;
                case 3:
                    F.id(R.id.btn_atm).backgroundColor(ContextCompat.getColor(MyWalletActivity.this,R.color.my_999));
                    F.id(R.id.btn_atm).textColor(ContextCompat.getColor(MyWalletActivity.this,R.color.white));
                    break;
            }
    }

    /**
     * ViewPager的PageChangeListener(页面改变的监听器)
     */
    private class MyPagerOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }
        /**
         */
        @Override
        public void onPageSelected(int position) {
            setTabSelection(position);
        }
    }

    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {
            case 0:
                if (data == null) {
                    return;
                }
                moneyUtil.moneyData();
                myTopUpFragment.onActivityResult(requestCode,resultCode,data);
                recordTopUpFragment.onActivityResult(requestCode,resultCode,data);
                break;
            default:
                break;
        }
    }

    @Override
    public void money(String user_money,String pay_points) {
        if (null !=recordTopUpFragment) {
            recordTopUpFragment.money(user_money);
        }
        if (null != myTopUpFragment){
            myTopUpFragment.money(user_money);
        }
    }

}
