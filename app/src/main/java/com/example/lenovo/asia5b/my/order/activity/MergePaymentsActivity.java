package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.lenovo.asia5b.home.bean.BdShippingBean;
import com.example.lenovo.asia5b.home.bean.ShippingBean;
import com.example.lenovo.asia5b.my.activity.AddressActivity;
import com.example.lenovo.asia5b.my.bean.AddressBean;
import com.example.lenovo.asia5b.my.bean.ShippingPersonBean;
import com.example.lenovo.asia5b.my.order.bean.MergePaymentsBean;
import com.example.lenovo.asia5b.my.order.bean.MergePaymentsThrBean;
import com.example.lenovo.asia5b.my.order.bean.MergePaymentsTitBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.MoneyUtil;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;


/**
 * Created by lenovo on 2017/7/26.
 */

public class MergePaymentsActivity  extends BaseActivity implements ICallBack, MoneyUtil.MoneyInterface{

    private List<MergePaymentsTitBean> lists = null;

    private LoadingDalog loadingDalog;
    private LayoutInflater mInflater;
    private LinearLayout ll_home;
    private List<List<CheckBox>> listCheckBoxs;
    private List<CheckBox> listTitBoxs;
    //收货地址ID，收货人ID,国际运输ID，本地运输ID
    private String  address_id,contact_id,shipping_id,transport_id,shipping_type="3492";
    private Spinner spr_shipping,spr_transport;
    private TextView txt_address,txt_consignee,txt_mobile;
    private boolean isMergePayments = false;
    private String type = "0";
    private   MoneyUtil moneyUtil = null;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge_payments);
        initView();
        mergePaymentsData();
        ressListData();
        personListData();
        bdShippingData();
    }

    public  void initView() {
        moneyUtil= new MoneyUtil(this);
        loadingDalog = new LoadingDalog(this);
        loadingDalog.show();
        F.id(public_btn_left).clicked(this);
        ll_home = (LinearLayout)findViewById(R.id.ll_home);
        spr_shipping = (Spinner) findViewById(R.id.spr_shipping);
        spr_transport = (Spinner) findViewById(R.id.spr_transport);
        this.mInflater = LayoutInflater.from(MergePaymentsActivity.this);
        txt_address = (TextView)findViewById(R.id.txt_address);
        txt_consignee = (TextView)findViewById(R.id.txt_consignee);
        txt_mobile = (TextView)findViewById(R.id.txt_mobile);
        F.id(R.id.frame_order_address).clicked(this);
        F.id(R.id.btn_next_step).clicked(this);
        if (getIntent().hasExtra("type")){
            type = getIntent().getStringExtra("type");
        }
        if (type.equals("0")) {
            F.id(R.id.public_title_name).text(getResources().getString(R.string.integration_payment));
        } else {
            F.id(R.id.public_title_name).text(getResources().getString(R.string.particularly_commodity_payment));
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                if (isMergePayments) {
                    intent = new Intent();
                    intent.putExtra("222", 2222);
                    this.setResult(0, intent);
                }
                finish();
                break;
            case R.id.frame_order_address:
                startActivityForResult(new Intent(MergePaymentsActivity.this,AddressActivity.class),0);
                break;
            case R.id.btn_next_step:
                nextStepData();
                break;
            default:
                break;
        }
    }

    /**
     * 获取合并和特殊商品列表数据
     */
    public void  mergePaymentsData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.GO_MERGE_ORDER;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"type"+type,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("type",type);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }

    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            JSONArray account = json.getJSONArray("order");
                            lists = Logic.getListToBean(account,new MergePaymentsTitBean());
                            for (int i = 0; i < lists.size(); i++) {
                                MergePaymentsTitBean titBean = lists.get(i);
                                titBean.list_merge = new ArrayList<MergePaymentsBean>();
                                JSONArray jsonTitArray = new JSONArray(titBean.order_goods);
                                for (int j = 0; j< jsonTitArray.length(); j++) {
                                    MergePaymentsBean bean = new MergePaymentsBean();
                                    JSONObject jsonObject = jsonTitArray.getJSONObject(j);
                                    String is_merged = jsonObject.getString("is_merged");
                                    if (is_merged.equals("0")) {
                                        bean = (MergePaymentsBean)Logic.getBeanToJSONObject(jsonObject,new MergePaymentsBean());
                                    } else {
                                        bean.order_id = jsonObject.getString("order_id");
                                        bean.rec_ids = jsonObject.getString("rec_ids");
                                        bean.china_fee_rmb = jsonObject.getString("china_fee_rmb");
                                        bean.china_fee = jsonObject.getString("china_fee");
                                        bean.is_merged = jsonObject.getString("is_merged");
                                        JSONArray goods_lsit = jsonObject.getJSONArray("goods_lsit");
                                        bean.goods_lsit = Logic.getListToBean(goods_lsit,new MergePaymentsThrBean());
                                    }
                                    titBean.list_merge.add(bean);
                                    titBean.order_goods = "";
                                }
                            }
                            addHome();

                        } else if (state == 1) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
            //地址配送
            else if (action == 1) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            JSONArray account = json.getJSONArray("address");
                            List<AddressBean> lists = Logic.getListToBean(account, new AddressBean());
                            AddressBean bean = lists.get(0);
                            txt_address.setText(bean.address+bean.province+bean.country_2);
                            address_id = bean.address_id;
                            shipping_type = bean.type;
                            shippingData();
                        } else if (state == 1) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
            }
            //收货人配送
            else if (action == 2) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            JSONArray account = json.getJSONArray("contact");
                            List<ShippingPersonBean> lists= Logic.getListToBean(account, new ShippingPersonBean());
                            ShippingPersonBean bean = lists.get(0);
                            txt_consignee.setText(bean.consignee);
                            txt_mobile.setText(bean.mobile);
                            contact_id = bean.contact_id;
                        } else if (state == 1) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
            }
            else if (action == 3) {
                try {
                    JSONObject json = (JSONObject) result;
                    int state = (int)json.getInt("State");
                    if (state == 0) {
                        JSONArray bd_shipping = json.getJSONArray("shipping");
                        List<BdShippingBean> listbdShipping = Logic.getListToBean(bd_shipping, new BdShippingBean());
                        showBdShippingData(listbdShipping);
                    }
                    else if (state == 1) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.qmsb));
                    } else if (state == 2) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.cscw));
                    }
                } catch (Exception e) {
                }
            }
            else if (action == 4) {
                try {
                    JSONObject json = (JSONObject) result;
                    int state = (int)json.getInt("State");
                    if (state == 0) {
                        JSONObject shipping = json.getJSONObject("feight");
                        JSONArray shippingList = shipping.getJSONArray(shipping_type);
                        List<ShippingBean> listShipping = Logic.getListToBean(shippingList, new ShippingBean());
                        showBdBdShippingData(listShipping);
                    }
                    else if (state == 1) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.qmsb));
                    } else if (state == 2) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.cscw));
                    }
                } catch (Exception e) {
                }
            }
            else if (action == 5) {
                try {
                    JSONObject json = (JSONObject) result;
                    int state = (int)json.getInt("State");
                    if (state == 0) {
                        Intent intent = new Intent(MergePaymentsActivity.this,ConfirmOrderActivity.class);
                        intent.putExtra("order_id",json.getString("new_order_id"));
                        if (type.equals("0")){
                            intent.putExtra("is_specail","0");
                        } else {
                            intent.putExtra("is_specail", "1");
                        }
                        intent.putExtra("is_merge","2");
                        //要是用户只点一个订单返回就不刷新整个列表
//                    if (ogoodsIds.indexOf("\\|")!=-1) {
                        intent.putExtra("mergePayments",true);
//                    }
                        startActivityForResult(intent,0);
                    }else if (state == 1) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.qmsb));
                    } else if (state == 2) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.cscw));
                    } else if (state == 3) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.wdl));
                    }else if (state == 4) {
                        U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.qsrshdz));
                    }
                } catch (Exception e) {
                }
                loadingDalog.dismiss();
            }

        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);

    }

    /**
     * 添加标题
     */
    public void addHome() {
        listCheckBoxs = new ArrayList<List<CheckBox>>();
        listTitBoxs = new ArrayList<CheckBox>();
        for (int i = 0; i < lists.size(); i++) {
            final MergePaymentsTitBean bean = lists.get(i);
            List<CheckBox> listSonBoxs = new ArrayList<CheckBox>();
            View convertView= mInflater.inflate(R.layout.item_merge_payments_tit, null);
            TextView txt_title = (TextView)convertView.findViewById(R.id.txt_title);
            CheckBox cb_pro_checkbox = (CheckBox) convertView.findViewById(R.id.cb_pro_checkbox);
            LinearLayout ll_title = (LinearLayout)convertView.findViewById(R.id.ll_title);
            final int pos = i;
            txt_title.setText(getResources().getString(R.string.order_number) +":\t"+bean.order_sn);
            cb_pro_checkbox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (((CheckBox)view).isChecked()) {
                        ((CheckBox)view).setChecked(true);
                    } else {
                        ((CheckBox)view).setChecked(false);
                    }
                    forTitSelect(pos, ((CheckBox)view).isChecked());
                }
            });
            initTitView(ll_title,bean.list_merge,listSonBoxs,i);
            ll_home.addView(convertView);
            listCheckBoxs.add(listSonBoxs);
            listTitBoxs.add(cb_pro_checkbox);
        }
    }

    /**
     * 添加内容
     * @param ll_title
     * @param lists
     */
    public void initTitView(LinearLayout ll_title,List<MergePaymentsBean> lists,List<CheckBox> listSonBoxs,int id) {
        for (int i = 0; i < lists.size(); i ++) {
            MergePaymentsBean bean = lists.get(i);
            //单个包裹
            if (bean.is_merged.equals("0")) {
                View hotSaleView = mInflater.inflate(R.layout.item_merge_payments, null);
                CheckBox cb_pro_checkbox = (CheckBox) hotSaleView.findViewById(R.id.cb_pro_checkbox);
                ImageView image_name = (ImageView) hotSaleView.findViewById(R.id.image_name);
                TextView txt_name = (TextView) hotSaleView.findViewById(R.id.txt_name);
                TextView txt_price = (TextView) hotSaleView.findViewById(R.id.txt_price);
                TextView txt_num = (TextView) hotSaleView.findViewById(R.id.txt_num);
                txt_name.setText(bean.goods_name);
                txt_price.setText("RM" + bean.goods_price);
                DownloadPicture.loadNetwork(bean.goods_attr_thumb, image_name);
                txt_num.setText("x" + bean.goods_number);
                ll_title.addView(hotSaleView);
                final int titPos = id;
                final int pos = i;
                cb_pro_checkbox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (((CheckBox)view).isChecked()) {
                            ((CheckBox)view).setChecked(true);
                        } else {
                            ((CheckBox)view).setChecked(false);
                        }
                        forTitSelectSon(titPos);
                    }
                });

                listSonBoxs.add(cb_pro_checkbox);
            }
            //合并包裹
            else {
                for (int j = 0; j < bean.goods_lsit.size(); j++) {
                    MergePaymentsThrBean thrBean = bean.goods_lsit.get(j);
                    View hotSaleView = mInflater.inflate(R.layout.item_merge_payments, null);
                    CheckBox cb_pro_checkbox = (CheckBox) hotSaleView.findViewById(R.id.cb_pro_checkbox);
                    ImageView image_name = (ImageView) hotSaleView.findViewById(R.id.image_name);
                    TextView txt_name = (TextView) hotSaleView.findViewById(R.id.txt_name);
                    TextView txt_price = (TextView) hotSaleView.findViewById(R.id.txt_price);
                    TextView txt_num = (TextView) hotSaleView.findViewById(R.id.txt_num);
                    TextView txt_line = (TextView) hotSaleView.findViewById(R.id.txt_line);
                    if (j!=0) {
                        cb_pro_checkbox.setVisibility(View.INVISIBLE);
                    } else {
                        cb_pro_checkbox.setVisibility(View.VISIBLE);
                        final int titPos = id;
                        final int pos = i;
                        cb_pro_checkbox.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                if (((CheckBox)view).isChecked()) {
                                    ((CheckBox)view).setChecked(true);
                                } else {
                                    ((CheckBox)view).setChecked(false);
                                }
                                forTitSelectSon(titPos);
                            }
                        });
                        listSonBoxs.add(cb_pro_checkbox);
                    }
                    if (j+1 == bean.goods_lsit.size()) {
                        txt_line.setVisibility(View.VISIBLE);
                    } else {
                        txt_line.setVisibility(View.GONE);
                    }
                    txt_name.setText(thrBean.goods_name);
                    txt_price.setText("RM" + thrBean.goods_price);
                    DownloadPicture.loadNetwork(thrBean.goods_attr_thumb, image_name);
                    txt_num.setText("x" + thrBean.goods_number);
                    ll_title.addView(hotSaleView);
                }
            }
        }
    }

    /**
     * 选中第二级和第三级
     * @param titPos
     */
    public void forTitSelectSon(int titPos) {
        //判断用户是否全选哦,默认全选
        boolean isCheckall = true;
        for (int i = 0; i< listCheckBoxs.size(); i++) {
            if (i == titPos) {
                List<CheckBox> listSonBoxs = listCheckBoxs.get(i);
                for (int j = 0; j <listSonBoxs.size();j++) {
                    CheckBox checkBox = listSonBoxs.get(j);
                    if (!checkBox.isChecked()) {
                        isCheckall = false;
                        break;
                    }
                }
                listTitBoxs.get(titPos).setChecked(isCheckall);
                break;
            }
        }
    }

    /**
     * 选中第一级
     * @param pos
     * @param isB
     */
    public void forTitSelect(int pos,boolean isB) {
        for (int i = 0; i< listCheckBoxs.size(); i++) {
            if (i == pos) {
                List<CheckBox> listSonBoxs = listCheckBoxs.get(i);
                for (int j = 0; j <listSonBoxs.size();j++) {
                    CheckBox checkBox = listSonBoxs.get(j);
                    checkBox.setChecked(isB);
                }
                break;
            }
        }
    }



    /**
     * 获取地址列表数据
     */
    public void ressListData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.RESS_LIST;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"type"+1,"page"+1,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("type",""+1);
            parmaMap.put("page",""+1);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap,- 1);
        }catch (Exception e) {
        }
    }

    /**
     * 获取收货人列表数据
     */
    public void personListData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.RESS_LIST;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"type"+2,"page"+1,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("type",""+2);
            parmaMap.put("page",""+1);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 2);
        }catch (Exception e) {
        }
    }

    /**
     * 本地运输方式
     */
    public void bdShippingData() {
        Map<String, String> parmaMap = new HashMap<String, String>();
        String url = Setting.BD_SHIPPING;
        JsonTools.getJsonAll(this, url, parmaMap, 3);
    }

    /**
     * 国际运输方式
     */
    public void shippingData() {
        Map<String, String> parmaMap = new HashMap<String, String>();
        String url = Setting.SHIPPING;
        parmaMap.put("type",type);
        JsonTools.getJsonAll(this, url, parmaMap, 4);
    }


    //本地派送选择器
    public void showBdShippingData(final List<BdShippingBean> listbdShipping) {
        List<String > listNameOnes=new ArrayList<String>();
        for (int i = 0; i <listbdShipping.size(); i++) {
            BdShippingBean bean = listbdShipping.get(i);
            String cityname = bean.shipping_name;
            listNameOnes.add(cityname);
        }
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(MergePaymentsActivity.this,
                R.layout.simple_list_item,
                listNameOnes);
        spr_shipping.setAdapter(adapter1);
        spr_shipping.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                shipping_id = listbdShipping.get(arg2).shipping_id;
            }
            public void onNothingSelected(AdapterView<?> arg0) {
                arg0.setVisibility(View.VISIBLE);
            }
        });

    }

    //国际派送选择器
    public void showBdBdShippingData(final List<ShippingBean> listShipping) {
        List<String > listNameOnes=new ArrayList<String>();
        for (int i = 0; i <listShipping.size(); i++) {
            ShippingBean bean = listShipping.get(i);
            String cityname = bean.transport;
            listNameOnes.add(cityname);
        }
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(MergePaymentsActivity.this,
                R.layout.simple_list_item,
                listNameOnes);
        spr_transport.setAdapter(adapter1);
        spr_transport.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                transport_id = listShipping.get(arg2).transport_id;
            }
            public void onNothingSelected(AdapterView<?> arg0) {
                arg0.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        switch(resultCode){
            case 0:
                if (null !=data) {
                    if (data.hasExtra("AddressBean")) {
                        //当点击地址Item回来的时候
                        AddressBean bean = (AddressBean)data.getSerializableExtra("AddressBean");
                        txt_address.setText(bean.address+bean.province+bean.country_2);
                        address_id = bean.address_id;
                        shipping_type = bean.type;
                        shippingData();
                    } else if (data.hasExtra("ShippingPersonBean")) {
                        //当点击地址Item回来的时候
                        ShippingPersonBean bean = (ShippingPersonBean)data.getSerializableExtra("ShippingPersonBean");
                        txt_consignee.setText(bean.consignee);
                        txt_mobile.setText(bean.mobile);
                        contact_id = bean.contact_id;
                    } else if (data.hasExtra("confirm")) {
                        Intent intent = new Intent();
                        intent.putExtra("confirm",2222);
                        MergePaymentsActivity.this.setResult(0, intent);
                        finish();
                    } else{
                        ll_home.removeAllViews();
                        loadingDalog.show();
                        mergePaymentsData();
                        isMergePayments = true;
                        moneyUtil.moneyData();
                    }
                }
                break;
        }
    }

    /**
     * 下一步
     */
    String ogoodsIds;
    public void nextStepData() {
        ogoodsIds = getOgoodsIds();
        if (TextUtils.isEmpty(contact_id)) {
            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.add_address_hint_2));
        }else if (TextUtils.isEmpty(address_id)) {
            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.add_address_hint_1));
        } else if (TextUtils.isEmpty(shipping_id)) {
            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.bdpsfsbnwk));
        } else if (TextUtils.isEmpty(transport_id)) {
            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.gjpsfsbnwk));
        } else if (TextUtils.isEmpty(ogoodsIds)) {
            U.Toast(MergePaymentsActivity.this, getResources().getString(R.string.spslbnw0));
        }else {
            try {
                loadingDalog.show();
                Map<String, String> parmaMap = new HashMap<String, String>();
                String url = Setting.GO_MERGE_CHULI;
                String user_id = getUserBean().user_id;
                Date date = new Date();
                String time = String.valueOf(date.getTime());
                String[] sort = {"user_id" + user_id, "shipping_id" + shipping_id, "address_id" + address_id, "contact_id" + contact_id, "transport" + transport_id, "time" + time};
                String sortStr = Logic.sortToString(sort);
                String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
                parmaMap.put("user_id", user_id);
                parmaMap.put("shipping_id", shipping_id);
                parmaMap.put("address_id", address_id);
                parmaMap.put("contact_id", contact_id);
                parmaMap.put("transport", transport_id);
                parmaMap.put("ogoods_ids",ogoodsIds);
                parmaMap.put("is_specail",type);
                parmaMap.put("time", time);
                parmaMap.put("submit_type","Android");
                parmaMap.put("sign", md5_32);
                JsonTools.getJsonAll(this, url, parmaMap, 5);
            } catch (Exception e) {

            }
        }

    }

    public String getOgoodsIds(){
        String ogoods_ids="";
        boolean isOne;
        if (listTitBoxs == null) {
            return "";
        }
        for (int i = 0; i <lists.size();i++) {
            CheckBox checkTitBox = listTitBoxs.get(i);
            MergePaymentsTitBean beanTit = lists.get(i);
            List<CheckBox> listSonBoxs = listCheckBoxs.get(i);
            //当订单号勾选上的情况，表示包裹号全选上
            if (checkTitBox.isChecked()) {
                ogoods_ids = ogoods_ids + beanTit.order_id+"-";
                for (int j = 0; j < beanTit.list_merge.size();j++) {
                    MergePaymentsBean bean = beanTit.list_merge.get(j);
                    if (bean.is_merged.equals("0")) {
                        ogoods_ids = ogoods_ids+ bean.rec_id+",";
                    } else {
                        ogoods_ids = ogoods_ids+ bean.rec_ids+",";
                    }
                }
                ogoods_ids = ogoods_ids.substring(0,ogoods_ids.length()-1)+"|";
            } else {
                isOne =false;
                for (int j = 0; j < beanTit.list_merge.size();j++) {
                    MergePaymentsBean bean = beanTit.list_merge.get(j);
                    CheckBox checkBox = listSonBoxs.get(j);
                    //判断包裹选中的情况才能把订单Id传进去
                    if (checkBox.isChecked()) {
                        if (!isOne) {
                            ogoods_ids = ogoods_ids + beanTit.order_id + "-";
                            isOne =true;
                        }
                        if (bean.is_merged.equals("0")) {
                            ogoods_ids = ogoods_ids + bean.rec_id + ",";
                        } else {
                            ogoods_ids = ogoods_ids + bean.rec_ids + ",";
                        }
                    }
                }
                if (!TextUtils.isEmpty(ogoods_ids)){
                    ogoods_ids = ogoods_ids.substring(0,ogoods_ids.length()-1)+"|";
                }
            }
        }
        if (TextUtils.isEmpty(ogoods_ids)){
            return "";
        } else {
            return ogoods_ids.substring(0,ogoods_ids.length()-1);
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断用户是否点击的是返回键
        if(keyCode == KeyEvent.KEYCODE_BACK){
            if (isMergePayments == true) {
                Intent intent = new Intent();
                intent.putExtra("222",2222);
                this.setResult(0, intent);
            }
            finish();

        }
        return false;
    }

    @Override
    public void money(String user_money,String pay_points) {

    }

}
