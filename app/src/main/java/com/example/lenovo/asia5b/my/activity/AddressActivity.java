package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.my.fragment.ShippingAddressFragment;
import com.example.lenovo.asia5b.my.fragment.ShippingPersonFragment;

import java.util.ArrayList;
import java.util.List;

import app.BaseActivity;
import app.FragAdapter;

import static com.wushi.lenovo.asia5b.R.id.btn_my_points;

/**
 * 收货地址列表
 * Created by lenovo on 2017/6/28.
 */

public class AddressActivity extends BaseActivity {
    private ShippingAddressFragment myDaikinRollFragment;
    private ShippingPersonFragment myPointsFragment;
    private ViewPager vp_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorable_integral);
        initView();
    }

    public void initView() {
        vp_content = (ViewPager) findViewById(R.id.vp_content);
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.shdzgl));
        F.id(R.id.public_txt_righe).visibility(View.VISIBLE);
        F.id(R.id.public_txt_righe).text(getResources().getString(R.string.tj));
        F.id(R.id.public_txt_righe).clicked(this);
        F.id(R.id.public_btn_left).clicked(this);
        F.id(R.id.btn_my_daikin_roll).clicked(this);
        F.id(R.id.btn_my_daikin_roll).text(getResources().getString(R.string.shipping_address));
        F.id(btn_my_points).clicked(this);
        F.id(btn_my_points).text(getResources().getString(R.string.shr));
        List<Fragment> fragments = new ArrayList<Fragment>();
        myDaikinRollFragment = new ShippingAddressFragment();
        myPointsFragment = new ShippingPersonFragment();
        fragments.add(myDaikinRollFragment);
        fragments.add(myPointsFragment);
        FragAdapter adapter = new FragAdapter(getSupportFragmentManager(), fragments);
        vp_content.setOnPageChangeListener(new MyPagerOnPageChangeListener());
        vp_content.setAdapter(adapter);
        setTabSelection(0);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.public_btn_left:
                finish();
                break;
            case R.id.btn_my_daikin_roll:
                setTabSelection(0);
                break;
            case btn_my_points:
                setTabSelection(1);
                break;
            case R.id.public_txt_righe:
                intent = new Intent(AddressActivity.this,AddAddressActivity.class);
                startActivityForResult(intent,0);
                break;
            default:
                break;
        }
    }

    public void setTabSelection(int index) {
        if (index == 0) {
            vp_content.setCurrentItem(0);
            F.id(R.id.btn_my_daikin_roll).backgroundColor(getResources().getColor(R.color.my_999));
            F.id(R.id.btn_my_daikin_roll).textColor(getResources().getColor(R.color.white));
            F.id(btn_my_points).backgroundColor(getResources().getColor(R.color.white));
            F.id(btn_my_points).textColor(getResources().getColor(R.color.my_333));
        } else {
            vp_content.setCurrentItem(1);
            F.id(btn_my_points).backgroundColor(getResources().getColor(R.color.my_999));
            F.id(btn_my_points).textColor(getResources().getColor(R.color.white));
            F.id(R.id.btn_my_daikin_roll).backgroundColor(getResources().getColor(R.color.white));
            F.id(R.id.btn_my_daikin_roll).textColor(getResources().getColor(R.color.my_333));
        }
    }

    /**
     * ViewPager的PageChangeListener(页面改变的监听器)
     */
    private class MyPagerOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        /**
         */
        @Override
        public void onPageSelected(int position) {
            setTabSelection(position);
        }
    }

    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case 0:
                if (data == null) {
                    return;
                }
                myPointsFragment.ressListData();
                myDaikinRollFragment.ressListData();
                break;
            case 1:
                myDaikinRollFragment.onActivityResult(requestCode,resultCode,data);
                break;
            case 2:
                myPointsFragment.onActivityResult(requestCode,resultCode,data);
                break;
            default:
                break;
        }
    }

}
