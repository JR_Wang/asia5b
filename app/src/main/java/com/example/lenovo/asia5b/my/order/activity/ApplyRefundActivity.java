package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.lenovo.asia5b.my.order.bean.ParcelListBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.JsonTools2;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.btn_next_step;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * 申请退货和查看退货
 * Created by lenovo on 2017/7/27.
 */

public class ApplyRefundActivity extends BaseActivity implements ICallBack {
    private ParcelListBean bean;
    private Spinner spr_reason;
    private List<LinearLayout> linearLayouts ;
    private String add_time,rec_id,order_id,mobile,order_sn;
    private LoadingDalog loadingDalog;
    private List<String> imagsPaths = null;
    private  int position;
    private String reason;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_refund);
        initView();
    }

    public void initView() {
        Intent intent = this.getIntent();
        bean = (ParcelListBean)intent.getSerializableExtra("bean");
        add_time = getIntent().getStringExtra("add_time");
        order_id = getIntent().getStringExtra("order_id");
        rec_id= getIntent().getStringExtra("rec_id");
        mobile= getIntent().getStringExtra("mobile");
        order_sn= getIntent().getStringExtra("order_sn");
        loadingDalog = new LoadingDalog(this);
        //退货原因
        spr_reason = (Spinner)findViewById(R.id.spr_reason);
        F.id(public_btn_left).clicked(this);
        F.id(btn_next_step).clicked(this);
        F.id(R.id.txt_parcel_number).text(bean.rec_id);
        F.id(R.id.txt_order_number).text(order_sn);
        F.id(R.id.txt_place_an_order_time).text(DateUtils.timedate(add_time));
        F.id(R.id.txt_contact_way).text(mobile);
        F.id(R.id.txt_name).text(bean.goods_name);
        F.id(R.id.txt_price).text(bean.goods_price);
        F.id(R.id.txt_num).text("x"+bean.goods_number);
        DownloadPicture.loadNetwork(bean.goods_attr_thumb,F.id(R.id.image_name).getImageView());
        //凭证上传
        linearLayouts = new ArrayList<LinearLayout>();
        imagsPaths = new ArrayList<String>();
        linearLayouts.add((LinearLayout) F.id(R.id.ll_one).getView());
        linearLayouts.add((LinearLayout) F.id(R.id.ll_two).getView());
        linearLayouts.add((LinearLayout) F.id(R.id.ll_three).getView());
        linearLayouts.add((LinearLayout) F.id(R.id.ll_four).getView());
        linearLayouts.add((LinearLayout) F.id(R.id.ll_five).getView());
        for (int i = 0; i<linearLayouts.size(); i++){
            imagsPaths.add("");
            LinearLayout layout = linearLayouts.get(i);
            if (getIntent().hasExtra("check_refund")) {
                layout.getChildAt(1).setVisibility(View.INVISIBLE);
            } else {
                layout.setOnClickListener(new StartPhotOnOnClick(i));
                layout.getChildAt(1).setOnClickListener(new DelImageOnOnClick(i));
            }
        }
        if (getIntent().hasExtra("check_refund")) {
            F.id(R.id.public_title_name).text(getResources().getString(R.string.ckth)
            );
            F.id(R.id.edit_issue_number).visibility(View.GONE);
            F.id(R.id.txt_issue_number).visibility(View.VISIBLE);
            F.id(R.id.edit_explain).visibility(View.GONE);
            F.id(R.id.txt_explain).visibility(View.VISIBLE);
            spr_reason.setVisibility(View.GONE);
            F.id(R.id.txt_reason).visibility(View.VISIBLE);
            F.id(R.id.btn_next_step).visibility(View.GONE);
            F.id(R.id.ll_sales_return_time).visibility(View.VISIBLE);
            F.id(R.id.ll_check_state).visibility(View.VISIBLE);
            checkRefundData();
            F.id(R.id.txt_uploading).visibility(View.GONE);
            F.id(R.id.txt_uploading_two).visibility(View.GONE);
        } else {
            F.id(R.id.public_title_name).text(getResources().getString(R.string.sqth));
            spr_reason.setOnItemSelectedListener(new Spinner.OnItemSelectedListener(){
                public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                    reason = (String) arg0.getAdapter().getItem(arg2);
                }
                public void onNothingSelected(AdapterView<?> arg0) {
                    arg0.setVisibility(View.VISIBLE);
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;
            case btn_next_step:
                applyRefundData();
                break;

        }
    }

    public class StartPhotOnOnClick implements View.OnClickListener {
        int id;
        public StartPhotOnOnClick(int id) {
            this.id = id;
        }
        @Override
        public void onClick(View v) {
            loadingDalog.showPhoto();
            position = id;
        }
    }

    public class DelImageOnOnClick implements View.OnClickListener {
        int id;
        public DelImageOnOnClick(int id) {
            this.id = id;
        }
        @Override
        public void onClick(View v) {
            ((ImageView)linearLayouts.get(id).getChildAt(0)).setImageResource(R.drawable.main_remaishangpin);
            imagsPaths.set(id,"");
        }
    }

    /**
     * 查看退货数据
     */
    public void checkRefundData() {
        loadingDalog.show();
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.CHECK_REFUND;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id" + user_id, "order_id" + order_id, "rec_id" + rec_id, "time" + time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id", order_id);
            parmaMap.put("rec_id", rec_id);
            parmaMap.put("time", time);
            parmaMap.put("sign", md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 1);
        } catch (Exception e) {
        }
    }


    /**
     * 申请退货数据
     */
    public void applyRefundData() {
        //问题件数
        String goods_number = F.id(R.id.edit_issue_number).getText().toString();
        //说明
        String msg = F.id(R.id.edit_explain).getText().toString();
        //原因
        String reason = this.reason;
        //图片集
        String imgs  = "";
        if (TextUtils.isEmpty(goods_number)){
            U.Toast(this,getResources().getString(R.string.wtjsbnwk));
        }
        else if (Integer.parseInt(goods_number) > Integer.parseInt(bean.goods_number) ){
            U.Toast(this,getResources().getString(R.string.srdydq));
        }
        else  if (TextUtils.isEmpty(msg)){
            U.Toast(this,getResources().getString(R.string.smbnwk));
        }else  if (reason.equals(getResources().getString(R.string.qxzthyy))){
            U.Toast(this,getResources().getString(R.string.yybnwk));
        }else if (TextUtils.isEmpty(imgs)){
            U.Toast(this,getResources().getString(R.string.qsctp));
        }else {
            try {
                loadingDalog.show();
                if (msg.indexOf("\n")!=-1) {
                    msg = msg.replaceAll("\\\n","");
                }
                for (int i = 0; i < imagsPaths.size();i++){
                    if (!TextUtils.isEmpty(imagsPaths.get(i))) {
                        imgs = imgs + imagsPaths.get(i) +",";
                    }
                }
                if (imgs.length() > 5) {
                    imgs = imgs.substring(0, imgs.length() - 1);
                }
                Map<String, String> parmaMap = new HashMap<String, String>();
                String url = Setting.APPLY_REFUND;
                String user_id = getUserBean().user_id;
                Date date = new Date();
                String time = String.valueOf(date.getTime());
                String[] sort = {"user_id" + user_id, "order_id" + order_id, "rec_id" + rec_id, "goods_number" + goods_number, "reason" + reason,"time" + time};
                String sortStr = Logic.sortToString(sort);
                String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
                parmaMap.put("user_id", user_id);
                parmaMap.put("order_id", order_id);
                parmaMap.put("rec_id", rec_id);
                parmaMap.put("goods_number", goods_number);
                parmaMap.put("reason", reason);
                parmaMap.put("msg", msg);
                parmaMap.put("time", time);
                parmaMap.put("sign", md5_32);
                Map<String, String> parmaMap2 = new HashMap<String, String>();
                parmaMap2.put("imgs", imgs);
                JsonTools2.dataHttps(this, url, parmaMap,parmaMap2, 0);
            } catch (Exception e) {
            }
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject jsonObect = (JSONObject) result;
                        int state = jsonObect.getInt("State");
                        if (state == 0) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.cg));
                            Intent intent = new Intent();
                            intent.putExtra("222", 2222);
                            ApplyRefundActivity.this.setResult(0, intent);
                            finish();
                        }
                        else if (state == 1) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.slbd));
                        } else if (state == 5) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.qxzthyy));
                        } else if (state == 6) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.smbnwk));
                        } else if (state == 7) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.mybg));
                        } else if (state == 8) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.spzzth));
                        } else if (state == 9) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.sqsb));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
            else if (action == 1) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            json = json.getJSONObject("msg");
                            //说明
                            F.id(R.id.txt_explain).text(json.getString("msg_content"));
                            //退货原因
                            F.id(R.id.txt_reason).text(json.getString("msg_title"));
                            //退货数量
                            F.id(R.id.txt_issue_number).text(json.getString("number_refund"));
                            //退货时间
                            String addTime = DateUtils.timedate(json.getString("msg_time"));
                            F.id(R.id.txt_sales_return_time).text(addTime);
                            //审核态
                            String msg_status =json.getString("msg_status");
                            if (msg_status.equals("9")) {
                                msg_status = getResources().getString(R.string.wsh);
                            } else if (msg_status.equals("15")) {
                                msg_status = getResources().getString(R.string.ytg);
                                msg_status = msg_status +"("+ getResources().getString(R.string.fhje)+"RM"+json.getString("msg_price")+")";
                            }else if (msg_status.equals("17")) {
                                msg_status = getResources().getString(R.string.btg);
                            }
                            F.id(R.id.txt_check_state).text(msg_status);
                            JSONArray imgs = json.getJSONArray("imgs");
                            for (int i = 0 ;i < imagsPaths.size(); i++) {
                                LinearLayout layout= linearLayouts.get(i);
                                if (i < imgs.length()) {
                                    String dd = imgs.getString(i);
                                    DownloadPicture.loadNetwork(dd,(ImageView)layout.getChildAt(0));
                                } else {
                                    layout.getChildAt(i).setVisibility(View.GONE);
                                }
                            }

                        }  else if (state == 1) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(ApplyRefundActivity.this, getResources().getString(R.string.wdl));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);

    }

    Uri cameraUri = null;
    @Override
    protected void onActivityResult(int requestCode, int arg1, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, arg1, data);
        switch (requestCode) {
            case 101:// 相机
                if (data != null) {
                    if (data.getExtras() != null) {
                        cameraUri = loadingDalog.getPhotoUrihotoUri();
                        hear(null);
                    } else {
                        U.Toast(this,getResources().getString(R.string.system_no_getpic));
                    }
                } else {
                    if (loadingDalog.getPhotoUrihotoUri() != null) {
                        cameraUri = loadingDalog.getPhotoUrihotoUri();
                        hear(null);
                    } else {
                        U.Toast(this,getResources().getString(R.string.system_no_getpic));
                    }
                }
                // setPicture();
                break;
            case 102:// 相册返回
//                listPicture.clear();
                cameraUri = null;
                if (data != null) {
                    if (data.getData() != null) {
                        cameraUri = data.getData();
                        hear(null);
                    } else {
                        U.Toast(this, getResources().getString(R.string.system_no_getpic));
                    }
                }else if (loadingDalog.getPhotoUrihotoUri() != null) {
                    cameraUri = loadingDalog.getPhotoUrihotoUri();
                    hear(null);
                } else {
                    U.Toast(this, getResources().getString(R.string.system_no_getpic));
                }
                break;
            case 103:
                break;
        }
    }

    public void hear(Intent data) {
        Bitmap photo = null;
        if (data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                photo = bundle.getParcelable("data");
            } else if (data != null) {
                Uri uri1 = data.getData();
                try {
                    photo = MediaStore.Images.Media.getBitmap(getContentResolver(), uri1);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }  else {
            try {
                photo = MediaStore.Images.Media.getBitmap(getContentResolver(), cameraUri);
                //图片太大实现压缩功能
                if (DownloadPicture.getBitmapSize(photo) > 10000000) {
                    photo = DownloadPicture.smallOneFifth(photo);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (photo == null) {
            return;
        }
        BufferedOutputStream bos = null;
        if (photo != null) {
            File dirFile = new File(Setting.IMAGE_ROOTPATH);
            if (!dirFile.exists())
                dirFile.mkdirs();
            SimpleDateFormat sDateFormat = new SimpleDateFormat(
                    "yyyyMMddhhmmss", Locale.ENGLISH);
            File uploadFile = new File(dirFile + "/"
                    + sDateFormat.format(new java.util.Date())
                    + ".jpg");

            char[] chars = "0123456789abcdef".toCharArray();
            StringBuilder sb = new StringBuilder("");
            int bit;
            try {
                uploadFile.createNewFile();
                bos = new BufferedOutputStream(new FileOutputStream(uploadFile));
                photo.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                FileInputStream fis = new FileInputStream(uploadFile);
                byte[] b = new byte[fis.available()];
                fis.read(b);
                for (int i = 0; i < b.length; i++) {
                    bit = (b[i] & 0x0f0) >> 4;
                    sb.append(chars[bit]);
                    bit = b[i] & 0x0f;
                    sb.append(chars[bit]);
                }

                fis.close();
                String photoFile = uploadFile.getAbsolutePath();
                DownloadPicture.loadThisLocality(photoFile, ((ImageView) linearLayouts.get(position).getChildAt(0)));
                imagsPaths.set(position, sb.toString());
            } catch (IOException e) {
                U.Toast(ApplyRefundActivity.this, e.getLocalizedMessage());
            } finally {
                if (bos != null) {
                    try {
                        bos.flush();
                        bos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

}
