package com.example.lenovo.asia5b.commodity.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.apkfuns.logutils.LogUtils;
import com.example.lenovo.asia5b.commodity.adapter.AdvertiseLinearLayoutManager;
import com.example.lenovo.asia5b.commodity.adapter.ClassifyRVLeftAdapter;
import com.example.lenovo.asia5b.commodity.adapter.ClassifyRVRightAdapter;
import com.example.lenovo.asia5b.commodity.bean.ClassifyBean;
import com.example.lenovo.asia5b.ui.MyGridView;
import com.wushi.lenovo.asia5b.R;
import com.zhy.adapter.recyclerview.CommonAdapter;

import java.util.List;

import app.BaseFragment;

/**
 * Created by lenovo on 2017/6/23.
 */

public class ClassifyMainFragment extends BaseFragment{
    private View fragmentView;
    private ClassifyBean bean;
    private LinearLayout ll_class_two;
    private boolean isPrepared;
    private RecyclerView rv_left_classify;
    private RecyclerView rv_right_classify;
    private CommonAdapter<ClassifyBean> rv_right_classify_Adapter;
    private List<ClassifyBean> classifyList;
    private MyGridView gridview;
    private List<ClassifyBean> datas;

    public static ClassifyMainFragment newInstance(int position, ClassifyBean bean) {
        ClassifyMainFragment f = new ClassifyMainFragment();
        Bundle b = new Bundle();
        b.putSerializable("ClassifyBean", bean);
        b.putString("pos", position+"");
        f.setArguments(b);
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initPrepare();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bean = (ClassifyBean)getArguments().getSerializable("ClassifyBean");
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.frame_classify_main, container,
                false);

        return fragmentView;
    }

    public void addView() {

        //右边recyclerview

//        datas = new ArrayList<ClassifyBean>();
        final RecyclerView.LayoutManager layoutManagerRight=new AdvertiseLinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rv_right_classify.setLayoutManager(layoutManagerRight);
//        for(int i = 0; i<this.bean.childs.size(); i++) {
//            final ClassifyBean bean2 = this.bean.childs.get(i);
////            for (int j = 0; j<bean2.childs.size();j++){
////                final ClassifyBean bean3 = bean2.childs.get(j);
//                datas.add(bean2);
////            }
//        }

        LogUtils.xml("===="+bean.getThreeChilds());
        ClassifyRVRightAdapter classifyAdapterRight=new ClassifyRVRightAdapter(getActivity(),this.bean.getThreeChilds());
        rv_right_classify.setAdapter(classifyAdapterRight);

        //左边recyclerview
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(getActivity(),LinearLayoutManager.VERTICAL,false);
        rv_left_classify.setLayoutManager(layoutManager);
        final ClassifyRVLeftAdapter classifyRVAdapterLeft=new ClassifyRVLeftAdapter(getActivity(),this.bean.getChilds());
        rv_left_classify.setAdapter(classifyRVAdapterLeft);


        //右边recyclerview
        rv_right_classify.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                RecyclerView.LayoutManager layoutManager = recyclerView.getLayoutManager();
                //判断是当前layoutManager是否为LinearLayoutManager
                // 只有LinearLayoutManager才有查找第一个和最后一个可见view位置的方法
                int lastItemPosition = 0;//最后可见 右边
                int firstItemPosition;//第一次可见 右边
                if (layoutManager instanceof LinearLayoutManager) {
                    LinearLayoutManager linearManager = (LinearLayoutManager) layoutManager;
                    //获取最后一个可见view的位置
                    lastItemPosition = linearManager.findLastVisibleItemPosition();
                    //获取第一个可见view的位置
                    firstItemPosition = linearManager.findFirstVisibleItemPosition();
//                    if (foodsArrayList.get(firstItemPosition) instanceof Foods) {
//                        int foodTypePosion = ((Foods) foodsArrayList.get(firstItemPosition)).getFood_stc_posion();
//                        FoodsTypeListview.getChildAt(foodTypePosion).setBackgroundResource(R.drawable.choose_item_selected);
//                    }
//                    System.out.println(lastItemPosition + "   " + firstItemPosition);
                }
                classifyRVAdapterLeft.setSelectedPosition(lastItemPosition);
                classifyRVAdapterLeft.notifyDataSetChanged();
            }
        });

        //左边recyclerview
        classifyRVAdapterLeft.setOnItemClickListener(new ClassifyRVLeftAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                rv_right_classify.smoothScrollToPosition(position);
                classifyRVAdapterLeft.setSelectedPosition(position);
                classifyRVAdapterLeft.notifyDataSetChanged();
            }
        });
    }

    /**
     * 第一次onResume中的调用onUserVisible避免操作与onFirstUserVisible操作重复
     */
    private boolean isFirstResume = true;

    @Override
    public void onResume() {
        super.onResume();
        if (isFirstResume) {
            isFirstResume = false;
            return;
        }
        if (getUserVisibleHint()) {
            onUserVisible();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getUserVisibleHint()) {
            onUserInvisible();
        }
    }

    private boolean isFirstVisible = true;
    private boolean isFirstInvisible = true;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            if (isFirstVisible) {
                isFirstVisible = false;
                initPrepare();
            } else {
                onUserVisible();
            }
        } else {
            if (isFirstInvisible) {
                isFirstInvisible = false;
                onFirstUserInvisible();
            } else {
                onUserInvisible();
            }
        }
    }

    public synchronized void initPrepare() {
        if (isPrepared) {
            onFirstUserVisible();
        } else {
            isPrepared = true;
        }
    }

    /**
     * 第一次fragment可见（进行初始化工作）
     */
    public void onFirstUserVisible() {
        ll_class_two = (LinearLayout)fragmentView.findViewById(R.id.ll_class_two);
        rv_left_classify=fragmentView.findViewById(R.id.rv_left_classify);
        rv_right_classify=fragmentView.findViewById(R.id.rv_right_classify);
        addView();
    }

    /**
     * fragment可见（切换回来或者onResume）
     */
    public void onUserVisible() {

    }

    /**
     * 第一次fragment不可见（不建议在此处理事件）
     */
    public void onFirstUserInvisible() {

    }

    /**
     * fragment不可见（切换掉或者onPause）
     */
    public void onUserInvisible() {

    }
}
