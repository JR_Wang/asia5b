package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.MyPointsBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lenovo on 2017/7/1.
 */

public class MypointsAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<MyPointsBean> lists ;
    private Context context;


    public MypointsAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<MyPointsBean>();
        this.context = context;
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_my_points, null);
            holder.txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txt_pay_points = (TextView)convertView.findViewById(R.id.txt_pay_points);
            holder.txt_con_points = (TextView)convertView.findViewById(R.id.txt_con_points);
            holder.txt_order_sn = (TextView)convertView.findViewById(R.id.txt_order_sn);
            holder.txt_change_desc = (TextView)convertView.findViewById(R.id.txt_change_desc);

            convertView.setTag(holder);

        }else {

            holder = (ViewHolder)convertView.getTag();
        }
        MyPointsBean bean = lists.get(position);
        if (!TextUtils.isEmpty(bean.change_time)) {
            String time = DateUtils.timedate(bean.change_time);
            holder.txt_time.setText(time.replace(" ","\n"));
        }

        String pay_points = bean.pay_points;
        holder.txt_pay_points.setText(pay_points);
        holder.txt_con_points.setText(context.getResources().getString(R.string.sy_fee)+":\t"+bean.con_points);
        String order_sn = bean.order_sn;
        holder.txt_order_sn.setText(context.getResources().getString(R.string.order_number)+":\t"+order_sn.replace("test",""));
        holder.txt_change_desc.setText(context.getResources().getString(R.string.explain)+":\t"+bean.change_desc);
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_time,txt_pay_points,txt_con_points,txt_order_sn,txt_change_desc;
    }

    public void setLists(List<MyPointsBean> lists) {
        this.lists = lists;
    }

    public List<MyPointsBean> getLists() {
        return lists;
    }

}
