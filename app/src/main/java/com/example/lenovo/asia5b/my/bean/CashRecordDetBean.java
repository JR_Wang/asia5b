package com.example.lenovo.asia5b.my.bean;

/**
 * Created by lenovo on 2017/7/12.
 */

public class CashRecordDetBean {
    public String id;
    public String amount;
    public String is_paid;
    public String add_time;
    public String time;
    public String serial;
    public String admin_note;
    public String bank;
    public String yhzh;
}
