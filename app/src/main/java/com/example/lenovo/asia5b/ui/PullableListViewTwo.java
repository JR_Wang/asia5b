package com.example.lenovo.asia5b.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;
/**
 * 图片轮播与下拉冲突解决
 */
public class PullableListViewTwo extends ListView implements Pullable {

	public PullableListViewTwo(Context context) {
		super(context);
	}

	public PullableListViewTwo(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public PullableListViewTwo(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public boolean canPullDown() {
		if (getCount() == 0) {
			// 没有item的时候也可以下拉刷新
			return PullableScrollView.isPullableScrollViewTrue;
		} else if (null!=getChildAt(0)&&getFirstVisiblePosition() == 0
				&& getChildAt(0).getTop() >= 0) {
			// 滑到ListView的顶部了
			return PullableScrollView.isPullableScrollViewTrue;
		} else
			return false;
	}

	@Override
	public boolean canPullUp() {
		if (getCount() == 0) {
			// 没有item的时候也可以上拉加载
			return true;
		} else if (getLastVisiblePosition() == (getCount() - 1)) {
			// 滑到底部了
			if (getChildAt(getLastVisiblePosition() - getFirstVisiblePosition()) != null
					&& getChildAt(
							getLastVisiblePosition()
									- getFirstVisiblePosition()).getBottom() <= getMeasuredHeight())
				return true;
		}
		return false;
	}
}
