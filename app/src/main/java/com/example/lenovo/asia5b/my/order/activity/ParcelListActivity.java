package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lenovo.asia5b.home.bean.UserBean;
import com.example.lenovo.asia5b.my.order.adapter.ParcelListAdapter;
import com.example.lenovo.asia5b.my.order.bean.ParcelListBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.MoneyUtil;
import com.example.lenovo.asia5b.util.Setting;
import com.example.lenovo.asia5b.util.SharedPreferencesUtils;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/6/24.
 */

public class ParcelListActivity extends BaseActivity implements ICallBack,MoneyUtil.MoneyInterface{

    private ListView lv_parcel;
    private ParcelListAdapter adapter;
    private String order_id,order_status,mobile;
    private LoadingDalog loadingDalog;
    private TextView activity_parcel_list_order_sn;
    public boolean isSelect;
    private MoneyUtil moneyUtil = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_list);
        Intent intent = getIntent();
        order_id = intent.getStringExtra("order_id");
        order_status= intent.getStringExtra("order_status");
        mobile= intent.getStringExtra("mobile");
        initView();
    }

    public void initView() {
        moneyUtil= new MoneyUtil(this);
        loadingDalog = new LoadingDalog(this);
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getString(R.string.orderdetails));
        F.id(public_btn_left).clicked(this);
        lv_parcel = (ListView)findViewById(R.id.lv_parcel);
        F.id(R.id.txt_cost_details).clicked(this);
        F.id(R.id.activity_parcel_list_order_payment).clicked(this);//付款
        String order_sn = getIntent().getStringExtra("order_sn");
        adapter = new ParcelListAdapter(this,order_status,order_id,mobile,order_sn);

        activity_parcel_list_order_sn = (TextView)findViewById(R.id.activity_parcel_list_order_sn);

        initPostURL();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                if (isSelect) {
                    intent = new Intent();
                    intent.putExtra("222",2222);
                    this.setResult(0, intent);
                }
                finish();
                break;
            case  R.id.txt_cost_details:
                intent = new Intent(ParcelListActivity.this,InvoiceDetailsActivity.class);
                intent.putExtra("order_id",order_id);
                startActivity(intent);
                break;
//            case  R.id.activity_parcel_list_order_payment:
//                intent = new Intent(ParcelListActivity.this,ConfirmOrderActivity.class);
//                intent.putExtra("order_id",order_id);
//                startActivity(intent);
//                break;
            default:
                break;
        }
    }

    public void initPostURL(){
        try{
            loadingDalog.show();
            Date date = new Date();
            if (null != adapter.getParcelList()) {
                adapter.getParcelList().clear();
                adapter.notifyDataSetChanged();
            }
            UserBean userBean = (UserBean) SharedPreferencesUtils.getObjectFromShare("user");
            if(userBean != null){
                String orderwrapUrl = Setting.ORDERWRAP;
                String user_id = userBean.user_id;
                String time = String.valueOf(date.getTime());

                Map<String,String> map = new HashMap<String,String>();
                map.put("user_id",user_id);
                map.put("order_id",order_id);
                map.put("time",time);

                String[] sortStr = {"user_id"+user_id,"order_id"+order_id,"time"+time};
                String sort = Logic.sortToString(sortStr);
                String md5 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sort));
                map.put("sign",md5);

                JsonTools.getJsonAll(this,orderwrapUrl,map,0);
            }
        }catch (Exception e){
            Log.e("订单包裹error",e.getMessage());
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            try{
                if(result != null){
                    if(action == 0){
                        JSONObject jsonObect = (JSONObject)result;
                        int state = jsonObect.getInt("State");
                        if(state == 0){
                            JSONObject order = jsonObect.getJSONObject("order");
                            if (!order.has("add_time")){
                                Intent intent = new Intent();
                                intent.putExtra("222",2222);
                                ParcelListActivity.this.setResult(0, intent);
                                finish();
                                return;
                            }
                            activity_parcel_list_order_sn.setText(":\t"+order.getString("order_sn"));//订单号

                            JSONArray jsonArray = order.getJSONArray("goods_list");
                            List<ParcelListBean> parcelList = Logic.getListToBean(jsonArray,new ParcelListBean());

                            adapter.setParcelList(parcelList);
                            adapter.setAddTimye(order.getString("add_time"));

                            lv_parcel.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }else if (state == 1) {
                            U.Toast(ParcelListActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(ParcelListActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(ParcelListActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            Intent intent = new Intent();
                            intent.putExtra("222",2222);
                            ParcelListActivity.this.setResult(0, intent);
                            finish();
                            return;
                        }
                    }
                }
            }catch (Exception e){
            }
            loadingDalog.dismiss();
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断用户是否点击的是返回键
        if(keyCode == KeyEvent.KEYCODE_BACK){
            if (isSelect) {
                Intent intent = new Intent();
                intent.putExtra("222",2222);
                this.setResult(0, intent);
            }
            finish();

        }
        return false;
    }

    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (requestCode) {
            case 0:
                if (data == null) {
                    return;
                }
                isSelect = true;
                loadingDalog.show();
                initPostURL();
                moneyUtil.moneyData();
                break;
            default:
                break;
        }
    }

    @Override
    public void money(String user_money,String pay_points) {

    }

}
