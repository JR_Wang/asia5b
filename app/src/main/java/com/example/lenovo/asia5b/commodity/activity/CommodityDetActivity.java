package com.example.lenovo.asia5b.commodity.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.commodity.adapter.RecommendAdapter;
import com.example.lenovo.asia5b.commodity.bean.CommodityDetBean;
import com.example.lenovo.asia5b.commodity.bean.GoodsPromoteBean;
import com.example.lenovo.asia5b.home.activity.LoginActivity;
import com.example.lenovo.asia5b.home.fragment.MianHomeFragment;
import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.mob.moblink.Scene;
import com.mob.moblink.SceneRestorable;
import com.umeng.socialize.UMShareAPI;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.GradationScrollView;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.WarpLinearLayout;
import com.example.lenovo.asia5b.ui.photoview.ImagePagerActivity;
import com.example.lenovo.asia5b.util.CommonUtil;
import com.example.lenovo.asia5b.util.DensityUtil;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.JsonTools2;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;

import static com.wushi.lenovo.asia5b.R.id.btn_sure;
import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;

/**
 * 商品详情
 * Created by lenovo on 2017/6/30.
 */

public class CommodityDetActivity  extends BaseActivity implements GradationScrollView.ScrollViewListener, ICallBack,SceneRestorable{

    private RollPagerView mRollViewPager;//图片轮播

    private Dialog mDialog,mDialog2,mDialog3;
    private ImageView img_course_icon;
    private TextView txt_product_price;
    private TextView txt_select;
    private LinearLayout layout_sku_content;
    private RecommendAdapter adapter;
    private ListView lv_recommend;
    private GradationScrollView sv_del;
    private ImageView image_name,image_baby,image_comment,image_details,image_recommend,public_btn_left;
    private String goods_id;
    private LoadingDalog loadingDalog;
    private String bottom_tit_url = null;
    //当前商品数量
    private int goods_num = 1;

    private TextView textView;//传递的商品详情URL-测试用
    //显示商品数量
    private TextView txt_count;
    private EditText edit_count;
    //判断商品属性是否为空
    private boolean isGoodsAttr;

    private WebView webView;

    //判断用户是否已收藏
    private boolean isCollect =false;

    private static  TextView txt_mess;
    private static RelativeLayout rl_shape;
    //第一次进来就使用一次
    private boolean isOne;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_commodity_det);

        Intent intent = getIntent();
        String tagName = "";
        if(intent.hasExtra("tmall")){
            goods_id = intent.getStringExtra("tmall");
//            tagName = "天猫商品详情：";
        }
        if (getIntent().hasExtra("goods_id")) {
            goods_id = getIntent().getStringExtra("goods_id");
        }
//        textView = findViewById(R.id.text_load_url);
//        textView.setText(tagName+url);
        Log.e("密码密码密码密码密码:",goods_id+"");
        initView();
        goodsDetailsData();
    }

    public void initView() {
        loadingDalog = new LoadingDalog(CommodityDetActivity.this);
        sv_del = (GradationScrollView)findViewById(R.id.sv_del);
        image_name = (ImageView) findViewById(R.id.image_name);
        image_baby = (ImageView) findViewById(R.id.image_baby);
        image_comment = (ImageView) findViewById(R.id.image_comment);
        image_details = (ImageView) findViewById(R.id.image_details);
        image_recommend = (ImageView) findViewById(R.id.image_recommend);

        image_name.setOnClickListener(this);
        webView = (WebView)findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        WebSettings webSettings = webView.getSettings();
        webSettings.setUseWideViewPort(true); // 将图片调整到适合webview的大小
        webSettings.setLoadWithOverviewMode(true); // 设置加载进来的页面自适应手机屏幕

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                top4 = F.id(R.id.ll_top4).getView().getTop()-  F.id(R.id.ll_tit_det).getView().getHeight();
            }
        });

        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.spxq));
        public_btn_left = (ImageView)findViewById(R.id.public_btn_left);
        public_btn_left.setOnClickListener(this);
        public_btn_left.setImageResource(R.drawable.arrow_left_blue_2);
        F.id(R.id.ll_select).clicked(this);
        F.id(R.id.txt_service).clicked(this);
        F.id(R.id.txt_store).clicked(this);
        //收藏
        F.id(R.id.txt_collect).clicked(this);


        F.id(R.id.btn_join_shoping_cart).clicked(this);
//        F.id(R.id.btn_promptly_purchase).clicked(this);
        F.id(R.id.ll_product_parameter).clicked(this);
        F.id(R.id.btn_look_over_all_evaluate).clicked(this);
        //顶部4个按钮
        F.id(R.id.ll_det).visibility(View.VISIBLE);
        F.id(R.id.ll_baby).clicked(this);
        F.id(R.id.ll_comment).clicked(this);
        F.id(R.id.ll_details).clicked(this);
        F.id(R.id.ll_recommend).clicked(this);

        lv_recommend = (ListView)findViewById(R.id.lv_recommend);
        adapter = new RecommendAdapter(this);
        lv_recommend.setAdapter(adapter);
        lv_recommend.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                GoodsPromoteBean bean = adapter.getLists().get(i);
                Intent intent = new Intent(CommodityDetActivity.this, CommodityDetActivity.class);
                intent.putExtra("goods_id",bean.goods_id);
                startActivity(intent);
            }
        });
        F.id(R.id.ll_cn).clicked(this);
        F.id(R.id.ll_ma).clicked(this);
        F.id(R.id.img_mess).image(R.drawable.icon_share);
        F.id(R.id.rl_mess).clicked(this);
        txt_mess = (TextView)findViewById(R.id.txt_mess);
        rl_shape = (RelativeLayout)findViewById(R.id.rl_shape);
        F.id(R.id.btn_translate).clicked(this);

        SharedPreferences languagePre = getSharedPreferences("language_choice",
                Context.MODE_PRIVATE);
        int id =languagePre.getInt("id", 1);
        switch (id) {
            case 0:
                F.id(R.id.txt_cn).text("中");
                F.id(R.id.txt_cn_2).text("EN");
                F.id(R.id.txt_ma).text("中");
                F.id(R.id.txt_ma_2).text("MA");
                break;
            case 1:
                F.id(R.id.txt_cn).text("EN");
                F.id(R.id.txt_cn_2).text("中");
                F.id(R.id.txt_ma).text("EN");
                F.id(R.id.txt_ma_2).text("MA");
                break;
            case 2:
                F.id(R.id.txt_cn).text("MA");
                F.id(R.id.txt_cn_2).text("中");
                F.id(R.id.txt_ma).text("MA");
                F.id(R.id.txt_ma_2).text("EN");
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.public_btn_left:
                if (!isCollect) {
                    intent = new Intent();
                    intent.putExtra("22","22");
                    this.setResult(0, intent);
                }
                finish();
                break;
            case R.id.ll_select:
                try {
                    if (!TextUtils.isEmpty(F.id(R.id.txt_cat_name).getText().toString())){
                        showDialog();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.btn_join_shoping_cart:
                try {
                    //当属性为空时，不需要弹出窗口，直接加入购物车接口
                    if (isGoodsAttr) {
                        joinShoppingCartData();
                    } else {
                        if (!TextUtils.isEmpty(F.id(R.id.txt_cat_name).getText().toString())){
                            showDialog();
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                break;
            case btn_sure :
                if ( null != getUserBean()) {
                    if (isProductPriceNull) {
                        joinShoppingCartData();
                    } else{
                        U.Toast(CommodityDetActivity.this,getResources().getString(R.string.spflwxq));
                    }
                } else {
//                    U.Toast(CommodityDetActivity.this,getResources().getString(R.string.qdlzh));
                    intent = new Intent(CommodityDetActivity.this, LoginActivity.class);
                    intent.putExtra("commodity","");
                    startActivity(intent);
                }
                break;
            case R.id.ll_product_parameter:
                productParameterDialog();
                break;
            case R.id.btn_look_over_all_evaluate:
                intent = new Intent(CommodityDetActivity.this,AllEvaluateActivity.class);
                intent.putExtra("goods_id",goods_id);
                startActivity(intent);
                break;
            case R.id.ll_baby:
                selectId = 0;
                select(selectId);
                break;
            case R.id.ll_comment:
                selectId = 1;
                select(selectId);
                break;
            case R.id.ll_details:
                selectId = 2;
                select(selectId);
                break;
            case R.id.ll_recommend:
                selectId = 3;
                select(selectId);
                break;
            case R.id.txt_service:
                intent = new Intent(CommodityDetActivity.this, ServiceActivity.class);
                startActivity(intent);
                break;
            case R.id.txt_store:
                finish();
                break;
            case R.id.image_del:
                if (goods_num>1) {
                    goods_num = goods_num - 1;
                }
                txt_count.setText(goods_num+"");
                edit_count.setText(goods_num+"");
                break;
            case R.id.image_add:
                goods_num = goods_num +1 ;
                txt_count.setText(goods_num+"");
                edit_count.setText(goods_num+"");
                break;
            case R.id.txt_collect:
                //收藏接口
                if (null ==getUserBean()) {
                    U.Toast(CommodityDetActivity.this,getResources().getString(R.string.qdlzh));
                } else {
                    //判断用户是否已收藏
                    if (isCollect) {
                        unCollectData();
                    } else {
                        collectData();
                    }
                }
                break;
            case R.id.image_name:
                imageBrower(0);
                break;
            //消息数量
            case R.id.rl_mess:
//                if(null == getUserBean()) {
//                    intent = new Intent(CommodityDetActivity.this, LoginActivity.class);
//                } else {
//                    intent = new Intent(CommodityDetActivity.this, MessageActivity.class);
//                    F.id(rl_shape).visibility(View.GONE);
//                    MianHomeFragment.message_count = 0;
//                    SharedPreferencesUtils.setInt("Message_Count",message_count);
//                }
//                startActivity(intent);
                if (null == HomeBean ||null == list_imag  ) {
                    return;
                }
                loadingDalog.showShare(HomeBean.fx_url,HomeBean.goods_name,"Amazing E-Shopping World",list_imag.get(0),0);

                break;
            case  R.id.img_course_icon:
                ArrayList<String> list_imag = new ArrayList<String>();
                list_imag.add(bottom_tit_url);
                intent = new Intent(CommodityDetActivity.this, ImagePagerActivity.class);
                // 图片url,为了演示这里使用常量，一般从数据库中或网络中获取
                intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_URLS, list_imag);
                intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_INDEX, 0);
                startActivity(intent);
                break;
            case R.id.btn_translate:
//                translateDialog ();
                break;
            case R.id.ll_cn:
                if (F.id(R.id.txt_cn).getText().toString().equals("中")) {
                    translateDialog (2);
                } else if (F.id(R.id.txt_cn).getText().toString().equals("EN")) {
                translateDialog (1);
                }else if (F.id(R.id.txt_cn).getText().toString().equals("MA")) {
                    translateDialog (1);
                }
                break;
            case R.id.ll_ma:
                if (F.id(R.id.txt_cn).getText().toString().equals("中")) {
                    translateDialog (3);
                }else if (F.id(R.id.txt_cn).getText().toString().equals("EN")) {
                    translateDialog (3);
                }else if (F.id(R.id.txt_cn).getText().toString().equals("MA")) {
                    translateDialog (2);
                }
                break;
            default:
                break;
        }
    }
    private ArrayList<String> list_imag = new ArrayList<String>();

    private void initRollViewPager( ){
        //图片轮播
        mRollViewPager =  (RollPagerView)findViewById(R.id.frame_new_products_roll_view_pager);
        //设置播放时间间隔
        mRollViewPager.setPlayDelay(900000000);
        //设置透明度
        mRollViewPager.setAnimationDurtion(500);
        //设置适配器
        mRollViewPager.setAdapter(new TestNormalAdapter());

        //设置指示器（顺序依次）
        //自定义指示器图片
        //设置圆点指示器颜色
        //设置文字指示器
        //隐藏指示器
        mRollViewPager.setHintView(new ColorPointHintView(CommodityDetActivity.this, Color.YELLOW,Color.WHITE));
    }

    class TestNormalAdapter extends StaticPagerAdapter {

        @Override
        public View getView(ViewGroup container, int position) {
            ImageView view = new ImageView(container.getContext());
            DownloadPicture.loadNetwork(list_imag.get(position),view);
            view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            final int id = position;
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageBrower(id);
                }
            });
            return view;
        }

        @Override
        public int getCount() {
            return list_imag.size();
        }
    }

    /**
     * 选择课程属性等...
     */
    private void showDialog() throws JSONException {
        if (null == mDialog){
            mDialog = new Dialog(this);
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            View view = LayoutInflater.from(this).inflate(R.layout.lessons_attributes_dialog, null);
            view.setBackgroundColor(Color.parseColor("#00000000"));
            img_course_icon = (ImageView) view.findViewById(R.id.img_course_icon);
            txt_product_price = (TextView) view.findViewById(R.id.txt_product_price);
            txt_select = (TextView) view.findViewById(R.id.txt_select);
            layout_sku_content = (LinearLayout) view.findViewById(R.id.layout_sku_content);
            Button btn_sure = (Button) view.findViewById(R.id.btn_sure);
            ImageView image_del = (ImageView) view.findViewById(R.id.image_del);
            ImageView image_add = (ImageView) view.findViewById(R.id.image_add);
            txt_count = (TextView)  view.findViewById(R.id.txt_count);
            edit_count = (EditText) view.findViewById(R.id.edit_count);
            edit_count.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


                }

                @Override
                public void afterTextChanged(Editable editable) {

                    if (!TextUtils.isEmpty(edit_count.getText().toString())) {
                        int dd = Integer.parseInt(edit_count.getText().toString());
                        if (dd > 9999) {
                            edit_count.setText("9999");
//                            edit_count.setSelection( edit_count.getText().toString().length());
                        }
                        if (dd < 1) {
                            edit_count.setText(1+"");
                        }
                        goods_num = Integer.parseInt(edit_count.getText().toString());
                    } else {
                        edit_count.setText(1+"");
                        goods_num = Integer.parseInt(edit_count.getText().toString());
                    }

                }
            });

            img_course_icon.setOnClickListener(this);
            refreshSkuProperties();
            //确定
            btn_sure.setOnClickListener(this);
            //减少数量
            image_del.setOnClickListener(this);
            //增加数量
            image_add.setOnClickListener(this);
            mDialog.setContentView(view);
            mDialog.setCanceledOnTouchOutside(true);
            Window dialogWindow = mDialog.getWindow();
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            //dialogWindow.setGravity(Gravity.LEFT | Gravity.TOP);
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = DensityUtil.dip2px(this, 430);
            lp.gravity = Gravity.BOTTOM;
            dialogWindow.setAttributes(lp);
            dialogWindow.setBackgroundDrawableResource(R.color.transparent_all);
        }
        mDialog.show();

    }

    /**
     * 刷新SKU 浮层
     * @throws JSONException
     */
    private void refreshSkuProperties() throws JSONException {
        //图标
        if (TextUtils.isEmpty(bottom_tit_url)) {
            img_course_icon.setImageResource(R.drawable.main_remaishangpin);
        } else {
            DownloadPicture.loadNetwork(bottom_tit_url,img_course_icon);
        }
////        //价格
        if (TextUtils.isEmpty(txt_product_price.getText().toString())) {
            txt_product_price.setText("RM\t"+HomeBean.shop_price_rm);
        }
        //选择
        txt_select.setText( F.id(R.id.txt_cat_name).getText().toString());

        //属性
        addViewForSKUUI(layout_sku_content);

        //一开始初始化就使用一次，为了默认选择第一个
        if (!isOne) {
            analysisGoodsRm();
        } else {
            isOne = true;
        }
    }

    /**
     * 为SKU填充内容
     *
     * @param layout
     */
    private void addViewForSKUUI(LinearLayout layout) throws JSONException {
        layout.removeAllViews();
        for (int i = 0; i < goods_attr_cat_ls.size();i++) {
            View view = LayoutInflater.from(this).inflate(R.layout.lesson_sku_centent_item, null);
            //SKU标题
            TextView attr_name = (TextView) view.findViewById(R.id.attr_name);
            //SKU内容
            CommodityDetBean bean = goods_attr_cat_ls.get(i);
            attr_name.setText(bean.catName);
            List<CommodityDetBean> beanList = bean.listBeans;
            //SKU内容
            WarpLinearLayout attr_content = (WarpLinearLayout) view.findViewById(R.id.attr_content);
            attr_content.setTag(""+i);
            fillUISkuViewGroup(beanList, attr_content);
            layout.addView(view);

        }
    }
    /**
     * 给属性布局SkuViewGroup填充
     *
     * @param layout
     */
    private void fillUISkuViewGroup(final List<CommodityDetBean> lists, WarpLinearLayout layout) throws JSONException {
        layout.removeAllViews();
        String tag = (String) layout.getTag();//SKU标题
//        String value = "小米标配";
//        if(skuAttrCache.containsKey(tag)) {
//            value = skuAttrCache.get(tag);//SKU值
//        }
        for (int i = 0; i < lists.size(); i++) {
            TextView textView = new TextView(this);
            textView.setTag(tag);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.rightMargin = DensityUtil.dip2px(this, 15);
            Drawable drawable1 = getResources().getDrawable(R.drawable.shape_background_f8f8f8_sku_btn);
            Drawable drawable2 = getResources().getDrawable(R.drawable.shape_background_ce160f_sku_btn);
            drawable1.setBounds(0, 0, drawable1.getMinimumWidth(), drawable1.getMinimumHeight());
            drawable2.setBounds(0, 0, drawable2.getMinimumWidth(), drawable2.getMinimumHeight());
            final  CommodityDetBean bean = lists.get(i);
            if (bean.isSelect) {
                textView.setBackground(drawable2);
                textView.setTextColor(getResources().getColor(R.color.white));
            } else {
                textView.setBackground(drawable1);
                textView.setTextColor(getResources().getColor(R.color.my_333));
            }
            textView.setText(bean.catName);
            textView.setLayoutParams(layoutParams);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    isProductPriceNull = false;
                    txt_product_price.setText("RM\t"+HomeBean.shop_price_rm);
                    if (bean.isSelect) {
                        bean.isSelect = false;
                    } else {
                        for (int i = 0 ; i< lists.size(); i++) {
                            lists.get(i).isSelect = false;
                        }
                        bean.isSelect = true;
                        if (!TextUtils.isEmpty( bean.imgIcon)) {
                            bottom_tit_url = bean.imgIcon;
                        } else {
//                            bottom_tit_url = "";
                        }
                        analysisGoodsRm();

                    }

                    try {
                        refreshSkuProperties();
                    } catch (JSONException e) {
                             e.printStackTrace();
                   }

                }
            });
            layout.addView(textView);

        }
    }

    public void productParameterDialog () {
        if (null == mDialog2){
            mDialog2 = new Dialog(this);
            mDialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
            View view = LayoutInflater.from(this).inflate(R.layout.view_product_parameter, null);
            view.setBackgroundColor(Color.parseColor("#ffffff"));
            Button btn_confirm = (Button)view.findViewById(R.id.btn_confirm);
            btn_confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog2.dismiss();
                }
            });
            //确定
            mDialog2.setContentView(view);
            mDialog2.setCanceledOnTouchOutside(true);
            Window dialogWindow = mDialog2.getWindow();
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            //dialogWindow.setGravity(Gravity.LEFT | Gravity.TOP);
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = DensityUtil.dip2px(this, 430);
            lp.gravity = Gravity.BOTTOM;
            dialogWindow.setAttributes(lp);
            dialogWindow.setBackgroundDrawableResource(R.color.transparent_all);
        }
        mDialog2.show();

    }

    //为了实现顶部4个按钮方法，这里获取 4个布局的 顶部位置
    int top2 = 0;
    int top3 = 0;
    int top4 = 0;
    int selectId = 0;
    int height ;
    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);


        top2 = F.id(R.id.ll_top2).getView().getTop()-  F.id(R.id.ll_tit_det).getView().getHeight();
        top3 = F.id(R.id.ll_top3).getView().getTop()-  F.id(R.id.ll_tit_det).getView().getHeight();
        top4 = F.id(R.id.ll_top4).getView().getTop()-  F.id(R.id.ll_tit_det).getView().getHeight();
        //实现滑动顶部逐渐透明化
        height = F.id(R.id.frame_new_products_roll_view_pager).getView().getHeight();
        sv_del.setScrollViewListener(CommodityDetActivity.this);
    }
    private void resetBtn() {
        image_baby.setVisibility(View.GONE);
        image_comment.setVisibility(View.GONE);
        image_details.setVisibility(View.GONE);
        image_recommend.setVisibility(View.GONE);
        F.id(R.id.txt_baby).textColor(Color.argb((int) 255, 1,24,28));
        F.id(R.id.txt_comment).textColor(Color.argb((int) 255, 1,24,28));
        F.id(R.id.txt_details).textColor(Color.argb((int) 255, 1,24,28));
        F.id(R.id.txt_recommend).textColor(Color.argb((int) 255, 1,24,28));
    }
    private void select(int selectId) {
        resetBtn();
        switch (selectId) {
            case 0:
//                image_baby.setVisibility(View.VISIBLE);
                F.id(R.id.txt_baby).textColor(Color.argb((int) 255, 22,140,213));
                sv_del.smoothScrollTo(0,0);
                break;
            case 1:
//                image_comment.setVisibility(View.VISIBLE);
                F.id(R.id.txt_comment).textColor(Color.argb((int) 255, 22,140,213));
                sv_del.smoothScrollTo(0,top2);
                break;
            case 2:
//                image_details.setVisibility(View.VISIBLE);
                F.id(R.id.txt_details).textColor(Color.argb((int) 255, 22,140,213));
                sv_del.smoothScrollTo(0,top3);
                break;
            case 3:
//                image_recommend.setVisibility(View.VISIBLE);
                F.id(R.id.txt_recommend).textColor(Color.argb((int) 255, 22,140,213));
                sv_del.smoothScrollTo(0,top4);
                break;
            default:
                break;
        }
    }

    /**
     * 滑动监听
     * @param scrollView
     * @param x
     * @param y
     * @param oldx
     * @param oldy
     */
    @Override
    public void onScrollChanged(GradationScrollView scrollView, int x, int y,
                                int oldx, int oldy) {
        if (y <= 0) {   //设置标题的背景颜色
            F.id(R.id.ll_tit_det).backgroundColor(Color.argb((int) 0, 255,255,255));
            F.id(R.id.ll_tit_det).visibility(View.GONE);
        } else if (y > 0 && y <= F.id(R.id.frame_new_products_roll_view_pager).getView().getHeight()) { //滑动距离小于banner图的高度时，设置背景和字体颜色颜色透明度渐变
            F.id(R.id.ll_tit_det).visibility(View.VISIBLE);
            float scale = (float) y / height;
            float alpha = (255 * scale);
            F.id(R.id.public_txt_righe).textColor(Color.argb((int) alpha, 1,24,28));
            F.id(R.id.txt_baby).textColor(Color.argb((int) alpha, 1,24,28));
            F.id(R.id.txt_comment).textColor(Color.argb((int) alpha, 1,24,28));
            F.id(R.id.txt_details).textColor(Color.argb((int) alpha, 1,24,28));
            F.id(R.id.txt_recommend).textColor(Color.argb((int) alpha, 1,24,28));
            F.id(R.id.ll_tit_det).backgroundColor(Color.argb((int) alpha, 255,255,255));
            image_name.setVisibility(View.VISIBLE);
            image_name.setAlpha((int) alpha);
            public_btn_left.setVisibility(View.VISIBLE);
            public_btn_left.setAlpha((int) alpha);

            if (selectId == 0 ) {
//                image_baby.setVisibility(View.VISIBLE);
                image_baby.setAlpha((int) alpha);
                F.id(R.id.txt_baby).textColor(Color.argb((int) alpha, 22,140,213));
            } else if (selectId == 1) {
                image_comment.setAlpha((int) alpha);
                F.id(R.id.txt_comment).textColor(Color.argb((int) alpha, 22,140,213));
            }  else if (selectId == 2) {
                image_details.setAlpha((int) alpha);
                F.id(R.id.txt_details).textColor(Color.argb((int) alpha, 22,140,213));
            } else if (selectId == 3) {
                image_recommend.setAlpha((int) alpha);
                 F.id(R.id.txt_recommend).textColor(Color.argb((int) alpha, 22,140,213));
            }
        } else {    //滑动到banner下面设置普通颜色
            F.id(R.id.ll_tit_det).backgroundColor(Color.argb((int) 255, 255,255,255));
        }
    }

    /**
     * 详情接口
     */
    public void goodsDetailsData() {
        loadingDalog.show();
        Map<String, String> formMap = new HashMap<String, String>();
        String url = Setting.GOODS_DETAILS;
        if (null != getUserBean()) {
            String user_id = getUserBean().user_id;
            formMap.put("user_id", user_id);
        }
        formMap.put("url_id",goods_id);
        JsonTools.getJsonAll(this, url, formMap, 0);
        JsonTools2.setOne(true);
    }

    /**
     *
     * 加入购物车
     */
    public  void joinShoppingCartData() {
        if (null != txt_product_price) {
            if (!TextUtils.isEmpty(txt_product_price.getText().toString())) {
                if (txt_product_price.getText().toString().indexOf("\t") != -1) {
                    String[] aa = txt_product_price.getText().toString().split("\\\t");
                    if (aa.length > 1) {
                        double bb = Double.parseDouble(aa[1]);
                        if (bb <= 0) {
                            return;
                        }
                    }
                }
            }
        }
        loadingDalog.show();
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.ADD_CART;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id" + user_id, "goods_id" + goods_id, "goods_num" + goods_num, "attr_id" + selectCount, "time" + time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("goods_id",""+goods_id);
            parmaMap.put("goods_num",""+goods_num);
            parmaMap.put("attr_id",""+selectCount);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 1);
            JsonTools2.setOne(true);
        } catch (Exception e) {

        }
    }

    //商品详情里的推荐商品
    public void goodsPromote() {
        Map<String, String> parmaMap = new HashMap<String, String>();
        String url = Setting.GOODS_PROMOTE;
        parmaMap.put("goods_id",""+goods_id);
        JsonTools.getJsonAll(this, url, parmaMap, 2);
        JsonTools2.setOne(true);
    }

    /*
     *收藏接口
     */
    public void collectData() {
        loadingDalog.show();
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.COLLECT;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id" + user_id, "goods_id" + goods_id, "time" + time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("goods_id",""+goods_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 3);
            JsonTools2.setOne(true);
        } catch (Exception e) {

        }
    }

    /*
    *取消收藏接口
    */
    public void unCollectData() {
        loadingDalog.show();
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.UN_COLLECT;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id" + user_id, "goods_id" + goods_id, "time" + time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("goods_id",""+goods_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 4);
            JsonTools2.setOne(true);
        } catch (Exception e) {

        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            //判断该商品是否收藏过
                            int collected = (int)json.getInt("collected");
                            if (collected == 1) {
                                isCollect = true;
                                Drawable rightDrawable = ContextCompat.getDrawable(CommodityDetActivity.this,R.drawable.collectionde);
                                rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(),rightDrawable.getMinimumHeight());
                                F.id(R.id.txt_collect).getTextView().setCompoundDrawables(null, rightDrawable, null, null);
                            }

                            JSONObject goods = json.getJSONObject("goods");
                            HomeBean = (CommodityDetBean) Logic.getBeanToJSONObject(goods,new CommodityDetBean());
                            goods_id = HomeBean.goods_id;
                            F.id(R.id.txt_goods_name).text(HomeBean.goods_name);
                            F.id(R.id.txt_shop_price_rm).text("RM "+ HomeBean.shop_price_rm);
                            F.id(R.id.txt_current_price).text("RMB"+HomeBean.shop_price);
                            F.id(R.id.txt_sales_volume).text(getResources().getString(R.string.zx)+":\t"+HomeBean.sales_volume);
                            //url
                            String decodedString =new String(Base64.decode(HomeBean.goods_desc, Base64.DEFAULT));
                            webView.loadDataWithBaseURL("about:blank", decodedString, "text/html", "utf-8", null);
                            JSONObject img_list = goods.getJSONObject("img_list");
                            Iterator<String> iter3 = img_list.keys();
                            boolean isEd = true;
                            while(iter3.hasNext()){
                                String keyStr = iter3.next();
                                JSONObject im = img_list.getJSONObject(keyStr);
                                list_imag.add(im.getString("img_url"));
                                if (isEd ) {
                                    bottom_tit_url = list_imag.get(0);
                                    DownloadPicture.loadNetwork(im.getString("img_url"),image_name);
                                    isEd = false;
                                }
                            }
                            //加载头部图片
                            initRollViewPager();
                            String goods_attr_str = goods.getString("goods_attr_cat");
                            //判断属性是否为空
                            if (!TextUtils.isEmpty(goods_attr_str)&& !goods_attr_str.equals("null")) {
                                //加载购物车参数
                                JSONObject goods_attr_cat = goods.getJSONObject("goods_attr_cat");
                                String catName = getResources().getString(R.string.xz);
                                Iterator<String> iter = goods_attr_cat.keys();
                                goods_attr_cat_ls = new ArrayList<CommodityDetBean>();
                                while (iter.hasNext()) {
                                    String keyStr = iter.next();
                                    JSONObject jsonValue = goods_attr_cat.getJSONObject(keyStr);
                                    catName = catName + jsonValue.getString("catName") + ",";
                                    CommodityDetBean bean1 = new CommodityDetBean();
                                    bean1.catName = jsonValue.getString("catName");
                                    bean1.goods_attr_cat_key = keyStr;
                                    JSONObject imgIcon = null;
                                    if (jsonValue.has("imgIcon")) {
                                        imgIcon = jsonValue.getJSONObject("imgIcon");
                                    }
                                    JSONObject skuItem = jsonValue.getJSONObject("skuItem");
                                    Iterator<String> iterItem = skuItem.keys();
                                    bean1.listBeans = new ArrayList<CommodityDetBean>();
                                    while (iterItem.hasNext()) {
                                        CommodityDetBean beanItem = new CommodityDetBean();
                                        String keyStrItem = iterItem.next();
                                        String jsonValueItem = skuItem.getString(keyStrItem);
                                        if (null != imgIcon) {
                                            try {
                                                beanItem.imgIcon = imgIcon.getString(keyStrItem);
                                            }catch (Exception e){
                                                beanItem.imgIcon = "";
                                            }
                                        }
                                        beanItem.catName = jsonValueItem;
                                        beanItem.goods_attr_cat_key = keyStrItem;
                                        bean1.listBeans.add(beanItem);
                                    }
                                    goods_attr_cat_ls.add(bean1);
                                }


                                //将所有属性 默认选中 第一个
                                for (int i = 0 ; i<goods_attr_cat_ls.size();i++){
                                    goods_attr_cat_ls.get(i).listBeans.get(0).isSelect = true;
                                    String imgIcon =  goods_attr_cat_ls.get(i).listBeans.get(0).imgIcon;
                                    if (!TextUtils.isEmpty(imgIcon)) {
                                        bottom_tit_url = imgIcon;
                                    }
                                }


                                F.id(R.id.txt_cat_name).text(getResources().getString(R.string.xzspsx));
                            } else {
                                F.id(R.id.ll_select).visibility(View.GONE);
                                F.id(R.id.txt_one).visibility(View.GONE);
                                isGoodsAttr = true;
                            }
                            F.id(R.id.txt_china_freight).text(getResources().getString(R.string.china_freight)+": RM"+goods.getString("fee"));

                            //评论部分
                            JSONObject comment = json.getJSONObject("comment");
                            String total = comment.optString("total");
                            if (!TextUtils.isEmpty(total)&& !total.equals("0")) {
                                F.id(R.id.ll_comment_2).visibility(View.VISIBLE);
                                F.id(R.id.btn_look_over_all_evaluate).visibility(View.VISIBLE);
                                F.id(R.id.txt_no).visibility(View.GONE);
                                JSONObject first = comment.getJSONObject("firstcomment");
                                DownloadPicture.loadHearNetwork(first.getString("avatar"),F.id(R.id.image_head).getImageView());
                                F.id(R.id.txt_name).text(first.getString("user_name"));
                                F.id(R.id.txt_content).text(first.getString("content"));
                                F.id(R.id.txt_goods_attr).text(first.getString("add_time")+"\t"+first.getString("goods_attr"));
                                if (comment.has("hao")) {
                                    F.id(R.id.txt_hp).visibility(View.VISIBLE);
                                    F.id(R.id.txt_hp).text(getResources().getString(R.string.hp) + "(" + comment.getString("hao") + ")");
                                } else {
                                    F.id(R.id.txt_hp).visibility(View.GONE);
                                }
                                if (comment.has("zhong")) {
                                    F.id(R.id.txt_zp).visibility(View.VISIBLE);
                                    F.id(R.id.txt_zp).text(getResources().getString(R.string.zp)+"("+comment.getString("zhong")+")");
                                } else {
                                    F.id(R.id.txt_zp).visibility(View.GONE);
                                }
                                if (comment.has("cha")) {
                                    F.id(R.id.txt_cp).visibility(View.VISIBLE);
                                    F.id(R.id.txt_cp).text(getResources().getString(R.string.cp)+"("+comment.getString("cha")+")");
                                } else {
                                    F.id(R.id.txt_cp).visibility(View.GONE);
                                }
                            } else {
                                F.id(R.id.ll_comment_2).visibility(View.GONE);
                                F.id(R.id.btn_look_over_all_evaluate).visibility(View.GONE);
                                F.id(R.id.txt_no).visibility(View.VISIBLE);
                            }
                            F.id(R.id.txt_cowry_evaluate).text(getResources().getString(R.string.cowry_evaluate)+"("+total+")");
                        }  else if(state == 1){
                            U.Toast(CommodityDetActivity.this,getResources().getString(R.string.spbcz));
                        }else if(state == 2){
                            U.Toast(CommodityDetActivity.this,getResources().getString(R.string.spyxj));
                        }else if(state == 3){
                            U.Toast(CommodityDetActivity.this,getResources().getString(R.string.hqyc));
                        }
                    } catch (Exception e) {
                    }
                }
                goodsPromote();
//            loadingDalog.dismiss();
            }
            //加入购物车
            if (action == 1) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            if (null != mDialog) {
                                mDialog.dismiss();
                            }
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.jrgwccg));
                            //发送广播，通知首页的购物车刷新数据
                            Intent intent = new Intent(CommonUtil.SHOPING_CART);
                            LocalBroadcastManager.getInstance(CommodityDetActivity.this).sendBroadcast(intent);

                        } else if (state == 1) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.spyxj));
                        } else if (state == 5) {
                            U.Toast(CommodityDetActivity.this,  getResources().getString(R.string.gmsldykc));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
            //推荐商品
            if (action == 2) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            JSONArray goods = json.getJSONArray("goods");
                            List<GoodsPromoteBean> lists = Logic.getListToBean(goods,new GoodsPromoteBean());
                            adapter.setLists(lists);
                            adapter.notifyDataSetChanged();
                            sv_del.smoothScrollTo(0,0);
                            DensityUtil.setListViewHeightBasedOnChildren(lv_recommend);
                        } else if (state == 1) {
//                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.cscw));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
            //收藏接口
            if (action == 3) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            Drawable rightDrawable = ContextCompat.getDrawable(CommodityDetActivity.this,R.drawable.collectionde);
                            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(),rightDrawable.getMinimumHeight());
                            F.id(R.id.txt_collect).getTextView().setCompoundDrawables(null, rightDrawable, null, null);
                            isCollect = true;
                            U.Toast(CommodityDetActivity.this,getResources().getString(R.string.scspcg));
                        } else if (state == -1) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.scsb));
                        } else if (state == -2) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.spidwk));
                        } else if (state == 1) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.jqsb));
                        } else if (state == 2) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.wdlbnsc));
                        } else if (state == 3) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.ysc));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
            //取消收藏接口
            if (action == 4) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            Drawable rightDrawable = ContextCompat.getDrawable(CommodityDetActivity.this,R.drawable.collection);
                            rightDrawable.setBounds(0, 0, rightDrawable.getMinimumWidth(),rightDrawable.getMinimumHeight());
                            F.id(R.id.txt_collect).getTextView().setCompoundDrawables(null, rightDrawable, null, null);
                            isCollect = false;
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.qxsccg));
                        } else if (state == -1) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.scsb));
                        } else if (state == -2) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.spidwk));
                        } else if (state == 1) {
                            U.Toast(CommodityDetActivity.this,getResources().getString(R.string.jqsb));
                        } else if (state == 2) {
                            U.Toast(CommodityDetActivity.this, getResources().getString(R.string.wdlbnsc));
                        } else if (state == 3) {
                            U.Toast(CommodityDetActivity.this,  getResources().getString(R.string.ysc));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };


    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }
    private List<CommodityDetBean> goods_attr_cat_ls;
    private  CommodityDetBean HomeBean;
    //规格ID
    private String  selectCount = "";
    //判断组合参数是否齐全
    private boolean isProductPriceNull = false;
    //解析商品价格
    public void analysisGoodsRm() {
        selectCount = "";
        try {
            for (int i = 0; i < goods_attr_cat_ls.size(); i++) {
                CommodityDetBean bean1 = goods_attr_cat_ls.get(i);
                List<CommodityDetBean> list2= bean1.listBeans;
                for (int j = 0; j < list2.size(); j++) {
                    CommodityDetBean bean2 = list2.get(j);
                    if (bean2.isSelect) {
                        selectCount = selectCount + bean1.goods_attr_cat_key + ":" + bean2.goods_attr_cat_key + ";";
                    }
                }
            }
            selectCount = selectCount.substring(0,selectCount.length()-1);
            JSONObject goods_rm = new JSONObject(HomeBean.goods_attr_price_rm);
            JSONObject goods = new JSONObject(HomeBean.goods_attr_price);
            Iterator<String> iter = goods_rm.keys();
            Iterator<String> iter2 = goods.keys();
            while(iter.hasNext()){
                String keyStr = iter.next();
                String keyStr2 = iter2.next();
                String price  = goods_rm.getString(keyStr);
                String price2 = goods.getString(keyStr2);
                if (keyStr.indexOf(selectCount)!=-1) {
//                    txt_product_price.setText("RM"+price+"\t\t\t人民币"+price2);
                    txt_product_price.setText("RM\t"+price);
                    isProductPriceNull = true;
                    break;
                }
            }
        } catch (Exception e) {

        }

    }
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)) {
            if (!isCollect) {
                Intent intent = new Intent();
                intent.putExtra("22","22");
                this.setResult(0, intent);
            }
            finish();
            return false;
        }else {
            return super.onKeyDown(keyCode, event);
        }

    }

    /**
     * 打开图片查看器
     *
     * @param position
     */
    protected void imageBrower(int position) {
        Intent intent = new Intent(CommodityDetActivity.this, ImagePagerActivity.class);
        // 图片url,为了演示这里使用常量，一般从数据库中或网络中获取
        intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_URLS, list_imag);
        intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_INDEX, position);
        startActivity(intent);
    }

    /**
     * 消息数量
     */
    @Override
    public void onStart() {
        super.onStart();
        if (MianHomeFragment.message_count == 0) {
            F.id(rl_shape).visibility(View.GONE);
        } else {
//            F.id(rl_shape).visibility(View.VISIBLE);
        }
        F.id(R.id.txt_mess).text(""+MianHomeFragment.message_count);
    }

    public static  void setMessCount(int id) {
        if (null != txt_mess) {
            txt_mess.setText(""+MianHomeFragment.message_count);
            if (id == 0){
                rl_shape.setVisibility(View.GONE);
            } else {
//                rl_shape.setVisibility(View.VISIBLE);
            }
        }
    }

     @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
                 super.onActivityResult(requestCode, resultCode, data);
                 UMShareAPI.get( this ).onActivityResult( requestCode, resultCode, data);
            }

    //===================================商品详情翻译功能=======================================================
    public void translateDialog (int lang_id) {
            mDialog3 = new Dialog(this);
            mDialog3.requestWindowFeature(Window.FEATURE_NO_TITLE);
            View view = LayoutInflater.from(this).inflate(R.layout.view_translate, null);
            view.setBackgroundColor(Color.parseColor("#00000000"));
            WebView wv_translate = (WebView) view.findViewById(R.id.wv_translate);
            Button btn_dis = (Button)view.findViewById(R.id.btn_dis);
            btn_dis.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDialog3.dismiss();
                }
            });
            wv_translate.getSettings().setJavaScriptEnabled(true);
            WebSettings webSettings = wv_translate.getSettings();
            webSettings.setUseWideViewPort(true); // 将图片调整到适合webview的大小
            webSettings.setLoadWithOverviewMode(true); // 设置加载进来的页面自适应手机屏幕
            wv_translate.setWebViewClient(new A5bWebViewClient());
            //确定
            mDialog3.setContentView(view);
            mDialog3.setCanceledOnTouchOutside(true);
            Window dialogWindow = mDialog3.getWindow();
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = DensityUtil.getScreenHeight(this)-DensityUtil.dip2px(this, 50);
            lp.width = DensityUtil.getScreenWidth(this)-DensityUtil.dip2px(this, 50);
            SharedPreferences languagePre = getSharedPreferences("language_choice", Context.MODE_PRIVATE);
            wv_translate.loadUrl(Setting.APPGOODS+goods_id+"&lang="+lang_id);
            lp.gravity = Gravity.CENTER;
            dialogWindow.setAttributes(lp);
            dialogWindow.setBackgroundDrawableResource(R.color.transparent_all);

        mDialog3.show();

    }

    /**
     * 重写WebViewClient
     */
    private class A5bWebViewClient extends WebViewClient{

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.e("URL",url);
            view.loadUrl(url);//每次发起新的请求，在新的WebView中打开
            if (url.indexOf("goods_add:")!=-1) {
                String[] a = url.split("goods_add:");
                selectCount = a[1].substring(0, a[1].length() - 1);
                goods_num = 1;
                if (null != getUserBean()) {
                    joinShoppingCartData();
                } else {
                    U.Toast(CommodityDetActivity.this, getResources().getString(R.string.qdlzh));
                }
                mDialog3.dismiss();
            }
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view,url,favicon);
            Log.e("URL",url);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
//            dialog.dismiss();
            super.onPageFinished(view,url);
            Log.e("URL",url);
        }

        @Override
        public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view,handler,error);
        }

    }

    @Override
    public void onReturnSceneData(Scene scene) {
        // 处理场景还原数据, 更新画面
        U.Toast(CommodityDetActivity.this,"222222222222222");
    }
}
