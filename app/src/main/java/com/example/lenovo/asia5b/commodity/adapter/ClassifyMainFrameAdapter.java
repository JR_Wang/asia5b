package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.commodity.bean.ClassifyBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2017/6/23.
 */

public class ClassifyMainFrameAdapter  extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<ClassifyBean> lists;


    public ClassifyMainFrameAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<ClassifyBean>();
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_frame_classify_main, null);
            holder.txt_tit = (TextView)convertView.findViewById(R.id.txt_tit);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        ClassifyBean bean = lists.get(position);
        holder.txt_tit.setText(bean.getCat_name());
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_tit;

    }

    public void  setLists (List<ClassifyBean> lists) {
        this.lists = lists;
    }

    public List<ClassifyBean>  getLists () {
        return  lists;
    }
}

