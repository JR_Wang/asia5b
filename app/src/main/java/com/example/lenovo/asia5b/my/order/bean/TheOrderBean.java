package com.example.lenovo.asia5b.my.order.bean;

/**
 * Created by lenovo on 2017/7/10.
 */

public class TheOrderBean {
    public String order_id="";
    public String order_sn="";
    public String add_time="";
    public String order_status="";
    public String shipping_status="";
    public String pay_status="";
    public String mobile="";
    public String shipping_on="";
    public String goods_num="";
    public String thumb="";
    public String isshow="";
    public String isShowBill="";
    public String is_specail = "";
}
