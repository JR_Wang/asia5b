package com.example.lenovo.asia5b.my.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/7/25.
 */

public class ShippingPersonBean implements Serializable{
    public String contact_id;
    public String consignee;
    public String mobile;
    public String is_default;
}
