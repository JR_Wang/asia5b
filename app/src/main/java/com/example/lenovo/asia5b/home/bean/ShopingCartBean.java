package com.example.lenovo.asia5b.home.bean;

/**
 * Created by lenovo on 2017/6/28.
 */

public class ShopingCartBean {
    public String rec_id ;
    public String goods_id;
    public String goods_name ;
    public String goods_attr_thumb ;
    public String goods_price ;
    public String goods_number ;
    public String goods_attr ;

    public boolean isSelected = false;
    public boolean isState = false;
    //单个商品的初始数量
    public String initial_number;
    public String remark="";

}
