package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.view.View;

import com.example.lenovo.asia5b.my.order.bean.ParcelListBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.example.lenovo.asia5b.util.StringUtil;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.umeng.socialize.utils.DeviceConfig.context;


/**
 * Created by lenovo on 2017/10/30.
 */

public class CancelOrderActivity extends BaseActivity implements ICallBack {
    private AppCompatRadioButton btn_one,btn_two,btn_three;
    private LoadingDalog loadingDalog;
    private ParcelListBean bean;
    private String order_id;
    private boolean isCancel;
    private String order_status;
    private String add_time;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_order);
        initView();
    }
    public void initView() {
        loadingDalog = new LoadingDalog(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.qxdg));
        F.id(R.id.public_btn_left).clicked(this);
        if (getIntent().hasExtra("bean")) {
            bean = (ParcelListBean)getIntent().getSerializableExtra("bean");
            order_id = getIntent().getStringExtra("order_id");
            isCancel = getIntent().getBooleanExtra("isCancel",false);
            order_status = getIntent().getStringExtra("order_status");
            add_time = getIntent().getStringExtra("add_time");
            DownloadPicture.loadNetwork(bean.goods_attr_thumb,F.id(R.id.item_parcel_list_img).getImageView());
            F.id(R.id.item_parcel_list_goods_name).text(bean.goods_name);
            F.id(R.id.item_parcel_list_goods_number).text("x"+bean.goods_number);
            F.id(R.id.item_parcel_list_goods_pric).text("RM"+bean.goods_price);
            F.id(R.id.item_parcel_list_orderStatu).text("："+ StringUtil.orderStatu(CancelOrderActivity.this,bean.orderStatu,bean.fillStatu,bean.cancle_type,order_status,bean.express_code));//包裹状态
            F.id(R.id.item_parcel_list_add_timye).text("："+ DateUtils.timedate(add_time));
            F.id(R.id.txt_goods_id).text("："+bean.rec_id);
        }
        btn_one = (AppCompatRadioButton)findViewById(R.id.btn_one);
        btn_two = (AppCompatRadioButton)findViewById(R.id.btn_two);
        btn_three = (AppCompatRadioButton)findViewById(R.id.btn_three);
        F.id(R.id.btn_cancel_order).clicked(this);
        btn_one.setOnClickListener(this);
        btn_two.setOnClickListener(this);
        btn_three.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.public_btn_left:
                finish();
                break;
            case  R.id.btn_cancel_order:
                cancelOrderData();
                break;
            case  R.id.btn_one:
                F.id(R.id.edit_explain).visibility(View.GONE);
                break;
            case  R.id.btn_two:
                F.id(R.id.edit_explain).visibility(View.GONE);
                break;
            case  R.id.btn_three:
                F.id(R.id.edit_explain).visibility(View.VISIBLE);
                break;
            default:
                break;
        }

    }


    public void cancelOrderData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = "";
            if (isCancel) {
                //取消订购
                url = Setting.CANCEL_ORDER;
            } else {
                //补差价的取消订购
                url = Setting.CANCEL_PAY_GOODS;
            }
            String msg = "";
            if (btn_one.isChecked()) {
                msg = getResources().getString(R.string.wbxml);
            } else if (btn_two.isChecked()) {
                msg = getResources().getString(R.string.cxp);
            } else if (btn_three.isChecked()) {
                String explain =F.id(R.id.edit_explain).getText().toString().trim();
                if (TextUtils.isEmpty(explain)) {
                    explain = "其他原因";
                }
                msg = explain;
            } else {
                return;
            }
            loadingDalog.show();
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"order_id"+order_id,"rec_id"+bean.rec_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("msg",msg);
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("rec_id",bean.rec_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }

    }


    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            U.Toast(CancelOrderActivity.this, getResources().getString(R.string.qxcg));
                            Intent intent = new Intent();
                            intent.putExtra("222", 2222);
                            CancelOrderActivity.this.setResult(0, intent);
                            finish();
                        }

                        else if (state == 1) {
                            U.Toast(context, context.getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(context, context.getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(context, context.getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(context, context.getResources().getString(R.string.qxsb));
                        }

                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }
}
