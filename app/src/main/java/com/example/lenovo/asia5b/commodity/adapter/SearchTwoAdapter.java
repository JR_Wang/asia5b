package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.commodity.bean.SearchBean;

import java.util.ArrayList;
import java.util.List;

import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.util.DensityUtil;

/**
 * Created by lenovo on 2017/8/4.
 */

public class SearchTwoAdapter extends BaseAdapter
{

    private LayoutInflater mInflater;
    public List<SearchBean> lists;
    private  int width;
    private boolean isTwo;

    public SearchTwoAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<SearchBean>();
        width = DensityUtil.getScreenWidth(context) - 60;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_main_home_one, null);
            holder.txt_favorable_mom = (TextView) convertView.findViewById(R.id.txt_favorable_mom);
            holder.image_goods = (ImageView) convertView.findViewById(R.id.image_goods);
            holder.txt_name_goods = (TextView) convertView.findViewById(R.id.txt_name_goods);
            holder.txt_shop_price = (TextView) convertView.findViewById(R.id.txt_shop_price);
            holder.image_type = (ImageView)convertView.findViewById(R.id.image_type);
            convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }
        SearchBean bean = lists.get(position);
        holder.txt_favorable_mom.setText("RM" + bean.market_price_rm);
        holder.txt_favorable_mom.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.txt_name_goods.setText(bean.goods_name);
        holder.txt_shop_price.setText("RM" + bean.shop_price_rm);
        DownloadPicture.loadNetwork(bean.goods_thumb, holder.image_goods);
        LinearLayout.LayoutParams layoutParams = null;
        if (isTwo) {
            layoutParams = new LinearLayout.LayoutParams(width / 2, width / 2);
        } else {
            layoutParams = new LinearLayout.LayoutParams(width, width);
        }
        layoutParams.gravity = Gravity.CENTER_HORIZONTAL;
        holder.image_goods.setLayoutParams(layoutParams);
        holder.image_type.setVisibility(View.VISIBLE);
        if (bean.source.equals("1")){
            holder.image_type.setImageResource(R.drawable.icon_tb);
        } else if (bean.source.equals("2")){
            holder.image_type.setImageResource(R.drawable.icon_tm);
        }else if (bean.source.equals("3")){
            holder.image_type.setImageResource(R.drawable.icon_jd);
        }else if (bean.source.equals("4")){
            holder.image_type.setImageResource(R.drawable.icon_mgj);
        }else if (bean.source.equals("5")){
            holder.image_type.setImageResource(R.drawable.icon_mls);
        }else if (bean.source.equals("6")){
            holder.image_type.setImageResource(R.drawable.icon_pdd);
        }  else {
            holder.image_type.setVisibility(View.GONE);
        }
        return convertView;
    }

    private class ViewHolder {
        TextView txt_favorable_mom;
        ImageView image_goods,image_type;
        TextView txt_name_goods, txt_shop_price;

    }

    public void setLists(List<SearchBean> lists) {
        this.lists = lists;
    }

    public List<SearchBean> getLists() {
        return lists;
    }

    public void setIsTwo(boolean isTwo){
        this.isTwo = isTwo;
    }
}