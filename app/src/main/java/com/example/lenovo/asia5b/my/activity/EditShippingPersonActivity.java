package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.widget.AppCompatRadioButton;
import android.text.TextUtils;
import android.view.View;

import com.example.lenovo.asia5b.my.bean.ShippingPersonBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/7/25.
 */

public class EditShippingPersonActivity extends BaseActivity implements ICallBack {
    private AppCompatRadioButton rad_is_default ;
    private LoadingDalog loadingDalog;
    private ShippingPersonBean bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_shipping_person);
        initView();
    }

    private void initView(){
        loadingDalog = new LoadingDalog(EditShippingPersonActivity.this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.bjshr));
        rad_is_default = (AppCompatRadioButton)findViewById(R.id.rad_is_default);
        F.id(R.id.btn_add).clicked(this);
        F.id(public_btn_left).clicked(this);
        if (getIntent().hasExtra("bean")) {
            bean = (ShippingPersonBean)getIntent().getSerializableExtra("bean");

            F.id(R.id.edit_consignee).text(bean.consignee);
            F.id(R.id.edit_phone).text(bean.mobile);
            if (bean.is_default.equals("1")) {
                rad_is_default.setChecked(true);
            }
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;
            case R.id.btn_add:
                addData();
                break;
        }
    }

    /**
     * 添加地址
     */
    public void addData() {
        Map<String, String> parmaMap = new HashMap<String, String>();
        String url = Setting.UPDA_RESS;
        String user_id = getUserBean().user_id;
        //收货人
        String consignee =  F.id(R.id.edit_consignee).getText().toString();
        //手机
        String mobile =  F.id(R.id.edit_phone).getText().toString();
        //设置默认
        String is_default = "0";
        //1为默认
        if (rad_is_default.isChecked()) {
            is_default = "1";
        }
        //判断条件
        if (TextUtils.isEmpty(consignee)) {
            U.Toast(EditShippingPersonActivity.this, getResources().getString(R.string.add_address_hint_2));
        }else if (TextUtils.isEmpty(mobile)) {
            U.Toast(EditShippingPersonActivity.this,getResources().getString(R.string.my_data_hint_4));
        } else {
            try {
                loadingDalog.show();
                Date date = new Date();
                String time = String.valueOf(date.getTime());
                String[] sort = {"user_id" + user_id,"type"+2,"is_default"+is_default, "time" + time};
                String sortStr = Logic.sortToString(sort);
                String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
                parmaMap.put("user_id", user_id);
                parmaMap.put("type", 2+"");
                parmaMap.put("consignee", consignee);
                parmaMap.put("mobile", mobile);
                parmaMap.put("is_default", is_default);
                parmaMap.put("time", time);
                parmaMap.put("sign", md5_32);
                if (null !=bean ) {
                    parmaMap.put("contact_id", bean.contact_id);
                }
                JsonTools.getJsonAll(this, url, parmaMap, 1);
            } catch (Exception e) {

            }
        }

    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 1) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int)json.getInt("State");
                        if(state == 0) {
                            U.Toast(EditShippingPersonActivity.this,getResources().getString(R.string.bjcg));
                            Intent intent = new Intent();
                            intent.putExtra("22","22");
                            EditShippingPersonActivity.this.setResult(0, intent);
                            finish();
                        }else if(state == 1){
                            U.Toast(EditShippingPersonActivity.this,getResources().getString(R.string.qmsb));
                        }else if(state == 2){
                            U.Toast(EditShippingPersonActivity.this,getResources().getString(R.string.cscw));
                        }else if(state == 3){
                            U.Toast(EditShippingPersonActivity.this,getResources().getString(R.string.qsrshdz));
                        }else if(state == 4){
                            U.Toast(EditShippingPersonActivity.this,getResources().getString(R.string.my_data_hint_4));
                        }else if(state == 5){
                            U.Toast(EditShippingPersonActivity.this,getResources().getString(R.string.wdl));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };


    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }
}
