package com.example.lenovo.asia5b.util;

public class CommonUtil {
	
	public static String acversion = "2.1.4";//更新版本必换
	
	
	public static int CHOOSE_MAP_INT = 100;// 选择地图时回调标示

	public static int CHOOSE_CAMERA_INT = 101;// 拍照时回调标示
	public static int CHOOSE_IMAGE_INT = 102;// 选择图片时回调标示
	public static int CHOOSE_SCAN_INT = 103;// 扫码时回调标示
	public static int CHOOSE_EDITQR_INT = 104;// 手动生成二维码时回调标示

	public static int CHOOSE_VIDEO_INT = 105;// 选择视频时回调标示
	public static int CHOOSE_VOICE_INT = 106;// 选择音频时回调标示
	
	public static int CHOOSE_ADDRESS_INT = 107;// 选择地址时回调标示
	
	public static int CHOOSE_FILE_INT = 108;// 选择文件时回调标示

	public static String SHOPING_CART = "SHOPING_CART";//  通知购物车数量刷新
}
