package com.example.lenovo.asia5b.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.example.lenovo.asia5b.commodity.activity.ServiceActivity;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.SMSCode;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import app.MyApplication;
import fay.frame.ui.U;

/**
 * 注册
 * Created by lenovo on 2017/6/28.
 */

public class RegisterActivity extends BaseActivity implements ICallBack {

    private TimeCount timeCount;
    private long timeOut;

    private EditText activity_register_edit_phone, activity_register_edit_password, activity_register_edit_message;
    private CheckBox activity_register_edit_xieyi;
    private Button btn_test_get_code;
    private LoadingDalog loadingDalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }

    public void initView() {
        btn_test_get_code = findViewById(R.id.btn_test_get_code);
        loadingDalog = new LoadingDalog(RegisterActivity.this);
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.register));
        F.id(R.id.public_btn_left).clicked(this);
        F.id(R.id.btn_test_get_code).clicked(this);
        F.id(R.id.btn_register).clicked(this);
        F.id(R.id.ll_xy).clicked(this);
        activity_register_edit_phone = findViewById(R.id.activity_register_edit_phone);
        activity_register_edit_password = findViewById(R.id.activity_register_edit_password);
        activity_register_edit_message = findViewById(R.id.activity_register_edit_message);
        activity_register_edit_xieyi = findViewById(R.id.activity_register_edit_xieyi);
        activity_register_edit_xieyi.setChecked(true);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.public_btn_left://返回
                finish();
                break;
            case R.id.btn_immediately_on:
                refresh();
                break;
            case R.id.btn_test_get_code:
                String phone = activity_register_edit_phone.getText().toString();
                if (!TextUtils.isEmpty(phone)) {
                    if (phone.length() > 8) {
                        loadingDalog.show();
                        SMSCode.getCode(this, phone);
                    } else {
                        U.Toast(RegisterActivity.this, getResources().getString(R.string.my_data_hint_5));
                    }
                } else {
                    U.Toast(RegisterActivity.this, getResources().getString(R.string.sjhbnwk));
                }
                break;
            case R.id.btn_register:
                String code = activity_register_edit_message.getText().toString();
                if (!TextUtils.isEmpty(code)) {
                    if (timeOut <= 1) {
                        U.Toast(this, getResources().getString(R.string.yzmsx));
                    } else {
                        register(code);
                    }
                } else {
                    U.Toast(this, getResources().getString(R.string.yzmbnwk));
                }
                break;
            case R.id.ll_xy:
                intent = new Intent(this, ServiceActivity.class);
                intent.putExtra("xy", "1");
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    public void refresh() {
    }

    //注册
    public void register(String code) {
        try {
            String phone = activity_register_edit_phone.getText().toString();
            String password = activity_register_edit_password.getText().toString();
            String password2 = F.id(R.id.activity_register_edit_password_2).getText().toString();
            if (phone.length() < 8) {
                U.Toast(RegisterActivity.this, getResources().getString(R.string.my_data_hint_5));
                return;
            }
            if (!TextUtils.isEmpty(phone)) {
                if (!TextUtils.isEmpty(password)) {
                    if (!TextUtils.isEmpty(password2)) {
                        if (password.equals(password2)) {
                            if (password.length() >= 6) {
                                if (!TextUtils.isEmpty(code)) {
                                    if (activity_register_edit_xieyi.isChecked()) {
                                        String registerURL = Setting.REGISTER;
                                        Map<String, String> map = new HashMap<String, String>();
                                        map.put("phone", phone);
                                        map.put("password", password);
                                        map.put("code", code);
                                        map.put("user_alias", MyApplication.registrationId);
                                        Date date = new Date();
                                        String time = String.valueOf(date.getTime());
                                        map.put("time", time);

                                        String[] sortStr = {"phone" + phone, "password" + password, "code" + code, "time" + time, "user_alias" + MyApplication.registrationId};
                                        String sort = Logic.sortToString(sortStr);
                                        String md5 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sort));
                                        map.put("sign", md5);

                                        JsonTools.getJsonAll(this, registerURL, map, 1);
                                        loadingDalog.show();
                                    } else {
                                        U.Toast(this, getResources().getString(R.string.qydyhxy));
                                    }
                                } else {
                                    U.Toast(this, getResources().getString(R.string.yzmbnwk));
                                }
                            } else {
                                U.Toast(this, getResources().getString(R.string.mmcdbxdy6));
                            }
                        } else {
                            U.Toast(this, getResources().getString(R.string.lcsrdmmbxd));
                        }
                    } else {
                        U.Toast(this, getResources().getString(R.string.qrmmbnwk));
                    }
                } else {
                    U.Toast(this, getResources().getString(R.string.login_hint_2));
                }
            } else {
                U.Toast(this, getResources().getString(R.string.sjhbnwk));
            }
        } catch (Exception e) {
//            Log.e("注册接口error",e.getMessage());
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result = null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            Intent intent = null;
            try {
                if (action == 0) {
                    if (null != result && result instanceof JSONObject) {
                        JSONObject json = (JSONObject) result;
                        String msgText = json.getString("MsgText");
                        if (TextUtils.equals("true", msgText)) {
                            timeCount = new TimeCount(60000, 1000);
                            timeCount.start();
                        } else if (TextUtils.equals("false", msgText)) {
                            U.Toast(RegisterActivity.this, json.getString("State"));
                        }
                    } else {
                        U.Toast(RegisterActivity.this, getResources().getString(R.string.dysb)
                        );
                    }
                    loadingDalog.dismiss();
                } else if (action == 1) {
                    if (null != result && result instanceof JSONObject) {
                        JSONObject json = (JSONObject) result;
                        String state = json.getString("State");
                        if (TextUtils.equals("0", state)) {
//                            JPushInterface.setAlias(RegisterActivity.this, "asia5b_" + json.getString("user_id"), RegisterActivity.this);
                            intent = new Intent(RegisterActivity.this, UploadHeadPortraitActivity.class);
                            intent.putExtra("user_id", json.getString("user_id"));
                            intent.putExtra("phone",activity_register_edit_phone.getText().toString());
                            intent.putExtra("register", "register");
                            startActivity(intent);
                        } else if (TextUtils.equals("1", state)) {
                            U.Toast(RegisterActivity.this, getResources().getString(R.string.qmsb)
                            );
                        } else if (TextUtils.equals("2", state)) {
                            U.Toast(RegisterActivity.this, getResources().getString(R.string.gsjyzc));
                        } else if (TextUtils.equals("3", state)) {
                            U.Toast(RegisterActivity.this, getResources().getString(R.string.login_hint_2));
                        } else if (TextUtils.equals("4", state)) {
                            U.Toast(RegisterActivity.this, getResources().getString(R.string.sjyzmcw));
                        } else if (TextUtils.equals("4", state)) {
                            U.Toast(RegisterActivity.this, getResources().getString(R.string.zcsb));
                        } else if (TextUtils.equals("15", state)) {
                            U.Toast(RegisterActivity.this, getResources().getString(R.string.yzmsx));
                        }else if (TextUtils.equals("10", state)) {
                            U.Toast(RegisterActivity.this,getResources().getString(R.string.hqyc));
                        }
                    } else {
                        U.Toast(RegisterActivity.this, getResources().getString(R.string.dysb));
                    }
                }
                loadingDalog.dismiss();
            } catch (Exception e) {
                Log.e("jsonError", e.getMessage());
            }
        }
    };


    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 获取验证码倒计时
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            btn_test_get_code.setClickable(false);
            timeOut = millisUntilFinished / 1000;
            btn_test_get_code.setText(getResources().getString(R.string.message_has_been_sent) + "(" + timeOut + ")");
        }

        @Override
        public void onFinish() {
            btn_test_get_code.setText(getResources().getString(R.string.to_obtain_the_verification_code));
            btn_test_get_code.setClickable(true);
        }
    }
}
