package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.order.adapter.TimeLineAdapter;
import com.example.lenovo.asia5b.my.order.bean.LogisticsBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/7/22.
 */

public class LogisticsActivity extends BaseActivity implements ICallBack {
    private TimeLineAdapter adapter;
    private LoadingDalog loadingDalog;
    private List<LogisticsBean> lists = null;
    private String express_code;
    private LayoutInflater mInflater;
    private LinearLayout ll_home;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logistics);
        initView();
        logisticsData();
    }
    public void initView() {
        if (getIntent().hasExtra("express_code")){
            express_code = getIntent().getStringExtra("express_code");
        }
        F.id(R.id.public_title_name).text(getResources().getString(R.string.wlxx));
        F.id(public_btn_left).clicked(this);
        loadingDalog = new LoadingDalog(this);
        loadingDalog.show();
        adapter = new TimeLineAdapter(this);
        ll_home = (LinearLayout)findViewById(R.id.ll_home);
        this.mInflater = LayoutInflater.from(LogisticsActivity.this);

    }

    /**
     * 获取我的物流数据
     */
    public void logisticsData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.ORDER_SHIPPING;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"express_code"+express_code,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("express_code",express_code);
            parmaMap.put("user_id", user_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            JSONArray shipping = json.getJSONArray("shipping");
                            addHome(shipping);
                        }
                        else if (state == 1) {
                            U.Toast(LogisticsActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(LogisticsActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(LogisticsActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(LogisticsActivity.this, getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);

    }
    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                finish();
                break;

            default:
                break;
        }
    }


    /**
     * 添加标题
     */
    public void addHome(JSONArray jsonArray) {

        try {
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject  = jsonArray.getJSONObject(i);
                View convertView= mInflater.inflate(R.layout.item_merge_payments_tit, null);
                TextView txt_title = (TextView)convertView.findViewById(R.id.txt_title);
                CheckBox cb_pro_checkbox = (CheckBox) convertView.findViewById(R.id.cb_pro_checkbox);
                LinearLayout ll_title = (LinearLayout)convertView.findViewById(R.id.ll_title);
                final int pos = i;
                txt_title.setText(getResources().getString(R.string.order_number) +":\t"+jsonObject.getString("shipping_on"));
                cb_pro_checkbox.setVisibility(View.GONE);
                initTitView(ll_title,jsonObject.getJSONArray("shipping_info"));
                ll_home.addView(convertView);
            }
        } catch (Exception e) {

        }
    }

    /**
     * 添加内容
     * @param ll_title
     */
    public void initTitView(LinearLayout ll_title,JSONArray jsonArray) {
        try {
            for (int i = 0; i < jsonArray.length(); i ++) {
                View convertView = mInflater.inflate(R.layout.item_logistics, null);
                TextView title = (TextView) convertView.findViewById(R.id.txt_title);
                TextView time = (TextView) convertView.findViewById(R.id.txt_time);
                ImageView image_dot = (ImageView)convertView.findViewById(R.id.image_dot);
                title.setText(jsonArray.getString(i));
                if (i == 0){
                    image_dot.setImageResource(R.drawable.dot_yellow);
                }else{
                    image_dot.setImageResource(R.drawable.dot);
                }
                ll_title.addView(convertView);
            }
        } catch (Exception e) {

        }
    }


}
