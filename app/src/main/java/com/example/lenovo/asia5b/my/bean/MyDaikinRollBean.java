package com.example.lenovo.asia5b.my.bean;

/**
 * Created by lenovo on 2017/7/15.
 */

public class MyDaikinRollBean {
    public String bonus_id;
    public String used_time;
    public String order_sn;
    public String type_money;
    public String use_start_date;
    public String use_end_date;
    public String code;
}
