package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.MyDaikinRollBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.DensityUtil;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;



/**
 * Created by lenovo on 2017/8/21.
 */

public class MyDaikinRollTwoAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<MyDaikinRollBean> lists ;
    private long time;
    private Context context;
    private   int width;

    public MyDaikinRollTwoAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<MyDaikinRollBean>();
        Date date = new Date();
        time = date.getTime();
        this.context = context;
        width = DensityUtil.getScreenWidth(context);
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_my_daikin_roll_two, null);
            holder.txt_type_money = (TextView)convertView.findViewById(R.id.txt_type_money);
            holder.txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txt_order_sn = (TextView)convertView.findViewById(R.id.txt_order_sn);
            holder.image_used= (ImageView) convertView.findViewById(R.id.image_used);
            holder.txt_used_time = (TextView)convertView.findViewById(R.id.txt_used_time);
            holder.txt_code = (TextView)convertView.findViewById(R.id.txt_code);

            holder.ll_code = (LinearLayout)convertView.findViewById(R.id.ll_code);
            holder.ll_time = (LinearLayout)convertView.findViewById(R.id.ll_time);
            holder.ll_order_sn = (LinearLayout)convertView.findViewById(R.id.ll_order_sn);
            holder.ll_used_time = (LinearLayout)convertView.findViewById(R.id.ll_used_time);
            holder.txt_sy = (TextView) convertView.findViewById(R.id.txt_sy);
            convertView.setTag(holder);

        }else {

            holder = (ViewHolder)convertView.getTag();
        }
        MyDaikinRollBean bean = lists.get(position);
        holder.txt_type_money.setText("RM"+bean.type_money);
        if (!TextUtils.isEmpty(bean.use_start_date)) {
            String use_start_date =  DateUtils.timedate(bean.use_start_date);
            String use_end_date = DateUtils.timedate(bean.use_end_date);
            holder.txt_time.setText(use_start_date +context.getResources().getString(R.string.z) + use_end_date);
            holder.ll_time.setVisibility(View.VISIBLE);
        } else {
            holder.ll_time.setVisibility(View.GONE);
        }
        if (TextUtils.isEmpty(bean.order_sn)||TextUtils.equals(bean.order_sn,"null")) {
            holder.ll_order_sn.setVisibility(View.GONE);
        } else {
            holder.txt_order_sn.setText(bean.order_sn);
            holder.ll_order_sn.setVisibility(View.VISIBLE);
        }
        if (!bean.used_time.equals("0")) {
            holder.txt_used_time.setText( DateUtils.timedate(bean.used_time));
            holder.ll_used_time.setVisibility(View.VISIBLE);
            holder.txt_sy.setVisibility(View.GONE);
        } else {
            holder.ll_used_time.setVisibility(View.GONE);
            holder.txt_sy.setVisibility(View.VISIBLE);
        }
        if (!TextUtils.isEmpty(bean.code)) {
            holder.txt_code.setText(bean.code);
            holder.ll_code.setVisibility(View.VISIBLE);
        } else {
            holder.ll_code.setVisibility(View.GONE);
        }
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_type_money,txt_time,txt_order_sn,txt_used_time,txt_code,txt_sy;
        ImageView image_used;
        LinearLayout ll_code,ll_time,ll_order_sn,ll_used_time;
    }

    public void setLists(List<MyDaikinRollBean> lists) {
        this.lists = lists;
    }

    public List<MyDaikinRollBean> getLists() {
        return lists;
    }
}
