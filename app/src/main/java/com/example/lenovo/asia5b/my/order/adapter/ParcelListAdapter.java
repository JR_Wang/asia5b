package com.example.lenovo.asia5b.my.order.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.commodity.activity.AddEvalutatActivity;
import com.example.lenovo.asia5b.commodity.activity.CommodityDetActivity;
import com.example.lenovo.asia5b.my.order.activity.ApplyRefundActivity;
import com.example.lenovo.asia5b.my.order.activity.CancelOrderActivity;
import com.example.lenovo.asia5b.my.order.activity.MakeMoneyActivity;
import com.example.lenovo.asia5b.my.order.activity.ParcelListActivity;
import com.example.lenovo.asia5b.my.order.bean.ParcelListBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.PublicDialog;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.example.lenovo.asia5b.util.StringUtil;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;


/**
 * Created by lenovo on 2017/6/24.
 */

public class ParcelListAdapter extends BaseAdapter implements ICallBack {
    private LoadingDalog loadingDalog;
    private LayoutInflater mInflater;
    private Context context;
    private List<ParcelListBean> parcelList;
    private String add_time;
    private String order_status,order_id,mobile,order_sn;

    public ParcelListAdapter(Context context,String order_status,String order_id,String mobile,String order_sn){
        this.context =context;
        this.mInflater = LayoutInflater.from(context);
        this.order_status = order_status;
        this.order_id = order_id;
        this.mobile = mobile;
        this.order_sn = order_sn;
        loadingDalog = new LoadingDalog(context);
    }
    @Override
    public int getCount() {
        return parcelList.size();
    }

    @Override
    public Object getItem(int arg0) {
        return parcelList.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {
            holder=new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_parcel_list, null);
            holder.ll_parcel = (LinearLayout)convertView.findViewById(R.id.ll_parcel);
            holder.item_parcel_list_goods_name = convertView.findViewById(R.id.item_parcel_list_goods_name);
            holder.item_parcel_list_goods_number = convertView.findViewById(R.id.item_parcel_list_goods_number);
            holder.item_parcel_list_goods_pric = convertView.findViewById(R.id.item_parcel_list_goods_pric);
            holder.item_parcel_list_add_timye = convertView.findViewById(R.id.item_parcel_list_add_timye);
            holder.item_parcel_list_orderStatu = convertView.findViewById(R.id.item_parcel_list_orderStatu);
            holder.item_parcel_list_img = convertView.findViewById(R.id.item_parcel_list_img);
            holder.btn_cancel = convertView.findViewById(R.id.btn_cancel);

            holder.btn_make_money = convertView.findViewById(R.id.btn_make_money);
            holder.btn_apply_for = convertView.findViewById(R.id.btn_apply_for);
            holder.btn_check_refund = convertView.findViewById(R.id.btn_check_refund);
            holder.btn_evaluate = convertView.findViewById(R.id.btn_evaluate);
            holder.txt_goods_id = convertView.findViewById(R.id.txt_goods_id);
            holder.btn_cancel_refund = convertView.findViewById(R.id.btn_cancel_refund);
            holder.btn_loss_return = convertView.findViewById(R.id.btn_loss_return);
            holder.btn_loss_reorder = convertView.findViewById(R.id.btn_loss_reorder);
            holder.item_parcel_list_orderStatu_2 = convertView.findViewById(R.id.item_parcel_list_orderStatu_2);
            convertView.setTag(holder);
        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        ParcelListBean parcelListBean = parcelList.get(position);
        DownloadPicture.loadNetwork(parcelListBean.goods_attr_thumb,holder.item_parcel_list_img);
        holder.item_parcel_list_goods_name.setText(parcelListBean.goods_name);
        holder.item_parcel_list_goods_number.setText("x"+parcelListBean.goods_number);
        holder.item_parcel_list_goods_pric.setText("RM"+parcelListBean.goods_price);
        holder.item_parcel_list_orderStatu.setText("："+ StringUtil.orderStatu(context,parcelListBean.orderStatu,parcelListBean.fillStatu,parcelListBean.cancle_type,order_status,parcelListBean.express_code));//包裹状态
        holder.item_parcel_list_add_timye.setText("："+ DateUtils.timedate(add_time));

        holder.ll_parcel.setOnClickListener(new ParcelListClick(parcelListBean));
        //申请退货
        if (parcelListBean.orderStatu.equals("8")){
            holder.btn_apply_for.setVisibility(View.VISIBLE);
            holder.btn_apply_for.setOnClickListener(new ApplyForClick(parcelListBean,true));

        } else {
            holder.btn_apply_for.setVisibility(View.GONE);
        }
        //查看退货
        if (parcelListBean.orderStatu.equals("9")||parcelListBean.orderStatu.equals("15")||parcelListBean.orderStatu.equals("17")){
            holder.btn_check_refund.setVisibility(View.VISIBLE);
            holder.btn_check_refund.setOnClickListener(new ApplyForClick(parcelListBean,false));
        } else {
            holder.btn_check_refund.setVisibility(View.GONE);
        }
        //撤销退货
        if (parcelListBean.orderStatu.equals("9")) {
            holder.btn_cancel_refund.setVisibility(View.VISIBLE);
            holder.btn_cancel_refund.setOnClickListener(new CancelRefundClick(position));
        } else {
            holder.btn_cancel_refund.setVisibility(View.GONE);
        }
        //补差价
        if (parcelListBean.orderStatu.equals("13")&& !parcelListBean.price_difference.equals("0")&&parcelListBean.is_give_difference.equals("1")) {
            holder.btn_make_money.setOnClickListener(new MakeMoneyClick(parcelListBean));
            holder.btn_cancel.setOnClickListener(new CancelOrderClick(position,false));
            holder.btn_make_money.setVisibility(View.VISIBLE);
            holder.btn_cancel.setVisibility(View.VISIBLE);
        } else  if (parcelListBean.orderStatu.equals("14")) {
            holder.btn_make_money.setOnClickListener(new MakeMoneyClick(parcelListBean));
            holder.btn_cancel.setOnClickListener(new CancelOrderClick(position,false));
            holder.btn_make_money.setVisibility(View.VISIBLE);
            holder.btn_cancel.setVisibility(View.VISIBLE);

        } else {
            holder.btn_make_money.setVisibility(View.GONE);
            holder.btn_cancel.setVisibility(View.GONE);
        }
        if (holder.btn_make_money.getVisibility() ==View.GONE){
            //取消订购
            if (!order_status.equals("2")) {
                if (parcelListBean.orderStatu.equals("0") || parcelListBean.orderStatu.equals("1")) {
                    holder.btn_cancel.setVisibility(View.VISIBLE);
                    holder.btn_cancel.setOnClickListener(new CancelOrderClick(position,true));
                } else {
                    holder.btn_cancel.setVisibility(View.GONE);
                }
            } else {
                holder.btn_cancel.setVisibility(View.GONE);
            }
        }
        //评价
        if (parcelListBean.is_comment.equals("0")){
            if (parcelListBean.orderStatu.equals("10")|| parcelListBean.orderStatu.equals("15")||parcelListBean.orderStatu.equals("17")){
                holder.btn_evaluate.setVisibility(View.VISIBLE);
                holder.btn_evaluate.setOnClickListener(new EvaluateClick(parcelListBean));
            } else {
                holder.btn_evaluate.setVisibility(View.GONE);
            }
        } else {
            holder.btn_evaluate.setVisibility(View.GONE);
        }

        //直接退款
        if (!parcelListBean.loss_num.equals("0")& parcelListBean.loss_state.equals("0")) {
            holder.btn_loss_return.setVisibility(View.VISIBLE);
            holder.btn_loss_reorder.setVisibility(View.VISIBLE);
            holder.btn_loss_return.setOnClickListener(new LossReturnClick(position));
            holder.btn_loss_reorder.setOnClickListener(new LossReorderClick(position));
        } else {
            holder.btn_loss_return.setVisibility(View.GONE);
            holder.btn_loss_reorder.setVisibility(View.GONE);
        }
        if (!parcelListBean.loss_num.equals("0")){
            holder.item_parcel_list_orderStatu_2.setVisibility(View.VISIBLE);
            if(parcelListBean.loss_state.equals("1")) {
                holder.item_parcel_list_orderStatu_2.setText("("+
                        context.getResources().getString(R.string.ystq)+":"+parcelListBean.loss_num+")");

            } else if(parcelListBean.loss_state.equals("2") || parcelListBean.loss_state.equals("3")) {
                holder.item_parcel_list_orderStatu_2.setText("("+
                        context.getResources().getString(R.string.cxdg)+":"+parcelListBean.loss_num+")");
            }
            else {
                holder.item_parcel_list_orderStatu_2.setText("("+
                        context.getResources().getString(R.string.ys)+":"+parcelListBean.loss_num+")");
            }
        } else {
            holder.item_parcel_list_orderStatu_2.setVisibility(View.GONE);
        }

        holder.txt_goods_id.setText("："+parcelListBean.rec_id);
        return convertView;
    }

    private class ViewHolder {
        LinearLayout ll_parcel;
        TextView item_parcel_list_goods_name,item_parcel_list_goods_number,item_parcel_list_goods_pric,
                item_parcel_list_add_timye,item_parcel_list_orderStatu,txt_goods_id;
        ImageView item_parcel_list_img;
        TextView btn_cancel,btn_make_money,btn_apply_for,btn_check_refund,btn_evaluate,btn_cancel_refund
                ,btn_loss_return,btn_loss_reorder,item_parcel_list_orderStatu_2;
    }



    public void setParcelList(List<ParcelListBean> parcelList){
        this.parcelList = parcelList;
    }
    public List<ParcelListBean> getParcelList(){
        return parcelList;
    }

    public void setAddTimye(String add_time){
        this.add_time = add_time;
    }

    class ParcelListClick implements View.OnClickListener {
        ParcelListBean parcelListBean;
        public  ParcelListClick(ParcelListBean parcelListBean) {
            this.parcelListBean = parcelListBean;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, CommodityDetActivity.class);
            intent.putExtra("goods_id",parcelListBean.goods_id);
            context.startActivity(intent);
        }
    }

    /**
     * 撤销退货
     */
    class CancelRefundClick implements View.OnClickListener {
        int position;
        public  CancelRefundClick( int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            isCancelDialog(position);
        }
    }

    /**
     * 取消订购
     */
    class CancelOrderClick implements View.OnClickListener {
        int position;
        boolean isCancel;
        public  CancelOrderClick( int position,boolean isCancel) {
            this.position = position;
            this.isCancel = isCancel;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, CancelOrderActivity.class);
            intent.putExtra("bean",parcelList.get(position));
            intent.putExtra("order_id",order_id);
            intent.putExtra("isCancel",isCancel);
            intent.putExtra("order_status",order_status);
            intent.putExtra("add_time",add_time);
            ((ParcelListActivity)context).startActivityForResult(intent,0);
//            isDeleteDialog(position,isCancel);
        }
    }

    /**
     * 补差价
     */
    class MakeMoneyClick implements View.OnClickListener{
        ParcelListBean bean;
        public  MakeMoneyClick( ParcelListBean bean) {
            this.bean = bean;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, MakeMoneyActivity.class);
            intent.putExtra("order_id",order_id);
            intent.putExtra("rec_id",bean.rec_id);
            ((ParcelListActivity)context).startActivityForResult(intent,0);
        }
    }

    /**
     * 申请退货和查看退货
     */
    class ApplyForClick implements View.OnClickListener{
        ParcelListBean bean;
        boolean isApplyFor ;
        public  ApplyForClick(ParcelListBean bean, boolean isApplyFor) {
            this.bean = bean;
            this.isApplyFor =isApplyFor;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, ApplyRefundActivity.class);
            intent.putExtra("bean",bean);
            intent.putExtra("order_id",order_id);
            intent.putExtra("order_sn",order_sn);
            intent.putExtra("rec_id",bean.rec_id);
            intent.putExtra("add_time",add_time);
            intent.putExtra("mobile",mobile);
            if (!isApplyFor) {
                intent.putExtra("check_refund",mobile);
            }
            ((ParcelListActivity)context).startActivityForResult(intent,0);
        }
    }

    /**
     * 评价
     */
    class EvaluateClick implements View.OnClickListener {
        ParcelListBean bean;
        public  EvaluateClick(ParcelListBean bean) {
            this.bean = bean;
        }

        @Override
        public void onClick(View view) {
            Intent intent = new Intent(context, AddEvalutatActivity.class);
            intent.putExtra("bean",bean);
            intent.putExtra("order_id",order_id);
            intent.putExtra("rec_id",bean.rec_id);
            ((ParcelListActivity)context).startActivityForResult(intent,0);
        }
    }

    /**
     * 直接退款
     */
    public class  LossReturnClick implements View.OnClickListener {
        int position;
        public  LossReturnClick( int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            lossReturnDialog(position);
        }
    }


    /**
     * 重新订购
     */
    public class  LossReorderClick implements View.OnClickListener {
        int position;
        public  LossReorderClick( int position) {
            this.position = position;
        }

        @Override
        public void onClick(View view) {
            lossReorderDialog(position);
        }
    }

    public void cancelOrderData(int position,boolean isCancel) {
        try {
            loadingDalog.show();
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = "";
            if (isCancel) {
                //取消订购
                url = Setting.CANCEL_ORDER;
            } else {
                //补差价的取消订购
                url = Setting.CANCEL_PAY_GOODS;
            }
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"order_id"+order_id,"rec_id"+parcelList.get(position).rec_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("rec_id",parcelList.get(position).rec_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }

 }

    /**
     * 撤销退货
     * @param position
     */
    public void cancelRefundData(int position) {
        try {
            loadingDalog.show();
            Map<String, String> parmaMap = new HashMap<String, String>();
                //撤销退货
            String    url = Setting.CANCEL_REFUND;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"order_id"+order_id,"rec_id"+parcelList.get(position).rec_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("rec_id",parcelList.get(position).rec_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 1);
        }catch (Exception e) {

        }

    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            U.Toast(context, context.getResources().getString(R.string.qxcg));
                            ((ParcelListActivity) context).initPostURL();
                            ((ParcelListActivity) context).isSelect = true;
                        }

                        else if (state == 1) {
                            U.Toast(context, context.getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(context, context.getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(context, context.getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(context, context.getResources().getString(R.string.qxsb));
                        }

                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }else   if (action == 1) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            U.Toast(context, context.getResources().getString(R.string.cg));
                            ((ParcelListActivity) context).initPostURL();
                            ((ParcelListActivity) context).isSelect = true;
                        }
                        else if (state == 1) {
                            U.Toast(context, context.getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(context, context.getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(context, context.getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(context, context.getResources().getString(R.string.qxsb));
                        }

                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 弹出删除消息
     */
    public void isDeleteDialog(final int position,final boolean isCancel) {
        final PublicDialog dialog = new PublicDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.xtts));// 设置title
        dialog.setContent(context.getResources().getString(R.string.qdqxdg));// 设置内容
        dialog.setLeftButton(context.getResources().getString(R.string.qx));// 设置按钮
        dialog.setRightButton(context.getResources().getString(R.string.confirm));
        dialog.setLeftButtonVisible(true); // true显示 false隐藏
        dialog.setRightButtonVisible(true);// true显示 false隐藏
        dialog.setRightButtonClick(new PublicDialog.OnClickListener() {

            @Override
            public void onClick(View v) {
                //调用删除消息接口
                cancelOrderData(position,isCancel);
            }
        });
        dialog.setLeftButtonClick(new PublicDialog.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismissDialog();
            }
        });
        dialog.showDialog();
    }

    /**
     * 弹出撤销退货消息
     */
    public void isCancelDialog(final int position) {
        final PublicDialog dialog = new PublicDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.xtts));// 设置title
        dialog.setContent(context.getResources().getString(R.string.qdcxdg));// 设置内容
        dialog.setLeftButton(context.getResources().getString(R.string.qx));// 设置按钮
        dialog.setRightButton(context.getResources().getString(R.string.confirm));
        dialog.setLeftButtonVisible(true); // true显示 false隐藏
        dialog.setRightButtonVisible(true);// true显示 false隐藏
        dialog.setRightButtonClick(new PublicDialog.OnClickListener() {

            @Override
            public void onClick(View v) {
                //调用删除消息接口
                cancelRefundData(position);
            }
        });
        dialog.setLeftButtonClick(new PublicDialog.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismissDialog();
            }
        });
        dialog.showDialog();
    }


    /**
     * 弹出撤销退款消息
     */
    public void  lossReturnDialog(final int position) {
        final PublicDialog dialog = new PublicDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.xtts));// 设置title
        dialog.setContent(context.getResources().getString(R.string.qdtksp));// 设置内容
        dialog.setLeftButton(context.getResources().getString(R.string.qx));// 设置按钮
        dialog.setRightButton(context.getResources().getString(R.string.confirm));
        dialog.setLeftButtonVisible(true); // true显示 false隐藏
        dialog.setRightButtonVisible(true);// true显示 false隐藏
        dialog.setRightButtonClick(new PublicDialog.OnClickListener() {

            @Override
            public void onClick(View v) {
                //调用撤销退款接口
                lossReturnData(position);
            }
        });
        dialog.setLeftButtonClick(new PublicDialog.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismissDialog();
            }
        });
        dialog.showDialog();
    }

    /**
     * 弹出重新订购消息
     */
    public void  lossReorderDialog(final int position) {
        final PublicDialog dialog = new PublicDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.xtts));// 设置title
        dialog.setContent(context.getResources().getString(R.string.qdcxdgsp));// 设置内容
        dialog.setLeftButton(context.getResources().getString(R.string.qx));// 设置按钮
        dialog.setRightButton(context.getResources().getString(R.string.confirm));
        dialog.setLeftButtonVisible(true); // true显示 false隐藏
        dialog.setRightButtonVisible(true);// true显示 false隐藏
        dialog.setRightButtonClick(new PublicDialog.OnClickListener() {

            @Override
            public void onClick(View v) {
                //调用删除消息接口
                lossReorderData(position);
            }
        });
        dialog.setLeftButtonClick(new PublicDialog.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismissDialog();
            }
        });
        dialog.showDialog();
    }

    /**
     * 撤销退款
     * @param position
     */
    public void lossReturnData(int position) {
        try {
            loadingDalog.show();
            Map<String, String> parmaMap = new HashMap<String, String>();
            //撤销退货
            String    url = Setting.LOSS_RETURN;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"rec_id"+parcelList.get(position).rec_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("rec_id",parcelList.get(position).rec_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 1);
        }catch (Exception e) {

        }

    }


    /**
     * 重新订购
     * @param position
     */
    public void lossReorderData(int position) {
        try {
            loadingDalog.show();
            Map<String, String> parmaMap = new HashMap<String, String>();
            //撤销退货
            String    url = Setting.LOSS_REORDER;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"rec_id"+parcelList.get(position).rec_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("rec_id",parcelList.get(position).rec_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 1);
        }catch (Exception e) {

        }

    }
}