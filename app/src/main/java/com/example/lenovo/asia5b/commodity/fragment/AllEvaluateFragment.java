package com.example.lenovo.asia5b.commodity.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.lenovo.asia5b.commodity.activity.EvaluateDetActivity;
import com.example.lenovo.asia5b.commodity.adapter.AllEvaluateAdapter;
import com.example.lenovo.asia5b.commodity.bean.AllEvaluateBean;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseFragment;
import fay.frame.ui.U;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.PullToRefreshLayout;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;

/**
 * Created by lenovo on 2017/8/10.
 */

public class AllEvaluateFragment extends BaseFragment  implements ICallBack, PullToRefreshLayout.OnRefreshListener {
    private AllEvaluateAdapter adapter;
    private ListView lv_all_evaluate;
    private int page  = 1;
    private int countPage = 0;
    private PullToRefreshLayout pullToRefreshLayout;
    private LoadingDalog loadingDalog;
    private List<AllEvaluateBean> lists = null;
    private String goods_id="";
    private View fragmentView;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.frame_all_evaluate, container,
                false);
        initView();
        AllEvaluateData();
        return fragmentView;
    }

    public void setGooodId(String goods_id) {
        this.goods_id = goods_id;
    }

    public void initView() {
        loadingDalog = new LoadingDalog(getActivity());
        loadingDalog.show();
        lv_all_evaluate = (ListView)fragmentView.findViewById(R.id.lv_all_evaluate);
        adapter = new AllEvaluateAdapter(getActivity());
        pullToRefreshLayout = ((PullToRefreshLayout)fragmentView.findViewById(R.id.refresh_view));
        pullToRefreshLayout.setOnRefreshListener(this);
        lv_all_evaluate.setAdapter(adapter);
        lv_all_evaluate.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(getActivity(),EvaluateDetActivity.class);
                AllEvaluateBean bean  = (AllEvaluateBean) adapterView.getAdapter().getItem(i);
                intent.putExtra("comment_id",bean);
                startActivityForResult(intent,0);
            }
        });
        lv_all_evaluate.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
            }
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        if (page < countPage) {
                            page += 1;
                            AllEvaluateData();
                        } else {
                            TextView load_tv = (TextView)fragmentView.findViewById(R.id.load_tv);
                            load_tv.setText(getResources().getString(R.string.no_more_data));
                            load_tv.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
    }

    public void AllEvaluateData(){
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.GOODS_COMMENLIST;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"goods_id"+goods_id,"type"+0,"page"+page,"time"+time};
            String user_id = getUserBean().user_id;
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("goods_id", goods_id);
            parmaMap.put("user_id",user_id);
            parmaMap.put("type","0");
            parmaMap.put("page",""+page);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            JSONObject paging = json.getJSONObject("paging");
                            if (null!=paging.getString("page") && "1".equals(paging.getString("page"))) {
                                pullToRefreshLayout.refreshFinish(PullToRefreshLayout.SUCCEED);
                                initPaging(paging);
                                JSONArray account = json.getJSONArray("comments");
                                lists = Logic.getListToBean(account,new AllEvaluateBean());
                                adapter.setLists(lists);
                                adapter.notifyDataSetChanged();
                            } else if(null!=paging.getString("page") && !"1".equals(paging.getString("page"))){
                                pullToRefreshLayout.loadmoreFinish(PullToRefreshLayout.SUCCEED);
                                JSONArray account = (JSONArray) json.get("comments");
                                if (null != account && account.length() > 0) {
                                    List<AllEvaluateBean> listBean  =  Logic.getListToBean(account,new AllEvaluateBean());
                                    for (int i = 0; i < listBean.size(); i++) {
                                        lists .add(listBean.get(i));
                                    }
                                    adapter .setLists(lists);
                                    adapter .notifyDataSetChanged();
                                } else {
                                    page = page - 1;
                                }

                            }
                        }  else if(state == 1){
                            U.Toast(getActivity(),getResources().getString(R.string.qmsb));
                        }else if(state == 2){
                            U.Toast(getActivity(),getResources().getString(R.string.cscw));
                        }else if(state == 3){
                            U.Toast(getActivity(),getResources().getString(R.string.wdl));
                        }else if(state == 4){
                            U.Toast(getActivity(),getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    public void initPaging(JSONObject json) throws JSONException {
        String pageSizeString = json.getString("count");
        String countPageString = json.getString("countpage");
        String pageString = json.getString("page");
        if (pageSizeString != null && countPageString != null && pageString != null) {
            countPage = Integer.parseInt(countPageString);
            if (countPage <=1) {
                pullToRefreshLayout.stopLoadMore();
            }
        }
    }

    /**
     * 向下刷新
     */
    @Override
    public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
        loadingDalog.show();
        page = 1;
        AllEvaluateData();
    }

    /**
     * 向上加载
     */
    @Override
    public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
        pullToRefreshLayout.loadmoreFinish(PullToRefreshLayout.SUCCEED);
    }

    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case 0:
                if (data == null) {
                    return;
                }
                loadingDalog.show();
                AllEvaluateData();
                break;
            default:
                break;
        }
    }
}
