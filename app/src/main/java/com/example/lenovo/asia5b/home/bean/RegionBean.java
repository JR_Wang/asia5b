package com.example.lenovo.asia5b.home.bean;

import java.util.List;

/**
 * Created by lenovo on 2017/7/10.
 */

public class RegionBean {
    public  String region_id;
    public  String parent_id;
    public  String region_name;
    public  String region_type;
    public List<RegionBean> chile;

}
