package com.example.lenovo.asia5b.my.order.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.order.bean.MakeMoneyBean;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;

import com.example.lenovo.asia5b.ui.DownloadPicture;

/**
 * Created by lenovo on 2017/7/27.
 */

public class MakeMoneyAdapter extends BaseAdapter

{
    private Context context;
    private List<MakeMoneyBean> lists = null;
    private LayoutInflater mInflater;

    public MakeMoneyAdapter(Context context) {
        super();
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<MakeMoneyBean>();
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TimeLineHolder holder = null;
        if (convertView == null) {
            holder = new TimeLineHolder();
            convertView = mInflater.inflate(R.layout.item_confirm_order, null);
            holder.txt_goods_name = (TextView)convertView.findViewById(R.id.txt_goods_name);
            holder.txt_goods_attr_h = (TextView)convertView.findViewById(R.id.txt_goods_attr_h);
            holder.txt_goods_price = (TextView)convertView.findViewById(R.id.txt_goods_price);
            holder.txt_goods_number = (TextView)convertView.findViewById(R.id.txt_goods_number);
            holder.image_goods_attr_thumb = (ImageView)convertView.findViewById(R.id.image_goods_attr_thumb);
            holder.ll_china_fee = (LinearLayout)convertView.findViewById(R.id.ll_china_fee);
            holder.ll_weight = (LinearLayout)convertView.findViewById(R.id.ll_weight);
            convertView.setTag(holder);
        } else {
            holder = (TimeLineHolder) convertView.getTag();
        }
        MakeMoneyBean bean = lists.get(position);

        holder.txt_goods_name.setText(bean.goods_name);
        holder.txt_goods_attr_h.setText("");
        holder.txt_goods_number.setText("x"+bean.goods_number);
        holder.txt_goods_price.setText(Html.fromHtml("<font color='#666666'>"+context.getResources().getString(R.string.bspjg)+"</font>"+":RM"+ bean.price_difference));
        DownloadPicture.loadNetwork(bean.goods_attr_thumb, holder.image_goods_attr_thumb);
        holder.ll_china_fee.setVisibility(View.GONE);
        holder.ll_weight.setVisibility(View.GONE);
        return convertView;

    }

    static class TimeLineHolder{
        TextView txt_goods_name,txt_goods_attr_h,txt_goods_price,txt_goods_number;
        ImageView image_goods_attr_thumb;
        LinearLayout ll_china_fee,ll_weight;
    }


    public void setLists(List<MakeMoneyBean> lists) {
        this.lists = lists;
    }

    public List<MakeMoneyBean> getLists() {
        return lists;
    }

}
