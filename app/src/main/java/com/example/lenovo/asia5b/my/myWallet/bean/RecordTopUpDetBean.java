package com.example.lenovo.asia5b.my.myWallet.bean;

/**
 * Created by lenovo on 2017/7/12.
 */

public class RecordTopUpDetBean {
    public String id;
    public String amount;
    public String amount_account;
    public String is_paid;
    public String add_time;
    public String paid_time;
    public String time;
    public String serial;
    public String type;
    public String bank;
}
