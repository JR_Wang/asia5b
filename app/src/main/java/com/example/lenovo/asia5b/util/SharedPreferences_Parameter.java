package com.example.lenovo.asia5b.util;

public class SharedPreferences_Parameter {
	// 是否登录过
	public static String LC_SESSIONID = "SESSIONID";
	// 是否登录过
	public static String LC_ISLOGGED = "LC_ISLOGGED";
	// 是否登录过IM
	public static String IM_ISLOGGED = "IM_ISLOGGED";
	// 是否匿名
	public static String LOGGED_PASSWORD= "LOGGED_PASSWORD";
	// 是否匿名
	public static String LC_ISANONYMOUS= "LC_ISANONYMOUS";
	// 登录用户信息
	public static String LC_USERBEAN = "LC_USERBEAN";
	// 预订单信息
	public static String LC_ORDERTEMPBEAN = "LC_ORDERTEMPBEAN";
	// 订单信息
	public static String LC_ORDERBEAN = "LC_ORDERBEAN";

	/**
	 * 上次进入APP的时间
	 */
	public static String YH_LAST_ENTER_APP_TIME = "YH_LAST_ENTER_APP_TIME";
	// 点赞列表
	public static String ZHIKU_AGREE = "ZHIKU_AGREE";

	// 收藏列表
	public static String LC_FAVORITE = "LC_FAVORITE";

	public static String LC_AGREE = "LC_AGREE";
	// 系统报错
	public static String LC_ERRORLOG = "LC_ERRORLOG";
	
	//我的购物车ID
	public static String MY_CART_ID = "MY_CART_ID";
	
	// 点赞列表
	public static String LC_PARISE = "LC_PARISE";
//	// 装修状态
//	public static String LC_HOUSESTATUSCODE = "LC_HOUSESTATUSCODE";
//	// 户型
//	public static String LC_HOUSEMODELCODE = "LC_HOUSEMODELCODE";
//	// 装修风格
//	public static String LC_DECORATIONSTYLECODE = "LC_DECORATIONSTYLECODE";
//	
	
	//成员贡献 开发类型
	public static String YH_DEVELOPTYPECODE = "YH_DEVELOPTYPECODE";
    public static String YH_DFSTATUSTYPECODE = "YH_DFSTATUSTYPECODE";
    public static String YH_DEVELOPSTATUSCODE = "YH_DEVELOPSTATUSCODE";
    //腾讯信鸽推送tokenid
 	public static String YH_XG_TOKENID = "YH_XG_TOKENID";
 	//是否是第一次进入ａｐｐ
 	public static String YH_ISFIRST_LOGIN = "YH_ISFIRST_LOGIN";
 	//首页资讯
 	public static String YH_MAIN_NEWS = "YH_MAIN_NEWS";
 	//第二页滚动页
 	public static String YH_SECOND_HEADLINE= "YH_SECOND_HEADLINE";
 	//第二页案例列表
 	public static String YH_SECOND_CASE= "YH_SECOND_CASE";
 	
 	//出门
 	public static String YH_USERDAILYCHECKIN_GO_OUT = "YH_USERDAILYCHECKIN_GO_OUT";
	//签到
 	public static String YH_USERDAILYCHECKIN_SIGN_IN = "YH_USERDAILYCHECKIN_SIGN_IN";
 	//签退
 	public static String YH_USERDAILYCHECKIN_SIGN_OUT = "YH_USERDAILYCHECKIN_SIGN_OUT";
 	//到家
 	public static String YH_USERDAILYCHECKIN_GET_HOME = "YH_USERDAILYCHECKIN_GET_HOME";
 	//checkinId
 	public static String YH_USERDAILYCHECKIN_CHECKIN_ID = "YH_USERDAILYCHECKIN_CHECKIN_ID";
 	//备忘录
 	public static String YH_USERDAILYCHECKIN_CHECKIN_REMARK = "YH_USERDAILYCHECKIN_CHECKIN_REMARK";
 	//上班routeId
 	public static String YH_USERDAILYCHECKIN_ROUTE_AT_WORK_ID = "YH_USERDAILYCHECKIN_ROUTE_AT_WORK_ID";
 	//下班routeId
 	public static String YH_USERDAILYCHECKIN_ROUTE_OFF_WORK_ID = "YH_USERDAILYCHECKIN_ROUTE_OFF_WORK_ID";
 	//单车
 	public static String YH_USERDAILYCHECKIN_BIKE = "YH_USERDAILYCHECKIN_BIKE";
	//地铁
 	public static String YH_USERDAILYCHECKIN_TRAIN = "YH_USERDAILYCHECKIN_TRAIN";
	//步行
 	public static String YH_USERDAILYCHECKIN_WALK = "YH_USERDAILYCHECKIN_WALK";
 	//公交
 	 public static String YH_USERDAILYCHECKIN_BUS = "YH_USERDAILYCHECKIN_BUS";
 	 //打的
 	 public static String YH_USERDAILYCHECKIN_TAXI = "YH_USERDAILYCHECKIN_TAXI";
 	 //自驾
 	 public static String YH_USERDAILYCHECKIN_CAR = "YH_USERDAILYCHECKIN_CAR";
 	 //用户交通选择
 	 public static String YH_USERDAILYCHECKIN_SELECT_TRAFFIC = "YH_USERDAILYCHECKIN_SELECT_TRAFFIC";
 	//判断是否卸载过的,为空就说明这个APP是第一次使用签到。有可能是刚使用，也有可能刚卸载再使用
 	public static String YH_USERDAILYCHECKIN_IS_UNLOAD = "YH_USERDAILYCHECKIN_IS_UNLOAD";
 	/**
 	 * 商品列表，判断用户上次操作是列表浏览还是瀑布流浏览使用
 	 */
 	public static String YH_USER_VIEW_PRODUCT_INFORMATION = "YH_USER_VIEW_PRODUCT_INFORMATION";
 	//搜索歷史
 	public static String YH_HISTORY_SEARCH="YH_HISTORY_SEARCH";
 	/**
 	 * 商品搜索历史纪录
 	 */
 	public static String YH_HISTORY_PRODUCT_RECORDING="YH_HISTORY_PRODUCT_RECORDING";
 	//购物车未选中状态
 	public static String USERCART_INDEX_NOT_SELECTED = "USERCART_INDEX_NOT_SELECTED";
 	/**
 	 * 搜索历史纪录
 	 */
 	public static String YH_HISTORY_CMS_RECORDING="YH_HISTORY_CMS_RECORDING";
 	//保存新闻资讯的用户选择
 	public static String CMS_MAIN_INDEX_LIST = "CMS_MAIN_INDEX_LIST";
 	/**首页城市定位历史**/
 	public static String YH_HISTORY_INDEX_POSATION ="YH_HISTORY_INDEX_POSATION";
 	/**首页城市定位使用**/
 	public static String YH_INDEX_POSATION_CITY ="YH_INDEX_POSATION_CITY";
 	
 	//BBS
	public static final String userChooesedSectionKey = "userChooesedSectionKey";
	public static final String sectionListJSONKey = "sectionListJSONKey";

	public static final String YH_MAIN_NEWS_DATA = "YH_MAIN_NEWS_DATA";//首页新闻缓存
	public static final String YH_MAIN_ADVENTURE = "YH_MAIN_ADVENTURE";//首页广告图
	public static final String YH_MAIN_RECOMMAND = "YH_MAIN_RECOMMAND";//首页热门推荐
	public static final String YH_MAIN_FREE = "YH_MAIN_FREE";//首页课堂
	public static final String YH_MAIN_TOPIC = "YH_MAIN_TOPIC";//首页专题
}
