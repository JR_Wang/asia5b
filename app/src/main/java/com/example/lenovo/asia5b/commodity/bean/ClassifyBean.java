package com.example.lenovo.asia5b.commodity.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2017/7/5.
 */

public class ClassifyBean implements Serializable {
    private  String cat_id;
    private String cat_name;
    private String fileimg;
    private String fileico;

    private List<ClassifyTwoBean> childs = new ArrayList<>() ;
    private List<ClassifyThreeBean> threeChilds = new ArrayList<>() ;

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getFileimg() {
        return fileimg;
    }

    public void setFileimg(String fileimg) {
        this.fileimg = fileimg;
    }

    public String getFileico() {
        return fileico;
    }

    public void setFileico(String fileico) {
        this.fileico = fileico;
    }

    public List<ClassifyTwoBean> getChilds() {
        return childs;
    }

    public void setChilds(List<ClassifyTwoBean> childs) {
        this.childs = childs;
    }

    public List<ClassifyThreeBean> getThreeChilds() {
        return threeChilds;
    }

    public void setThreeChilds(List<ClassifyThreeBean> threeChilds) {
        this.threeChilds = threeChilds;
    }

    @Override
    public String toString() {
        return "ClassifyBean{" +
                "cat_id='" + cat_id + '\'' +
                ", cat_name='" + cat_name + '\'' +
                ", fileimg='" + fileimg + '\'' +
                ", fileico='" + fileico + '\'' +
                ", childs=" + childs +
                ", threeChilds=" + threeChilds +
                '}';
    }
}
