package com.example.lenovo.asia5b.commodity.bean;

/**
 * Created by lenovo on 2017/7/17.
 */

public class SearchBean {

    public  String goods_id;
    public  String goods_name;
    public  String sales_volume;
    public  String shop_price;
    public  String shop_price_rm;
    public  String market_price;
    public  String market_price_rm;
    public  String goods_thumb;
    public  String comments_number;
    public  String source="";
}
