package com.example.lenovo.asia5b.commodity.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Setting;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;
import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;

/**
 * Created by lenovo on 2017/8/5.
 */

public class ServiceActivity  extends BaseActivity implements ICallBack{
    private WebView webView;
    private String art_id="1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card_url);
        initView();
    }

    public void initView() {
        F.id(public_btn_left).clicked(this);
        webView = (WebView)findViewById(R.id.webView);
        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);//支持javaScript
        webSettings.setDefaultTextEncodingName("utf-8");//设置网页默认编码
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.NARROW_COLUMNS);//可能的话使所有列的宽度不超过屏幕宽度
        webSettings.setUseWideViewPort(true);//设置webview推荐使用的窗口
        webSettings.setLoadWithOverviewMode(true);//设置加载进来的页面自适应手机屏幕
//        webView.setWebChromeClient(new MyWebChromeClient());// 设置浏览器可弹窗
        //覆盖WebView默认使用第三方或系统默认浏览器打开网页的行为，使网页用WebView打开
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                //返回值是true的时候控制去WebView打开，为false调用系统浏览器或第三方浏览器
                view.loadUrl(url);
                return true;
            }
            @Override
            public void onPageStarted(WebView view, String url,
                                      Bitmap favicon) {
                //支付完成后,点返回关闭界面
                if(url.endsWith("http://120.1.1.1/xxx/xx/xxx")){
                    finish();
                }
                super.onPageStarted(view, url, favicon);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
            }
        });

        if (getIntent().hasExtra("xy")) {
            String xy = getResources().getString(R.string.yhxy);
            xy = xy.substring(0,xy.length()-1);
            xy = xy.substring(1,xy.length());
            F.id(R.id.public_title_name).text(xy);
            art_id = getIntent().getStringExtra("xy");
            xyData();

        } else {
            F.id(R.id.public_title_name).text(getResources().getString(R.string.kflx));
            udeskData();
        }
    }

    public void xyData() {
        Map<String, String> parmaMap = new HashMap<String, String>();
        String url = Setting.GETURL;
        parmaMap.put("art_id", art_id);
        JsonTools.getJsonAll(this, url, parmaMap, 0);
    }
    public void udeskData(){
        Map<String, String> parmaMap = new HashMap<String, String>();
        String url = Setting.JS_UDESK;
        String user_id = getUserBean().user_id;
        parmaMap.put("user_id", user_id);
        JsonTools.getJsonAll(this, url, parmaMap, 1);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            String images =json.getString("images");
                            webView.loadUrl(images);
                        }
                    } catch (Exception e) {

                    }
                }
            }
            else if (action == 1) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            String udesk_jscode =json.getString("udesk_jscode");
                            webView.loadUrl(udesk_jscode);
                        }
                    } catch (Exception e) {

                    }
                }
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }
}
