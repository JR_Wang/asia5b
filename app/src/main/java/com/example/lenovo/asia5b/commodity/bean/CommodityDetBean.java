package com.example.lenovo.asia5b.commodity.bean;

import java.util.List;

/**
 * Created by lenovo on 2017/7/11.
 */

public class CommodityDetBean {
    public  String goods_id;
    public  String goods_name;
    public  String market_price;
    public  String shop_price;
    public  String goods_number;
    public  String goods_desc;
    public  String sales_volume;
    public  String shop_price_rm;
    public  String fee;
    public  String market_price_rm;
    public  String goods_attr_cat;
    public  String goods_attr_price;
    public  String goods_attr_price_rm;
    public  String img_list;
    public  String fx_url;


    public String img_id;
    public String img_url;
    public String thumb_url;
    public String img_desc;

    public String goods_attr_cat_key;
    public String catName;
    public String skuItem;
    public String imgIcon;

    public List<CommodityDetBean> listBeans ;

    public boolean isSelect;

}
