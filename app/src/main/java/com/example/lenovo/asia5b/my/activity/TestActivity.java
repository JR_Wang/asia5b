package com.example.lenovo.asia5b.my.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.lenovo.asia5b.my.adapter.TestAdapter;
import com.example.lenovo.asia5b.my.bean.TestBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.PullToRefreshLayout;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;
import static com.wushi.lenovo.asia5b.R.id.public_title_name;

/**
 * Created by apple on 2017/11/11.
 */



public class TestActivity extends BaseActivity implements ICallBack,PullToRefreshLayout.OnRefreshListener{

    private  ListView lv_message;
    private LoadingDalog loadingDalog;
    private PullToRefreshLayout pullToRefreshLayout;
    private List<TestBean> lists = null;
    private TestAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jrtest);
        initView();
//        testRequest();





    }

    public void initView() {
        loadingDalog = new LoadingDalog(this);
        F.id(public_title_name).text("MyTestActivity");
        F.id(public_btn_left).clicked(this);
        pullToRefreshLayout = ((PullToRefreshLayout)findViewById(R.id.refresh_view));
        pullToRefreshLayout.setOnRefreshListener(this);
        lv_message = (ListView)findViewById(R.id.lv_message);
        adapter = new TestAdapter(this);
        lv_message.setAdapter(adapter);
        lv_message.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.e("fejjijin:",i+"");
            }
        });

        lists = new ArrayList<TestBean>();
        for (int i = 0; i < 9; i++){
            TestBean aa = new TestBean();

            lists.add(aa);
        }

        adapter.setLists(lists);
        adapter.notifyDataSetChanged();


    }


    public void testRequest(){
        loadingDalog.show();
        Map<String, String> parmaMap = new HashMap<String, String>();
        String url = Setting.Test_Url;
//        String user_id = getUserBean().user_id;
//        Date date = new Date();
//        String time = String.valueOf(date.getTime());
//        String[] sort = {"user_id"+user_id,"page"+page,"time"+time};
//        String sortStr = Logic.sortToString(sort);
//        String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
//        parmaMap.put("user_id", user_id);
//        parmaMap.put("page",""+page);
//        parmaMap.put("time",time);
//        parmaMap.put("sign",md5_32);
        JsonTools.getJsonAll(this, url, null, 0);


    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {

//

                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
                pullToRefreshLayout.refreshFinish(PullToRefreshLayout.SUCCEED);
            }
            if (action == 1) {

            }
        }
    };



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;

        }
    }

    public void logicFinish(Object result, int action)
    {

    }


    @Override
    public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {

    }

    @Override
    public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {

    }
}

