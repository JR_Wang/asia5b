package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.commodity.bean.ClassifyMainSonBean;

import java.util.ArrayList;
import java.util.List;

import com.example.lenovo.asia5b.ui.DownloadPicture;

/**
 * Created by lenovo on 2017/7/19.
 */

public class ClassifyMainSonAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private int cur_pos = 0;// 当前显示的一行
    private Context context;
    private List<ClassifyMainSonBean> lists ;



    public ClassifyMainSonAdapter(Context context) {
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<ClassifyMainSonBean>();
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder = new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_recommend, null);
            holder.image_thumb = (ImageView)convertView.findViewById(R.id.image_thumb);
            holder.txt_goods_name = (TextView) convertView.findViewById(R.id.txt_goods_name);
            holder.txt_market_price_rm = (TextView) convertView.findViewById(R.id.txt_market_price_rm);
            holder.txt_shop_price_rm = (TextView) convertView.findViewById(R.id.txt_shop_price_rm);
            holder.txt_sales_volume = (TextView) convertView.findViewById(R.id.txt_sales_volume);
            holder.image_type = (ImageView)convertView.findViewById(R.id.image_type);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ClassifyMainSonBean bean = lists.get(position);
        DownloadPicture.loadNetwork(bean.goods_thumb,holder.image_thumb);
        holder.txt_goods_name.setText(bean.goods_name);
        holder.txt_market_price_rm.setText("RM"+bean.market_price_rm);
        holder.txt_market_price_rm.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG );
        holder.txt_shop_price_rm.setText("RM"+bean.shop_price_rm);
        holder.txt_sales_volume.setText(bean.sales_volume);
        holder.image_type.setVisibility(View.VISIBLE);
        if (bean.source.equals("1")){
            holder.image_type.setImageResource(R.drawable.icon_tb);
        } else if (bean.source.equals("2")){
            holder.image_type.setImageResource(R.drawable.icon_tm);
        }else if (bean.source.equals("3")){
            holder.image_type.setImageResource(R.drawable.icon_jd);
        }else if (bean.source.equals("4")){
            holder.image_type.setImageResource(R.drawable.icon_mgj);
        }else if (bean.source.equals("5")){
            holder.image_type.setImageResource(R.drawable.icon_mls);
        } else if (bean.source.equals("6")){
            holder.image_type.setImageResource(R.drawable.icon_pdd);
        }else {
            holder.image_type.setVisibility(View.GONE);
        }
        return convertView;
    }

    private class ViewHolder {
        ImageView image_thumb,image_type;
        TextView txt_goods_name,txt_market_price_rm,txt_shop_price_rm,txt_sales_volume;
    }


    public void  setLists (List<ClassifyMainSonBean> lists) {
        this.lists = lists;
    }

    public List<ClassifyMainSonBean>  getLists () {
        return  lists;
    }
}
