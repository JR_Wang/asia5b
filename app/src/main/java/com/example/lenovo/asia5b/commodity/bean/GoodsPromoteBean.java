package com.example.lenovo.asia5b.commodity.bean;

/**
 * //推荐商品
 * Created by lenovo on 2017/7/13.
 */

public class GoodsPromoteBean {
    public String goods_id;
    public String goods_name;
    public String goods_thumb;
    public String market_price;
    public String shop_price;
    public String sales_volume;
    public String source="";
    public String market_price_rm;
    public String shop_price_rm;

}
