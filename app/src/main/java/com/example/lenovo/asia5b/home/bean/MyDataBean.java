package com.example.lenovo.asia5b.home.bean;

/**
 * Created by lenovo on 2017/7/10.
 */

public class MyDataBean {
    public String user_name="";
    public String nickname="";
    public String alias="";
    public String sex="";
    public String birthday="";
    public String constell="";
    public String live_country="";
    public String live_province="";
    public String live_city="";
    public String home_country="";
    public String home_province="";
    public String home_city="";
    public String email="";
    public String mobile_phone="";
    public String avatar="";
    public String constell_id="";
    public String live_country_id="";
    public String live_province_id="";
    public String home_country_id="";
    public String home_province_id="";
}
