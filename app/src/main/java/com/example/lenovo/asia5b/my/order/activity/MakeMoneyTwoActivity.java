package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.order.bean.MakeMoneyTwoBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;


/**
 * Created by lenovo on 2017/8/22.
 */

public class MakeMoneyTwoActivity extends BaseActivity implements ICallBack {
    private LoadingDalog loadingDalog;
    private String order_id;
    private List<MakeMoneyTwoBean> lists = null;
    private LinearLayout ll_home;
    private LayoutInflater mInflater;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_money_two);
        initView();
        orderDifferData();
    }
    public void initView() {
        loadingDalog = new LoadingDalog(this);
        loadingDalog.show();
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.bcj));
        ll_home = (LinearLayout)findViewById(R.id.ll_home);
        this.mInflater = LayoutInflater.from(MakeMoneyTwoActivity.this);
        F.id(public_btn_left).clicked(this);
        if (getIntent().hasExtra("order_id")){
            order_id = getIntent().getStringExtra("order_id");
        }
    }

    /**
     * 获取我的消息数据
     */
    public void orderDifferData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.ORDER_DIFFER;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"order_id"+order_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }

    }


    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                finish();
                break;
            default:
                break;
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            JSONArray account = json.getJSONArray("diff");
                            lists = Logic.getListToBean(account,new MakeMoneyTwoBean());
                            addHome();
                        }  else if(state == 1){
                            U.Toast(MakeMoneyTwoActivity.this,getResources().getString(R.string.qmsb));
                        }else if(state == 2){
                            U.Toast(MakeMoneyTwoActivity.this,getResources().getString(R.string.cscw));
                        }else if(state == 3){
                            U.Toast(MakeMoneyTwoActivity.this,getResources().getString(R.string.wdl));
                        }else if(state == 4){
                            U.Toast(MakeMoneyTwoActivity.this,getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 添加标题
     */
    public void addHome() {
        for (int i = 0; i < lists.size(); i++) {
            final MakeMoneyTwoBean bean = lists.get(i);
            View convertView= mInflater.inflate(R.layout.activity_make_money_two_son, null);
            TextView txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            TextView txt_cn_freight = (TextView)convertView.findViewById(R.id.txt_cn_freight);
            TextView txt_heji = (TextView)convertView.findViewById(R.id.txt_heji);
            TextView txt_tax_diff = (TextView)convertView.findViewById(R.id.txt_tax_diff);
            TextView txt_pack_diff = (TextView)convertView.findViewById(R.id.txt_pack_diff);
            TextView txt_integral_money = (TextView)convertView.findViewById(R.id.txt_integral_money);
            TextView txt_bonus = (TextView)convertView.findViewById(R.id.txt_bonus);
            TextView txt_amount = (TextView)convertView.findViewById(R.id.txt_amount);
            LinearLayout ll_title = (LinearLayout)convertView.findViewById(R.id.ll_title);
            txt_time.setText(getResources().getString(R.string.time)+":\t"+ DateUtils.timedate(bean.add_time));
            txt_cn_freight.setText(bean.cn_freight);
            txt_heji.setText(bean.heji);
            txt_tax_diff.setText(bean.tax_diff);
            txt_pack_diff.setText(bean.pack_diff);
            txt_integral_money.setText(bean.integral_money);
            txt_bonus.setText(bean.bonus);
            txt_amount.setText(bean.amount);
            initTitView(ll_title,bean.goods_list);
            ll_home.addView(convertView);
        }
    }

    /**
     * 添加内容
     * @param ll_title
     * @param lists
     */
    public void initTitView(LinearLayout ll_title, String lists) {
        try {
            JSONArray jsonArray = new JSONArray(lists);
            for (int i = 0; i < jsonArray.length(); i ++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                //单个包裹
                View hotSaleView = mInflater.inflate(R.layout.item_merge_payments, null);
                CheckBox cb_pro_checkbox = (CheckBox) hotSaleView.findViewById(R.id.cb_pro_checkbox);
                ImageView image_name = (ImageView) hotSaleView.findViewById(R.id.image_name);
                TextView txt_name = (TextView) hotSaleView.findViewById(R.id.txt_name);
                TextView txt_price_hi = (TextView) hotSaleView.findViewById(R.id.txt_price_hi);
                TextView txt_price = (TextView) hotSaleView.findViewById(R.id.txt_price);
                TextView txt_num = (TextView) hotSaleView.findViewById(R.id.txt_num);
                txt_name.setText(jsonObject.getString("goods_name"));
                txt_price_hi.setText(getResources().getString(R.string.bspjg));
                txt_price.setText("RM" + jsonObject.getString("price_diff"));
                DownloadPicture.loadNetwork(jsonObject.getString("goods_attr_thumb"), image_name);
                txt_num.setText("x" + jsonObject.getString("goods_number"));
                cb_pro_checkbox.setVisibility(View.GONE);
                ll_title.addView(hotSaleView);
            }
        } catch (Exception e) {

        }
    }
}
