package com.example.lenovo.asia5b.util;

import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Base64;

import com.example.lenovo.asia5b.home.bean.UserBean;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.util.List;

import app.MyApplication;

public class SharedPreferencesUtils {
    public static final String PREFS_NAME = "com.example.lenovo.myapplication";
    private static SharedPreferences preferences;

    private static void init() {
        if (preferences == null) {
            preferences = MyApplication.getInstance().getSharedPreferences(PREFS_NAME, 0);
        }
    }

    public static boolean getBoolean(String key, boolean defValue) {
        init();
        return preferences.getBoolean(key, defValue);
    }

    public static int getInt(String key, int defValue) {
        init();
        return preferences.getInt(key, defValue);
    }

    public static String getString(String key, String defValue) {
        init();
        return preferences.getString(key, defValue);
    }

    public static long getLong(String key, long defValue) {
        init();
        return preferences.getLong(key, defValue);
    }

    public static float getFloat(String key, float defValue) {
        init();
        return preferences.getFloat(key, defValue);
    }

    public static void setLong(String key, long value) {
        init();
        Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.commit();
    }

    public static void setBoolean(String key, boolean value) {
        init();
        Editor editor = preferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static void setString(String key, String value) {
        init();
        Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static void setInt(String key, int value) {
        init();
        Editor editor = preferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static void setFloat(String key, float value) {
        init();
        Editor editor = preferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    /**
     * 清空数据
     */
    public static void clear() {
        init();
        Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * 清除指定数据
     */
    public static void remove(String key) {
        init();
        Editor editor = preferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public static String SceneList2String(List SceneList)
            throws IOException {
        // 实例化一个ByteArrayOutputStream对象，用来装载压缩后的字节文件。
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        // 然后将得到的字符数据装载到ObjectOutputStream
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                byteArrayOutputStream);
        // writeObject 方法负责写入特定类的对象的状态，以便相应的 readObject 方法可以还原它
        objectOutputStream.writeObject(SceneList);
        // 最后，用Base64.encode将字节文件转换成Base64编码保存在String中
        String SceneListString = new String(Base64.encode(byteArrayOutputStream.toByteArray(), Base64.DEFAULT));
        // 关闭objectOutputStream
        objectOutputStream.close();
        return SceneListString;

    }


    @SuppressWarnings("unchecked")
    public static List String2SceneList(String SceneListString)
            throws StreamCorruptedException, IOException, ClassNotFoundException {
        byte[] mobileBytes = Base64.decode(SceneListString.getBytes(), Base64.DEFAULT);
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(mobileBytes);
        ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        List SceneList = (List) objectInputStream.readObject();
        objectInputStream.close();
        return SceneList;
    }

    public static boolean setObjectToShare(Object object, String key) {
        init();
        if (object == null) {
            Editor editor = preferences.edit().remove(key);
            return editor.commit();
        }
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ObjectOutputStream oos = null;
        try {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
// 将对象放到OutputStream中
// 将对象转换成byte数组，并将其进行base64编码
        String objectStr = new String(Base64.encode(baos.toByteArray(),
                Base64.DEFAULT));
        try {
            baos.close();
            oos.close();
        } catch (IOException e) {
// TODO Auto-generated catch block
            e.printStackTrace();
        }
        SharedPreferences.Editor editor = preferences.edit();
// 将编码后的字符串写到base64.xml文件中
        editor.putString(key, objectStr);
        return editor.commit();
    }


    public static Object getObjectFromShare(String key) {
        init();
        try {
            String wordBase64 = preferences.getString(key, "");
// 将base64格式字符串还原成byte数组
            if (wordBase64 == null || wordBase64.equals("")) { // 不可少，否则在下面会报java.io.StreamCorruptedException
                return null;
            }
            byte[] objBytes = Base64.decode(wordBase64.getBytes(),
                    Base64.DEFAULT);
            ByteArrayInputStream bais = new ByteArrayInputStream(objBytes);
            ObjectInputStream ois = new ObjectInputStream(bais);
// 将byte数组转换成product对象
            Object obj = ois.readObject();
            bais.close();
            ois.close();
            return obj;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static UserBean getUserBean() {
        if (getObjectFromShare("user") instanceof UserBean) {
            UserBean bean = (UserBean) getObjectFromShare("user");
            return bean;
        }
        return null;
    }

}
