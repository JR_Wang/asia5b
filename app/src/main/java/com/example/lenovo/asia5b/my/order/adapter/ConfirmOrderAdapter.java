package com.example.lenovo.asia5b.my.order.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.order.activity.ConfirmOrderActivity;
import com.example.lenovo.asia5b.my.order.bean.ConfirmOrderBean;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;

import com.example.lenovo.asia5b.ui.DownloadPicture;

/**
 * Created by lenovo on 2017/6/20.
 */

public class ConfirmOrderAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<ConfirmOrderBean> lists ;
    private Context context;

    public ConfirmOrderAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<ConfirmOrderBean>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return lists.size();
    }
    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }
    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_confirm_order, null);
            holder.txt_goods_name = (TextView)convertView.findViewById(R.id.txt_goods_name);
            holder.txt_goods_attr = (TextView)convertView.findViewById(R.id.txt_goods_attr);
            holder.txt_goods_price = (TextView)convertView.findViewById(R.id.txt_goods_price);
            holder.txt_goods_number = (TextView)convertView.findViewById(R.id.txt_goods_number);
            holder.image_goods_attr_thumb = (ImageView)convertView.findViewById(R.id.image_goods_attr_thumb);
            holder.cb_pro_checkbox = (CheckBox)convertView.findViewById(R.id.cb_pro_checkbox);
            holder.txt_line = (TextView)convertView.findViewById(R.id.txt_line);
            holder.txt_china_fee = (TextView)convertView.findViewById(R.id.txt_china_fee);
            holder.txt_weight = (TextView)convertView.findViewById(R.id.txt_weight);
            holder.ll_china_fee = (LinearLayout)convertView.findViewById(R.id.ll_china_fee);
            holder.txt_market_price = (TextView)convertView.findViewById(R.id.txt_market_price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final ConfirmOrderBean bean = lists.get(position);
        holder.txt_goods_name.setText(bean.goods_name);
        holder.txt_goods_attr.setText(":  "+bean.goods_attr);
        holder.txt_goods_number.setText("x"+bean.goods_number);
        holder.txt_goods_price.setText("RM"+ bean.goods_price);
        if (!bean.goods_price.equals(bean.examine_price)) {
            holder.txt_market_price.setText("RM" + bean.examine_price);
            holder.txt_market_price.setVisibility(View.VISIBLE);
        } else{
            holder.txt_market_price.setVisibility(View.GONE);
        }
        holder.txt_market_price.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG );
        DownloadPicture.loadNetwork(bean.goods_attr_thumb, holder.image_goods_attr_thumb);
        holder.cb_pro_checkbox.setVisibility(View.VISIBLE);

        if (position > 0) {
            ConfirmOrderBean bean2 = lists.get(position-1);
            if (bean.sign .equals(bean2.sign)) {
                bean.isSelected = bean2.isSelected;
                holder.cb_pro_checkbox.setVisibility(View.INVISIBLE);
                holder.txt_line.setVisibility(View.GONE);
            }else {
                holder.cb_pro_checkbox.setVisibility(View.VISIBLE);
                holder.txt_line.setVisibility(View.VISIBLE);
            }
        }
        if (bean.isSelected) {
            holder.cb_pro_checkbox.setChecked(true);
            bean.isSelected = true;
        } else {
            holder.cb_pro_checkbox.setChecked(false);
            bean.isSelected = false;
        }
        if (lists.size()-1 == position) {
            holder.ll_china_fee.setVisibility(View.VISIBLE);
            holder.txt_china_fee.setText( bean.china_fee);
        } else {
            ConfirmOrderBean bean2 = lists.get(position+1);
            if (bean.sign .equals(bean2.sign)) {
                holder.ll_china_fee.setVisibility(View.GONE);
            } else {
                holder.txt_china_fee.setText("RM "+bean.china_fee);
                holder.ll_china_fee.setVisibility(View.VISIBLE);
            }
        }
        holder.cb_pro_checkbox.setOnClickListener(new View.OnClickListener() {
                                                      @Override
                                                      public void onClick(View view) {
                                                          if (bean.isSelected) {
                                                              ((CheckBox) view).setChecked(false);
                                                              bean.isSelected = false;
                                                          } else {
                                                              ((CheckBox) view).setChecked(true);
                                                              bean.isSelected = true;
                                                          }
                                                          for (int i = 0; i<lists.size();i++) {
                                                              if (i > 0) {
                                                                  ConfirmOrderBean bean2 = lists.get(i-1);
                                                                  if (lists.get(i).sign .equals(bean2.sign)) {
                                                                      lists.get(i).isSelected = bean2.isSelected;
                                                                  }
                                                              }
                                                          }
                                                          ((ConfirmOrderActivity)context).settlementSum();
                                                      }
                                                  }
        );
        holder.txt_weight.setText(bean.weight);
        return convertView;
    }

    private class ViewHolder {
       TextView txt_goods_name,txt_goods_attr,txt_goods_price,txt_goods_number,txt_line,txt_china_fee,txt_weight,txt_market_price;
        ImageView image_goods_attr_thumb;
        CheckBox cb_pro_checkbox;
        LinearLayout ll_china_fee;
    }

    public void setLists(List<ConfirmOrderBean> lists) {
        this.lists = lists;
    }

    public List<ConfirmOrderBean> getLists() {
        return lists;
    }
}



