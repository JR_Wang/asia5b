package com.example.lenovo.asia5b.home.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/9/28.
 */

public class AccountBean implements Serializable{
    public String user_id;
    public String user_name;
    public String avatar;
}
