package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.example.lenovo.asia5b.home.bean.UserBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.photoview.ImagePagerActivity;
import com.example.lenovo.asia5b.util.DensityUtil;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.example.lenovo.asia5b.util.SharedPreferencesUtils;
import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/10/10.
 */

public class BillListActivity  extends BaseActivity implements ICallBack {
    private RollPagerView mRollViewPager;//图片轮播
    private ArrayList<String> list_process_type = new ArrayList<String>();
    private ArrayList<String> list_imag = new ArrayList<String>();
    private ArrayList<String> list_imag_all = new ArrayList<String>();
    private TestNormalAdapter adapter;
    private String order_sn;
    private PopupWindow pop;
    private LoadingDalog loadingDalog ;
    private String order_total;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_list);
        initView();
        billListData();
    }

    public void initView() {
        loadingDalog = new LoadingDalog(this);
//        F.id(R.id.public_txt_righe).visibility(View.VISIBLE);
        F.id(R.id.public_txt_righe).text(getResources().getString(R.string. state));
        F.id(R.id.public_txt_righe).clicked(this);
        F.id(public_btn_left).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.pj));
        //图片轮播
        mRollViewPager =  (RollPagerView)findViewById(R.id.frame_new_products_roll_view_pager);
        //设置播放时间间隔
        mRollViewPager.setPlayDelay(3000);
        //设置透明度
        mRollViewPager.setAnimationDurtion(500);
        if (getIntent().hasExtra("order_sn")) {
            order_sn = getIntent().getStringExtra("order_sn");
        }
        adapter = new TestNormalAdapter();
        mRollViewPager.setAdapter(adapter);
        //设置指示器（顺序依次）
        //自定义指示器图片
        //设置圆点指示器颜色
        //设置文字指示器
        //隐藏指示器
        mRollViewPager.setHintView(new ColorPointHintView(BillListActivity.this, Color.YELLOW,Color.WHITE));

    }

    private void notifData(String type) {
        list_imag.clear();
        for (int i = 0; i < list_imag_all.size(); i ++) {
            if (list_process_type.get(i).equals(type)) {
                list_imag.add(list_imag_all.get(i));
            }
        }
        initRollViewPager();
    }

    private void initRollViewPager( ){
        adapter.notifyDataSetChanged();
        if (null != pop) {
            pop.dismiss();
        }

    }


    class TestNormalAdapter extends StaticPagerAdapter {

        @Override
        public View getView(ViewGroup container, int position) {
            ImageView view = new ImageView(container.getContext());
            DownloadPicture.loadNetwork(list_imag.get(position),view);
            view.setScaleType(ImageView.ScaleType.FIT_XY);
            final int id = position;
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    imageBrower(id);
                }
            });
            return view;
        }

        @Override
        public int getCount() {
            return list_imag.size();
        }
    }

    /**
     * 打开图片查看器
     *
     * @param position
     */
    protected void imageBrower(int position) {
        Intent intent = new Intent(BillListActivity.this, ImagePagerActivity.class);
        // 图片url,为了演示这里使用常量，一般从数据库中或网络中获取
        intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_URLS, list_imag);
        intent.putExtra(ImagePagerActivity.EXTRA_IMAGE_INDEX, position);
        startActivity(intent);
    }

    /**
     * 调用接口
     */
    public void billListData() {
        try {
            loadingDalog.show();
            Date date = new Date();
            UserBean userBean = (UserBean) SharedPreferencesUtils.getObjectFromShare("user");
            if (userBean != null) {
                String url = Setting.ORDER_NOTE;
                Map<String, String> map = new HashMap<String, String>();
                map.put("user_id", userBean.user_id);
                map.put("order_sn",order_sn);
                map.put("time", String.valueOf(date.getTime()));
                //排序
                String[] sortStr = {"user_id" + userBean.user_id,  "time" + date.getTime()};
                String sort = Logic.sortToString(sortStr);
                String md5 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sort));
                map.put("sign", md5);
                JsonTools.getJsonAll(this, url, map, 0);
            }
        } catch (Exception e) {
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            try {
                if (result != null) {
                    if (action == 0) {
                        JSONObject jsonObject = (JSONObject) result;
                        int state = jsonObject.getInt("State");
                        if (state == 0) {
                            JSONArray jsonArray = jsonObject.getJSONArray("order_bills");
                            for (int i = 0; i < jsonArray.length();i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                list_imag.add(jsonObject1.getString("image"));
                                list_process_type.add(jsonObject1.getString("process_type"));
                            }
                            order_total = jsonObject.getString("order_total");
                            list_imag.add(order_total);
                            list_imag_all.clear();
                            list_imag_all.addAll(list_imag);
                            imageBrower(0);
                            finish();
//                            initRollViewPager();
                        }else if (state == 1) {
                            U.Toast(BillListActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(BillListActivity.this, getResources().getString(R.string.cscw));
                        }
                        loadingDalog.dismiss();
                    }
                }
            } catch (Exception e) {
            }
        }
    };

    /**
     * 接口回调
     * @param result
     * @param action
     */
    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.public_btn_left:
                finish();
                break;
            case  R.id.public_txt_righe:
                showPop();
                break;
            case R.id.txt_qb:
                list_imag.clear();
                list_imag.addAll(list_imag_all);
                initRollViewPager();
                break;
            case R.id.txt_cz:
                notifData("0");
                break;
            case R.id.txt_qx:
                notifData("1");
                break;
            case R.id.txt_xdzf:
                notifData("4");
                break;
            case R.id.txt_th:
                notifData("5");
                break;
            case R.id.txt_qhqx:
                notifData("6");
                break;
            case R.id.txt_zdkk:
                notifData("7");
                break;
            case R.id.txt_bcj:
                notifData("8");
                break;
            case R.id.txt_qxdg:
                notifData("9");
                break;
            case R.id.txt_ngqx:
                notifData("10");
                break;
            case R.id.txt_zjzhye:
                    notifData("12");
                break;
            case R.id.txt_jlzj:
                notifData("13");
                break;
            case R.id.txt_fxzj:
                notifData("14");
                break;
            case R.id.txt_ddpj:
                list_imag.clear();
                list_imag.add(order_total);
                initRollViewPager();
                break;
        }
    }

    public void showPop() {
        int screenWidth = DensityUtil.getScreenHeight(BillListActivity.this);
        if (null == pop ) {
            View popview = View.inflate(this, R.layout.view_the_pj_right, null);
            TextView txt_qb = (TextView) popview.findViewById(R.id.txt_qb);
            TextView txt_cz = (TextView) popview.findViewById(R.id.txt_cz);
            TextView txt_qx = (TextView) popview.findViewById(R.id.txt_qx);
            TextView txt_xdzf = (TextView) popview.findViewById(R.id.txt_xdzf);
            TextView txt_th = (TextView) popview.findViewById(R.id.txt_th);
            TextView txt_qhqx = (TextView) popview.findViewById(R.id.txt_qhqx);
            TextView txt_zdkk = (TextView) popview.findViewById(R.id.txt_zdkk);
            TextView txt_bcj = (TextView) popview.findViewById(R.id.txt_bcj);
            TextView txt_qxdg = (TextView) popview.findViewById(R.id.txt_qxdg);
            TextView txt_ngqx = (TextView) popview.findViewById(R.id.txt_ngqx);
            TextView txt_zjzhye = (TextView) popview.findViewById(R.id.txt_zjzhye);
            TextView txt_jlzj = (TextView) popview.findViewById(R.id.txt_jlzj);
            TextView txt_fxzj = (TextView) popview.findViewById(R.id.txt_fxzj);
            TextView txt_ddpj = (TextView)popview.findViewById(R.id.txt_ddpj);
            txt_qb.setOnClickListener(this);
            txt_cz.setOnClickListener(this);
            txt_qx.setOnClickListener(this);
            txt_xdzf.setOnClickListener(this);
            txt_th.setOnClickListener(this);
            txt_qhqx.setOnClickListener(this);
            txt_zdkk.setOnClickListener(this);
            txt_bcj.setOnClickListener(this);
            txt_qxdg.setOnClickListener(this);
            txt_ngqx.setOnClickListener(this);
            txt_zjzhye.setOnClickListener(this);
            txt_jlzj.setOnClickListener(this);
            txt_fxzj.setOnClickListener(this);
            txt_ddpj.setOnClickListener(this);
            pop = new PopupWindow(popview, FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT, true);
            pop.setBackgroundDrawable(new BitmapDrawable());
            pop.setWidth(DensityUtil.dip2px(BillListActivity.this, 200));
            pop.setHeight(DensityUtil.dip2px(BillListActivity.this, 350));
            pop.setOutsideTouchable(true);
        }
        pop.showAsDropDown(findViewById(R.id.public_txt_righe),screenWidth,0);
    }
}
