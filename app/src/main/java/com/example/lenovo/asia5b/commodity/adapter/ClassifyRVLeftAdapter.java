package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.lenovo.asia5b.commodity.bean.ClassifyTwoBean;
import com.wushi.lenovo.asia5b.R;

import java.util.List;

/**
 * Created by lenovo on 2017/12/8.
 */

public class ClassifyRVLeftAdapter extends RecyclerView.Adapter<ClassifyRVLeftAdapter.MyViewHolder>  implements View.OnClickListener {
    private List<ClassifyTwoBean> mDatas;
    private Context mContext;
    private LayoutInflater inflater;

    private int selectedPosition = 0;

    private OnItemClickListener mOnItemClickListener = null;
    private View viewClassify;

    //define interface
    public static interface OnItemClickListener {
        void onItemClick(View view , int position);
    }


    public ClassifyRVLeftAdapter(Context context, List<ClassifyTwoBean> datas){
        this. mContext=context;
        this. mDatas=datas;
        inflater= LayoutInflater. from(mContext);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view  = inflater.inflate(R.layout. item_classify_rv,parent, false);
        MyViewHolder holder= new MyViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tv.setText( mDatas.get(position).getCat_name());
        //选中和没选中时，设置不同的颜色
        if (position == selectedPosition) {
            holder.tv.setBackgroundColor(mContext.getResources().getColor(R.color.my_e1e1));
        } else {
            holder.tv.setBackgroundColor(Color.WHITE);
        }
        holder.itemView.setTag(position);
    }


    @Override
    public int getItemCount() {
        return mDatas.size();
    }

    @Override
    public void onClick(View view) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取position
            mOnItemClickListener.onItemClick(view,(int)view.getTag());
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }


    class MyViewHolder extends RecyclerView.ViewHolder
    {

        TextView tv;

        public MyViewHolder(View view)
        {
            super(view);
            tv = (TextView) view.findViewById(R.id.classsify_tv);
        }
    }

}
