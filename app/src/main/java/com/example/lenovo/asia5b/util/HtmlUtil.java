package com.example.lenovo.asia5b.util;

import java.io.UnsupportedEncodingException;
import java.util.regex.Pattern;

public class HtmlUtil {
	/**
	 *
	 * @Title: html
	 * @Description: 解析HTML符号实体
	 * @param @param content
	 * @param @return 解析后的通知
	 * @return String
	 * @throws
	 */
//	public static String html(String content) {
//		if (content == null) {
//			return "";
//		} else {
//			String html = content;
//			html = StringUtils.replace(html, "&ldquo;", "“");
//			html = StringUtils.replace(html, "&rdquo;", "”");
//			html = StringUtils.replace(html, "&mdash;", "—");
//			html = StringUtils.replace(html, "&ndash", "–");
//			html = StringUtils.replace(html, "&nbsp;", " ");
//			html = StringUtils.replace(html, "&lsquo;", "‘");
//			html = StringUtils.replace(html, "&rsquo;", "’");
//			html = StringUtils.replace(html, "&sbquo;", "‚");
//			html = StringUtils.replace(html, "&bull;", "•");
//			html = StringUtils.replace(html, "&hellip;", "…");
//			html = StringUtils.replace(html, "&bdquo;", "„");
//			html = StringUtils.replace(html, "&prime;", "′");
//			html = StringUtils.replace(html, "&Prime;", "″");
//			html = StringUtils.replace(html, "&apos;", "'");
//			html = StringUtils.replace(html, "&amp;", "&");
//			html = StringUtils.replace(html, "&lt;", "<");
//			html = StringUtils.replace(html, "&gt;", ">");
//			html = StringUtils.replace(html, "&reg;", "®");
//			html = StringUtils.replace(html, "&copy;", "©");
//			html = StringUtils.replace(html, "&middot;", "·");
//			html = StringUtils.replace(html, "&ge;", "≥");
//			return html;
//		}
//	}

	/**
	 * 把html内容转为文本
	 *

	 * @return
	 */
	public static String htmlToText(String inputString) {
		String htmlStr = inputString; // 含html标签的字符串
		String textStr = "";
		Pattern p_script;
		java.util.regex.Matcher m_script;
		Pattern p_style;
		java.util.regex.Matcher m_style;
		Pattern p_html;
		java.util.regex.Matcher m_html;
		try {
			String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; // 定义script的正则表达式{或<script[^>]*?>[\\s\\S]*?<\\/script>
			String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; // 定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style>
			String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式
			p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
			m_script = p_script.matcher(htmlStr);
			htmlStr = m_script.replaceAll(""); // 过滤script标签
			p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
			m_style = p_style.matcher(htmlStr);
			htmlStr = m_style.replaceAll(""); // 过滤style标签
			p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
			m_html = p_html.matcher(htmlStr);
			htmlStr = m_html.replaceAll(""); // 过滤html标签
			textStr = htmlStr;
		} catch (Exception e) {
			System.err.println("Html2Text: " + e.getMessage());
		}
		return textStr;
	}

	public static String urlEncoder(String str) {
		if (null != str && !"".equals(str)) {
			try {
				return java.net.URLEncoder.encode(str, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return "";
	}

	public static String urlDecoder(String str) {
		if (null != str && !"".equals(str)) {
			try {
				return java.net.URLDecoder.decode(str, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		return "";
	}
}
