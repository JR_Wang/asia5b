package com.example.lenovo.asia5b.my.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.lenovo.asia5b.home.activity.ForgotPasswordActivity;
import com.example.lenovo.asia5b.home.bean.UserBean;
import com.example.lenovo.asia5b.my.activity.SetPayPassword;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.PayPassword;
import com.example.lenovo.asia5b.util.SMSCode;
import com.example.lenovo.asia5b.util.SharedPreferencesUtils;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import app.BaseFragment;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;


/**
 * Created by lenovo on 2017/7/14.
 */

public class PayPWDFragment extends BaseFragment implements ICallBack {

    private LoadingDalog loadingDalog;

    private View view;
    private TimeCount timeCount;
    private long timeOut;
    private UserBean user;

    private Button update_pay_password_code,update_pay_password_save;
    private EditText update_pay_password_old_password,update_pay_password_new_password,
            update_pay_password_confirm_new_password,update_pay_password_message;
    private TextView update_pay_password_nothing,update_pay_password_forget;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.viewpager_update_pay_password,null);
        initView();
        return view;
    }

    private void initView(){
        loadingDalog = new LoadingDalog(getContext());

        update_pay_password_old_password = view.findViewById(R.id.update_pay_password_old_password);//旧支付密码
        update_pay_password_new_password = view.findViewById(R.id.update_pay_password_new_password);//新支付密码
        update_pay_password_confirm_new_password = view.findViewById(R.id.update_pay_password_confirm_new_password);//确认新支付密码
        update_pay_password_message = view.findViewById(R.id.update_pay_password_message);//验证码

        //验证码
        update_pay_password_code = view.findViewById(R.id.update_pay_password_code);
        update_pay_password_code.setOnClickListener(this);
        //修改保存
        update_pay_password_save = view.findViewById(R.id.update_pay_password_save);
        update_pay_password_save.setOnClickListener(this);
        //还没有支付密码
        update_pay_password_nothing = view.findViewById(R.id.update_pay_password_nothing);
        update_pay_password_nothing.setOnClickListener(this);
        //忘记密码
        update_pay_password_forget = view.findViewById(R.id.update_pay_password_forget);
        update_pay_password_forget.setOnClickListener(this);
        if (!getUserBean().paypass.equals("0")){
            update_pay_password_nothing.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.update_pay_password_code://验证码
                user = (UserBean) SharedPreferencesUtils.getObjectFromShare("user");
                loadingDalog.show();
                SMSCode.getCode(this,user.mobile_phone);
                break;
            case R.id.update_pay_password_save://修改保存
                String code = update_pay_password_message.getText().toString();//验证码
                if(!TextUtils.isEmpty(code)){
                    if(timeOut <= 1){
                        U.Toast(getActivity(),getResources().getString(R.string.yzmsx));
                    }else{
                        updatePayPassword(code);
                    }
                }else{
                    U.Toast(getActivity(),getResources().getString(R.string.yzmbnwk));
                }
                break;
            case R.id.update_pay_password_nothing://还没有支付密码
                intent = new Intent(getActivity(),SetPayPassword.class);
                break;
            case R.id.update_pay_password_forget://忘记密码
                intent = new Intent(getActivity(),ForgotPasswordActivity.class);
                intent.putExtra("from","paypassword");
                break;
        }
        if(intent != null){
            startActivity(intent);
        }
    }

    private void updatePayPassword(String code){
        try{
            String oldPWD = update_pay_password_old_password.getText().toString();//旧密码
            String newPWD = update_pay_password_new_password.getText().toString();//新密码
            String confirmNewPWD = update_pay_password_confirm_new_password.getText().toString();//确认新密码
            if(!TextUtils.isEmpty(oldPWD)){
                if(!TextUtils.isEmpty(newPWD)){
                    if(newPWD.length() == 6){
                        if(!TextUtils.isEmpty(confirmNewPWD)){
                            if(TextUtils.equals(newPWD,confirmNewPWD)){
                                loadingDalog.show();
                                PayPassword.savePayPassword(this,oldPWD,newPWD,code);
                            }else{
                                U.Toast(getActivity(),getResources().getString(R.string.zfmmbxd));
                            }
                        }else{
                            U.Toast(getActivity(),getResources().getString(R.string.qrxfmmbnwk));
                        }
                    }else{
                        U.Toast(getActivity(),getResources().getString(R.string.zfmmcdbxdy6));
                    }
                }else{
                    U.Toast(getActivity(),getResources().getString(R.string.xzfmmbnwk));
                }
            }else{
                U.Toast(getActivity(),getResources().getString(R.string.jzfmmbnwk));
            }
        }catch (Exception e){
            Log.e("修改密码接口Error：",e.getMessage());
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            Intent intent = null;
            try{
                if(result != null){
                    JSONObject jsonObject = (JSONObject)result;
                    if(action == 0){
                        String msgText = jsonObject.getString("MsgText");
                        if("true".equals(msgText)){
                            timeCount = new TimeCount(60000,1000);
                            timeCount.start();
                            String state = jsonObject.getString("State");
//                            U.Toast(getActivity(),state);
                        }else if("false".equals(msgText)){
                            String state = jsonObject.getString("State");
                            U.Toast(getActivity(),state);
                        }
                        loadingDalog.dismiss();
                    }if(action == 1){
                        int state = jsonObject.getInt("State");
                        if(state == 0){
                            U.Toast(getActivity(),getResources().getString(R.string.modify_succeed));
                            getActivity().finish();
                        }else if (state == 1) {
                            U.Toast(getActivity(), getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(getActivity(), getResources().getString(R.string.cscw));
                        } else if(state == 3){
                            U.Toast(getActivity(),getResources().getString(R.string.xmmcdbxdy6));
                        }else if(state == 4){
                            U.Toast(getActivity(), getResources().getString(R.string.wdl));
                        }else if(state == 5){
                            U.Toast(getActivity(),getResources().getString(R.string.sjyzmcw));
                        }else if(state == 6){
                            U.Toast(getActivity(),getResources().getString(R.string.ndzfmmyw));
                        }else if(state == 7){
                            U.Toast(getActivity(),getResources().getString(R.string.xjzfmmbnyz));
                        }else if(state == 15){
                            U.Toast(getActivity(),getResources().getString(R.string.yzmsx));
                        }
                        loadingDalog.dismiss();
                    }
                }
            }catch (Exception e){
                Log.e("修改密码JSONError：",e.getMessage());
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 获取验证码倒计时
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if (null == getActivity()){
                return;
            }

            update_pay_password_code.setClickable(false);
            timeOut = millisUntilFinished / 1000;
            update_pay_password_code.setText(getResources().getString(R.string.message_has_been_sent)+"(" + timeOut + ")");
        }

        @Override
        public void onFinish() {
            update_pay_password_code.setText(getResources().getString(R.string.to_obtain_the_verification_code));
            update_pay_password_code.setClickable(true);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (null != timeCount) {
            timeCount.cancel();
        }
    }
}
