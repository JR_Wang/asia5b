package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.MyDaikinRollBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.DensityUtil;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.wushi.lenovo.asia5b.R.id.ll_background;

/**
 * Created by lenovo on 2017/7/1.
 */

public class MyDaikinRollAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<MyDaikinRollBean> lists ;
    private long time;
    private Context context;
    private   int width;

    public MyDaikinRollAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<MyDaikinRollBean>();
        Date date = new Date();
        time = date.getTime();
        this.context = context;
        width = DensityUtil.getScreenWidth(context);
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_my_daikin_roll, null);
            holder.txt_type_money = (TextView)convertView.findViewById(R.id.txt_type_money);
            holder.txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txt_order_sn = (TextView)convertView.findViewById(R.id.txt_order_sn);
            holder.image_used= (ImageView) convertView.findViewById(R.id.image_used);
            holder.txt_code = (TextView)convertView.findViewById(R.id.txt_code);
            holder.ll_background = (LinearLayout)convertView.findViewById(ll_background);
            convertView.setTag(holder);

        }else {

            holder = (ViewHolder)convertView.getTag();
        }
        MyDaikinRollBean bean = lists.get(position);
        holder.txt_type_money.setText("RM"+bean.type_money);
        if (!TextUtils.isEmpty(bean.use_start_date)) {
//            String use_start_date =  DateUtils.timedate(bean.use_start_date);
            String use_end_date = DateUtils.timedate(bean.use_end_date);
//            holder.txt_time.setText(context.getResources().getString(R.string.yxq)+":\t"+ use_start_date +context.getResources().getString(R.string.z) + use_end_date);
            String[] ends = use_end_date.split(" ");
            holder.txt_time.setText(ends[0]);
        }
        holder.txt_code.setText(bean.code);
//        if (TextUtils.isEmpty(bean.order_sn)) {
//            holder.txt_order_sn.setText(context.getResources().getString(R.string.order_number)+":\t"+bean.order_sn);
//        } else {
//            holder.txt_order_sn.setText("");
//        }
//        //未使用
//        if (bean.used_time.equals("0")){
//            long end_date = Long.parseLong(bean.use_end_date);
//            //当结束时间小于当前时间，就显示已失效
//            if (end_date < time) {
//                holder.image_used.setVisibility(View.VISIBLE);
//            } else {
//                holder.image_used.setVisibility(View.INVISIBLE);
//            }
//        } else {
//            holder.image_used.setVisibility(View.VISIBLE);
//        }
        int ss =width * DensityUtil.dip2px(context,150)/DensityUtil.dip2px(context,450);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, ss);
        holder.ll_background.setLayoutParams(layoutParams);

        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_type_money,txt_time,txt_order_sn;
        ImageView image_used;
        TextView txt_code;
        LinearLayout ll_background;
    }

    public void setLists(List<MyDaikinRollBean> lists) {
        this.lists = lists;
    }

    public List<MyDaikinRollBean> getLists() {
        return lists;
    }
}
