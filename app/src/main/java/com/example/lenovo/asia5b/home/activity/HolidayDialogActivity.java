package com.example.lenovo.asia5b.home.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.lenovo.asia5b.commodity.adapter.ViewPageAdapter;
import com.example.lenovo.asia5b.my.bean.TestBean;
import com.example.lenovo.asia5b.ui.photoview.ViewPagerIndicator;
import com.example.lenovo.asia5b.util.DateUtils;
import com.jiangyy.easydialog.CommonDialog;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;

import app.BaseActivity;
import app.MyApplication;

/**
 * 节日提示框
 * Created by lenovo on 2017/9/22.
 */

public class HolidayDialogActivity extends BaseActivity{

    public static  final String HOLIDAY_DIALOG = "HOLIDAY_DIALOG";
    public static  final String HINT = "HINT";

    private Button public_dialog_left_bt,public_dialog_right_bt;

    private ViewPager vp;
    private ViewPageAdapter vpAdapter;
    private LinearLayout ll_indicator;
    private ArrayList<TestBean> lists = new ArrayList<>();


    //    public String title =getResources().getString(R.string.xtts),hint_ch = getResources().getString(R.string.jjr_hint),hint_en="";
        public String title = "",hint_ch="",identifying = "",mobile_picture = "";
    private ImageView img_colse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hint_view);
        Intent intent = getIntent();
        if (intent.hasExtra("data")){
            ArrayList<TestBean> data= (ArrayList) intent.getSerializableExtra("data");
            lists.addAll(data);

        }

        initView();
    }

    private void initView() {

        public_dialog_left_bt = (Button) findViewById(R.id.public_dialog_left_bt);
        public_dialog_left_bt.setOnClickListener(this);
        public_dialog_right_bt = (Button) findViewById(R.id.public_dialog_right_bt);
        public_dialog_right_bt.setOnClickListener(this);

        img_colse=(ImageView) findViewById(R.id.img_colse);
        img_colse.setOnClickListener(this);

        ll_indicator = (LinearLayout)findViewById(R.id.ll_indicator);

        vp = (ViewPager) findViewById(R.id.vp_huodong);
        vpAdapter = new ViewPageAdapter(this,lists);
        vp.setAdapter(vpAdapter);
        vp.addOnPageChangeListener(new ViewPagerIndicator(this,vp,ll_indicator,lists.size()));

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
//            case  R.id.public_dialog_left_bt:
//                finish();
//                SharedPreferences preferences = MyApplication.getInstance().getSharedPreferences("holiday",
//                        Context.MODE_PRIVATE);
//                SharedPreferences.Editor editor = preferences.edit();
//                long time=System.currentTimeMillis()/1000;
//                editor.putLong(HINT, time);
//                editor.commit();
//
//                break;
//            case R.id.public_dialog_right_bt:
//                finish();

//                break;
            case R.id.img_colse :
                new CommonDialog.Builder(this)
                        .setTitle(getString(R.string.bzts))
//                        .setMessage("这里是提示内容")
                        .setNegativeButton(getString(R.string.qd), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                                SharedPreferences preferences = MyApplication.getInstance().getSharedPreferences("holiday",
                                        Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = preferences.edit();
                                long time=System.currentTimeMillis()/1000;
                                editor.putLong(HINT, time);
                                editor.commit();
                            }
                        }).setPositiveButton(getString(R.string.qx), new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                finish();
                            }
                        }).show();
                break;
            default:
                break;
        }
    }

    public static boolean  getOnes() {
        boolean isFirstOpen = true;
        long time=System.currentTimeMillis()/1000;
        //国庆1号到7号
        if (time >= 1506700800 && time <= 1507305600) {
            // 判断是否是第一次开启应用
            SharedPreferences preferences = MyApplication.getInstance().getSharedPreferences("holiday",
                    Context.MODE_PRIVATE);
            isFirstOpen = preferences.getBoolean(HOLIDAY_DIALOG, false);
            if (!isFirstOpen) {
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(HOLIDAY_DIALOG, true);
                editor.commit();
            }
        }
       return isFirstOpen;
    }

    public static boolean  getTwos() {
        boolean isFirstOpen = true;
        SharedPreferences preferences = MyApplication.getInstance().getSharedPreferences("holiday",
                Context.MODE_PRIVATE);
        //获取当前时间
        long time=System.currentTimeMillis()/1000;
        String[] timeString = DateUtils.timesTwo(time+"").split("-");
        //获取本地时间
        long time2=preferences.getLong(HINT,0);
        if (time2 == 0) {
            return false;
        }
        String[] time2String = DateUtils.timesTwo(time2+"").split("-");
        //当前年等于 存储的年+1,弹出提示
        if (Integer.parseInt(timeString[0].trim()) ==Integer.parseInt(time2String[0].trim())+1 ){
            return false;
        } else if (Integer.parseInt(timeString[1]) ==Integer.parseInt(time2String[1])+1 ){
            return false;
        }else if (Integer.parseInt(timeString[2]) ==Integer.parseInt(time2String[2])+1 ){
            return false;
        }
        return isFirstOpen;
    }
}
