package com.example.lenovo.asia5b.util;

import java.security.MessageDigest;

/**
 * Created by lenovo on 2017/7/3.
 */

public class MD5 {

    private static final char HEX_DIGITS[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'a', 'b', 'c', 'd', 'e', 'f' };

    /**
     * MD5的32位加密
     * @param val：需要加密的字符串
     * @return 返回加密后的32位字符串
     * @throws Exception
     */
    public static String MD5EncryptionFor32(String val) throws Exception{
        MessageDigest md5 = MessageDigest.getInstance("MD5");
        md5.update(val.getBytes());
        byte[] m = md5.digest();//加密
        return getString(m);
    }

    private static String getString(byte[] b){
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < b.length; i++){
            sb.append(HEX_DIGITS[(b[i] & 0xf0) >>> 4]);
            sb.append(HEX_DIGITS[b[i] & 0x0f]);
        }
        return sb.toString();
    }
}
