package com.example.lenovo.asia5b.my.myWallet.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.myWallet.bean.RecordTopUpBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.DensityUtil;
import com.example.lenovo.asia5b.util.StringUtil;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lenovo on 2017/7/12.
 */

public class RecordTopUpAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<RecordTopUpBean> lists = null;
    private Context context;


    public RecordTopUpAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<RecordTopUpBean>();
        this.context = context;
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_record_top_up, null);
            holder.txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txt_amount = (TextView)convertView.findViewById(R.id.txt_amount);
            holder.txt_process_type = (TextView)convertView.findViewById(R.id.txt_process_type);
            holder.ll_pjh = (LinearLayout)convertView.findViewById(R.id.ll_pjh);
            holder.ll_state = (LinearLayout)convertView.findViewById(R.id.ll_state);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        RecordTopUpBean bean = lists.get(position);
        holder.txt_amount.setText("RM"+bean.amount);
        String time = DateUtils.timedate(bean.add_time);
        holder.txt_time.setText(time.replace(" ","\n"));
        holder.txt_process_type.setText(StringUtil.walletType(context,bean.process_type));
        holder.ll_state.setPadding(0, DensityUtil.dip2px(context,8),0,0);
        holder.ll_pjh.setVisibility(View.GONE);
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_time,txt_amount,txt_process_type;
        LinearLayout ll_pjh,ll_state;
    }

    public void  setLists (List<RecordTopUpBean> lists) {
        this.lists = lists;
    }

    public List<RecordTopUpBean>  getLists () {
        return  lists;
    }

}
