package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.lenovo.asia5b.util.AppInnerDownLoder;
import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * 关于我们
 * Created by lenovo on 2017/8/23.
 */

public class ServiceAboutActivity  extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_about);
        initView();
    }

    public void initView(){
        F.id(R.id.txt_ver_name).text(AppInnerDownLoder.getVerName(ServiceAboutActivity.this));
        F.id(R.id.public_title_name).text(getResources().getString(R.string.service_about));
        F.id(public_btn_left).clicked(this);
        F.id(R.id.ll_service_about).clicked(this);
        F.id(R.id.ll_contact_us).clicked(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case public_btn_left:
                finish();
                break;
            case R.id.ll_service_about:
                intent = new Intent(ServiceAboutActivity.this,MessageDetActivity.class);
                intent.putExtra("content",getResources().getString(R.string.service_about_hint));
                intent.putExtra("serviceAbout","serviceAbout");
                startActivity(intent);
                break;
            case R.id.ll_contact_us:
                intent = new Intent(ServiceAboutActivity.this,ContactUsActivity.class);
                startActivity(intent);
                break;
        }
    }
}
