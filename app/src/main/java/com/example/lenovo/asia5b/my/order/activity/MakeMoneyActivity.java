package com.example.lenovo.asia5b.my.order.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;

import com.example.lenovo.asia5b.my.activity.SetPayPassword;
import com.example.lenovo.asia5b.my.order.adapter.MakeMoneyAdapter;
import com.example.lenovo.asia5b.my.order.bean.MakeMoneyBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * 补差价
 * Created by lenovo on 2017/7/27.
 */

public class MakeMoneyActivity extends BaseActivity implements ICallBack,LoadingDalog.PasswordDialogInterface{
    private ListView listView;
    private LoadingDalog loadingDalog;
    private String order_id,rec_id;
    private MakeMoneyAdapter adapter;
    private List<MakeMoneyBean> lists = null;
    private String count_price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_money);
        initView();
        makeMoneyData();
    }

    public void initView(){
        if(getIntent().hasExtra("order_id")) {
            order_id = getIntent().getStringExtra("order_id");
        }
        if(getIntent().hasExtra("rec_id")) {
            rec_id = getIntent().getStringExtra("rec_id");
        }
        F.id(R.id.public_title_name).text(getResources().getString(R.string.bcj));
        F.id(public_btn_left).clicked(this);
        F.id(R.id.btn_next_step).clicked(this);
        loadingDalog = new LoadingDalog(this);
        listView=(ListView) findViewById(R.id.lv_list);
        adapter = new MakeMoneyAdapter(this);
        listView.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                finish();
                break;
            case R.id.btn_next_step:
                if (getUserBean().paypass.equals("0")){
                    U.Toast(MakeMoneyActivity.this, getString(R.string.none_pay_pwd));
                    intent = new Intent(MakeMoneyActivity.this, SetPayPassword.class);
                    startActivity(intent);
                    return;
                }
                loadingDalog.showPassword(count_price);
                break;
            default:
                break;
        }
    }

    /**
     * 获取补差价数据
     */
    public void makeMoneyData() {
        try {
            loadingDalog.show();
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.DIFF_LIST;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"order_id"+order_id,"rec_id"+rec_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("rec_id",rec_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }
    }

    /**
     * 支付补差价数据
     */
    public void payData(String pay_psss) {
        try {
            loadingDalog.show();
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.GO_TO_FIFFE;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"order_id"+order_id,"rec_id"+rec_id,"paypass"+pay_psss,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("order_id",order_id);
            parmaMap.put("rec_id",rec_id);
            parmaMap.put("paypass",pay_psss);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 1);
        }catch (Exception e) {

        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            JSONArray trackInfo = json.getJSONArray("goods");
                            lists = Logic.getListToBean(trackInfo,new MakeMoneyBean());
                            adapter.setLists(lists);
                            adapter.notifyDataSetChanged();
                            F.id(R.id.txt_cn_freight).text(json.getString("cn_freight"));
                            F.id(R.id.txt_tax_diff).text(json.getString("tax_diff"));
                            F.id(R.id.txt_pack_diff).text(json.getString("pack_diff"));
                            F.id(R.id.btn_next_step).text(getResources().getString(R.string.bcj)+"("+getResources().getString(R.string.count_price)+
                                    ":"+json.getString("count_price")+")");
                            count_price = json.getString("count_price");
                            F.id(R.id.txt_sy_fee).text("(RM"+json.getString("sy_fee")+")");
                            F.id(R.id.txt_dk).text(json.getString("dk"));
                            F.id(R.id.txt_bonus_fee).text("(RM"+json.getString("bonus_fee")+")");
                            F.id(R.id.txt_bonus_dk).text(json.getString("bonus_dk"));
                        }  else if (state == 1) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.cscw));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
            if (action == 1) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject jsonObect = (JSONObject) result;
                        int state = jsonObect.getInt("State");
                        if (state == 0) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.cg));
                            Intent intent = new Intent();
                            intent.putExtra("222", 2222);
                            MakeMoneyActivity.this.setResult(0, intent);
                            finish();
                        }else if (state == 1) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.mysj));
                        } else if (state == 4) {
                            U.Toast(MakeMoneyActivity.this,getResources().getString(R.string.mmcw));
                        } else if (state == 5) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.zfcc));
                        } else if (state == 6) {
                            U.Toast(MakeMoneyActivity.this, getResources().getString(R.string.yebz));
                        }else if (state == 10) {
                            U.Toast(MakeMoneyActivity.this, getString(R.string.none_pay_pwd));
                            Intent intent = new Intent(MakeMoneyActivity.this, SetPayPassword.class);
                            startActivity(intent);
                            loadingDalog.dismiss();
                            loadingDalog.passwordDialog = null;
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.clearPassword();
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    @Override
    public void gainPasswor(String strPasswor) {
        //开始调用补差价接口
        payData(strPasswor);
    }
}
