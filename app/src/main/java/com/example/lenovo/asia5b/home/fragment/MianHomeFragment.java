package com.example.lenovo.asia5b.home.fragment;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.commodity.activity.ClassifyMainActivity;
import com.example.lenovo.asia5b.commodity.activity.ClassifyMainSonActivity;
import com.example.lenovo.asia5b.commodity.activity.CommodityDetActivity;
import com.example.lenovo.asia5b.commodity.activity.SearchActivity;
import com.example.lenovo.asia5b.home.activity.LoginActivity;
import com.example.lenovo.asia5b.home.activity.WebViewActivity;
import com.example.lenovo.asia5b.home.adapter.MainHomeAdapter;
import com.example.lenovo.asia5b.home.bean.HotSaleBean;
import com.example.lenovo.asia5b.home.bean.HotSaleGoodsBean;
import com.example.lenovo.asia5b.home.bean.MSGbean;
import com.example.lenovo.asia5b.my.activity.MessageActivity;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.HeaderGridView;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.PullToRefreshLayout;
import com.example.lenovo.asia5b.util.DensityUtil;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.example.lenovo.asia5b.util.SharedPreferencesUtils;
import com.google.gson.Gson;
import com.jude.rollviewpager.OnItemClickListener;
import com.jude.rollviewpager.RollPagerView;
import com.jude.rollviewpager.adapter.StaticPagerAdapter;
import com.jude.rollviewpager.hintview.ColorPointHintView;
import com.umeng.message.PushAgent;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.entity.UMessage;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import app.BaseFragment;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;


/**
 *首页
 * Created by lenovo on 2017/6/20.
 */

public class MianHomeFragment extends BaseFragment implements PullToRefreshLayout.OnRefreshListener, ICallBack {
    public View fragmentView;
//    private MyApplication badgeIS;
    private MainHomeAdapter  adapter;
    private HeaderGridView gv_main_home;
    private PullToRefreshLayout pullToRefreshLayout;
    private LoadingDalog loadingDalog;
    private RollPagerView mRollViewPager;
    private View  tit;
    private int page  = 1;
    private int countPage = 0;
    private List<HotSaleGoodsBean> lists;
    private Button btn_classify;
    private TextView txt_commodity;
    private List<Map<String,String>> list_imag = new ArrayList<Map<String,String>>();
    private ImageView image_ad;
    private LinearLayout ll_classify;
    private RelativeLayout rl_mess;
    private static  TextView txt_mess;
    private static  RelativeLayout rl_shape;
    //照相功能
    private TextView txt_camera;

    public static int message_count = 0;

    private ImageView image_top;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.frame_mian_home, container,
                false);
//        badgeIS = new MyApplication();
//        badgeIS.setBadgeListener(new MyApplication.OnBadgeListener() {
//            @Override
//            public void onBadgeCount(int count) {
//                setMessCount(count);
//            }
//        });
        PushAgent mPushAgent = PushAgent.getInstance(getActivity());;
        UmengMessageHandler messageHandler = new UmengMessageHandler() {
            @Override
            public Notification getNotification(Context context, UMessage msg) {
                Log.e("接收到的推送消息",msg.getRaw().toString());
                Gson gson = new Gson();
                MSGbean msGbean = gson.fromJson(msg.getRaw().toString(),MSGbean.class);
                Log.e("接收到的推送消息条数",msGbean.getExtra().getMsgcount()+"");
//                ShortcutBadger.applyCount(context,msGbean.getExtra().getMsgcount());
                setMessCount(msGbean.getExtra().getMsgcount());
                return super.getNotification(context, msg);
            }
        };
        mPushAgent.setMessageHandler(messageHandler);


        hotSaleGoodsData();
        initView();
        hotSaleData();
        headImagData();
        homeAdData();

        messageNoRead();

        return fragmentView;
    }
    @Override
    public void onStart() {
        super.onStart();
        message_count =  SharedPreferencesUtils.getInt("Message_Count",0);
        if (message_count == 0) {
            rl_shape.setVisibility(View.GONE);
        } else {
            rl_shape.setVisibility(View.VISIBLE);
        }
        txt_mess.setText(""+message_count);
    }

    @Override
    public void onResume() {
        super.onResume();
        messageNoRead();
    }
    //    public void

    public static  void setMessCount(int id) {
        if (id == 0) {
            message_count = 0;
        } else {
            message_count = id;
        }
        if (null != txt_mess) {
            txt_mess.setText(""+message_count);
            if (id == 0){
                rl_shape.setVisibility(View.GONE);
            } else {
                rl_shape.setVisibility(View.VISIBLE);
            }
        }
        MianShopingCartFragment.setMessCount(id);
        CommodityDetActivity.setMessCount(id);
        MianMyA5bFragment.setMessCount(id);
        SharedPreferencesUtils.setInt("Message_Count",message_count);
    }

    public void initView() {
        loadingDalog = new LoadingDalog(getActivity());
        gv_main_home = (HeaderGridView)fragmentView. findViewById(R.id.gv_main_home);
        btn_classify = (Button)fragmentView.findViewById(R.id.btn_classify);
        txt_commodity = (TextView)fragmentView.findViewById(R.id.txt_commodity);
        txt_camera = (TextView)fragmentView.findViewById(R.id.txt_camera);
        txt_camera.setOnClickListener(this);
        txt_mess = (TextView)fragmentView.findViewById(R.id.txt_mess);
        btn_classify.setOnClickListener(this);
        rl_mess = fragmentView.findViewById(R.id.rl_mess);
        rl_shape  = fragmentView.findViewById(R.id.rl_shape);
        image_top = fragmentView.findViewById(R.id.image_top);
        rl_mess.setOnClickListener(this);
        pullToRefreshLayout = ((PullToRefreshLayout)fragmentView. findViewById(R.id.refresh_view));
        pullToRefreshLayout.setOnRefreshListener(this);
        adapter = new MainHomeAdapter(getActivity());
        tit  = LayoutInflater.from(getActivity()).inflate(R.layout.frame_main_home_tit, null);
        image_ad = (ImageView) tit.findViewById(R.id.image_ad);
        ll_classify=(LinearLayout) tit.findViewById(R.id.ll_classify);
        ll_classify.setOnClickListener(this);
        gv_main_home.addHeaderView(tit);
        gv_main_home.setAdapter(adapter);
        txt_commodity.setOnClickListener(this);
        image_top.setOnClickListener(this);


        gv_main_home.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // 当不滚动时
                if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
                    // 判断是否滚动到底部
                    if (view.getLastVisiblePosition() == view.getCount() - 1) {
                        if (page < countPage) {
                            page += 1;
                            hotSaleGoodsData();
                        } else {
                            TextView load_tv = (TextView)fragmentView.findViewById(R.id.load_tv);
                            load_tv.setText(getResources().getString(R.string.no_more_data));
                            load_tv.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });
        gv_main_home.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= 2) {
                    HotSaleGoodsBean bean = adapter.getLists().get(i-2);
                    Intent intent = new Intent(getActivity(), CommodityDetActivity.class);
                    intent.putExtra("goods_id",bean.goods_id);
                    getActivity().startActivityForResult(intent,0);
                }
            }
        });
        /**
         * 主页轮播
         */
        mRollViewPager =  (RollPagerView)tit.findViewById(R.id.roll_view_pager);
        //设置播放时间间隔
        mRollViewPager.setPlayDelay(3000);
        //设置透明度
        mRollViewPager.setAnimationDurtion(500);
        mRollViewPager.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Intent intent = null;
                Map map =list_imag.get(position);
                try {
                    JSONArray jsonArray = new JSONArray(map.get("ad_link").toString());
                    String type = jsonArray.getString(0);
                    if (type.equals("goods")) {
                        intent = new Intent(getActivity(), CommodityDetActivity.class);
                        intent.putExtra("goods_id",jsonArray.getString(1));
                        getActivity().startActivityForResult(intent,0);
                    }
                    else if (type.equals("cat_id")) {
                        intent = new Intent(getActivity(), ClassifyMainSonActivity.class);
                        intent.putExtra("catid",jsonArray.getString(1));
                        intent.putExtra("catname",jsonArray.getString(2));
                        startActivity(intent);
                    } else if (type.equals("promotion")) {
                        intent = new Intent(getActivity(), WebViewActivity.class);
                        String url = jsonArray.getString(1);
                        String urlPing = "";
                        if (url.indexOf("/activity.php?") > -1){
                            String pingA = "&asia";
                            urlPing = url + pingA;
                        }else{
                            urlPing = url;
                        }

                        intent.putExtra("url",urlPing);
                        intent.putExtra("promotionTitle",jsonArray.getString(2));
                        startActivity(intent);
                    }
                } catch (Exception e) {

                }

            }
        });

        //设置指示器（顺序依次）
        //自定义指示器图片
        //设置圆点指示器颜色
        //设置文字指示器
        //隐藏指示器
        //mRollViewPager.setHintView(new IconHintView(this, R.drawable.point_focus, R.drawable.point_normal));
        mRollViewPager.setHintView(new ColorPointHintView(fragmentView.getContext(),Color.YELLOW,Color.WHITE));
        //mRollViewPager.setHintView(new TextHintView(this));
        //mRollViewPager.setHintView(null);
    }

    private void initRollViewPager( ){
        //设置适配器
        mRollViewPager.setAdapter(new TestNormalAdapter());
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btn_classify:
                intent = new Intent(getActivity(), ClassifyMainActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.txt_commodity:
                intent = new Intent(getActivity(),SearchActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.ll_classify:
                intent = new Intent(getActivity(), ClassifyMainActivity.class);
                getActivity().startActivity(intent);
                break;
            case R.id.rl_mess:
                if(null == getUserBean()) {
                    intent = new Intent(getActivity(), LoginActivity.class);
                } else {
                    intent = new Intent(getActivity(), MessageActivity.class);
                    rl_shape.setVisibility(View.GONE);
                    message_count = 0;
                    SharedPreferencesUtils.setInt("Message_Count",message_count);
                }
                getActivity().startActivity(intent);
                break;
            case R.id.txt_camera:
                loadingDalog.showPhoto();
                break;
            case R.id.image_top:
                gv_main_home.setSelection (-1);
            default:
                break;
        }
    }


    /**
     * 向下刷新
     */
    @Override
    public void onRefresh(PullToRefreshLayout pullToRefreshLayout) {
//        loadingDalog.show();
        hotSaleGoodsData();
    }

    /**
     * 向上加载
     */
    @Override
    public void onLoadMore(PullToRefreshLayout pullToRefreshLayout) {
        pullToRefreshLayout.loadmoreFinish(PullToRefreshLayout.SUCCEED);
    }

    final Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    pullToRefreshLayout.loadmoreFinish(PullToRefreshLayout.SUCCEED);
                    pullToRefreshLayout.refreshFinish(PullToRefreshLayout.SUCCEED);
            }
            super.handleMessage(msg);
        }
    };

    class TestNormalAdapter extends StaticPagerAdapter {

        @Override
        public View getView(ViewGroup container, int position) {
            ImageView view = new ImageView(container.getContext());
            Map map = list_imag.get(position);
            DownloadPicture.loadNetwork(map.get("ad_code").toString(),view);
            view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            return view;
        }


        @Override
        public int getCount() {
            return list_imag.size();
        }
    }


    public void initTitView(List<HotSaleBean> lists) {
        int width = DensityUtil.getScreenWidth(getActivity())-60;
        LinearLayout ll_hot_sale = (LinearLayout) tit.findViewById(R.id.ll_hot_sale);
        ll_hot_sale.setVisibility(View.VISIBLE);
        gv_main_home.setAdapter(adapter);
        LinearLayout  mLayout = null;

        for (int i = 0; i < lists.size(); i ++) {
            final HotSaleBean bean = lists.get(i);
            if (i%3 == 0) {
                mLayout = new LinearLayout(getActivity());
                mLayout.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                mLayout.setOrientation(LinearLayout.HORIZONTAL);
                mLayout.setPadding(14,0,0,0);
                ll_hot_sale.addView(mLayout);
            }
            View hotSaleView =  LayoutInflater.from(getActivity()).inflate(R.layout.item_hot_sale, null);
            hotSaleView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ClassifyMainSonActivity.class);
                    intent.putExtra("catid",bean.cat_id);
                    intent.putExtra("catname",bean.cat_name);
                    startActivity(intent);
                }
            });
            ImageView image_commodity = (ImageView)hotSaleView.findViewById(R.id.image_commodity);
            TextView txt_name = (TextView)hotSaleView.findViewById(R.id.txt_name);

            image_commodity.setLayoutParams(new LinearLayout.LayoutParams(
                    width/3,
                    width/2));
            txt_name.setText(bean.cat_name);
            DownloadPicture.loadNetwork(bean.fileimg,image_commodity);
            mLayout.addView(hotSaleView,new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT,1));
        }
    }

    private Handler handler2 = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            try{
                switch (msg.what){
                    case 0 : {
                        JSONObject json = new JSONObject((String)msg.obj);
                        JSONArray remai = json.getJSONArray("remai");
                        List<HotSaleBean> lists = Logic.getListToBean(remai,new HotSaleBean());
                        initTitView(lists);
                    };break;
                    case 1 : {

                        try {
                            JSONObject json = new JSONObject((String)msg.obj);
                            JSONObject paging = json.getJSONObject("paging");
                            if (null!=paging.getString("page") && "1".equals(paging.getString("page"))) {
                                pullToRefreshLayout.refreshFinish(PullToRefreshLayout.SUCCEED);
                                initPaging(paging);
                                JSONArray hot = json.getJSONArray("hot");
                                lists = Logic.getListToBean(hot,new HotSaleGoodsBean());
                                adapter.setLists(lists);
                                adapter.notifyDataSetChanged();
                            }  else if(null!=paging.getString("page") && !"1".equals(paging.getString("page"))){
                                pullToRefreshLayout.loadmoreFinish(PullToRefreshLayout.SUCCEED);
                                JSONArray hot = (JSONArray) json.get("hot");
                                if (null != hot && hot.length() > 0) {
                                    List<HotSaleGoodsBean> listBean  =  Logic.getListToBean(hot,new HotSaleGoodsBean());
                                    for (int i = 0; i < listBean.size(); i++) {
                                        lists .add(listBean.get(i));
                                    }
                                    adapter .setLists(lists);
                                    adapter .notifyDataSetChanged();
                                } else {
                                    page = page - 1;
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    };break;
                    case 2 : {
                        JSONObject json =  new JSONObject((String)msg.obj);
                        int state = (int)json.getInt("State");
                        if (state == 0) {
//                        JSONArray jsonArray = json.getJSONArray("images");
//                        String ad_code = jsonArray.getJSONObject(0).getString("ad_code");
//                        image_ad.setVisibility(View.VISIBLE);
//                        DownloadPicture.loadNetwork(ad_code,image_ad);
                        }
                        initRollViewPager();
                    };break;
                    case 3 : {
                        JSONObject json =  new JSONObject((String)msg.obj);
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            JSONArray jsonArray = json.getJSONArray("images");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Map<String,String> map = new HashMap<String,String>();
                                JSONObject img  = jsonArray.getJSONObject(i);
                                map.put("ad_code",img.getString("ad_code"));
                                map.put("ad_link",img.getString("ad_link"));
                                list_imag.add(map);
                            }
                        }
                        initRollViewPager();
                    };break;
                    case 4 : {
                        JSONObject json =  new JSONObject((String)msg.obj);
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            setMessCount((int)json.getInt("msgcount"));
                        }
                    };
                    break;
                }

                super.handleMessage(msg);
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action)  {

        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 热卖系列接口
     */
    public void hotSaleData() {
//        loadingDalog.show();
        Map<String, String> formMap = new HashMap<String, String>();
        String url = Setting.hotSeries;
        JsonTools.getJsonAll(this, url, formMap, 0);
    }

    /**
     * 热卖商品接口
     */
    public void hotSaleGoodsData() {
        Map<String, String> formMap = new HashMap<String, String>();
        formMap.put("page",""+page);
        String url = Setting.hotGoods;
        JsonTools.getJsonAll(this, url, formMap, 1);
    }

    /**
     * 首页广告
     */
    public void homeAdData() {
        Map<String, String> formMap = new HashMap<String, String>();
        formMap.put("adtype",""+"indexAD");
//        String url = "http://192.168.2.208"+ "/api/ads.php?";
        String url = Setting.INDEX_FLASH;
        JsonTools.getJsonAll(this, url, formMap, 2);
    }

    /**
     * 头部轮播图片
     *
     */
    public void headImagData() {
        Map<String, String> formMap = new HashMap<String, String>();
        formMap.put("adtype",""+"indexFlash");
        String url = Setting.INDEX_FLASH;
//        String url = "http://192.168.2.208"+ "/api/ads.php?";
        JsonTools.getJsonAll(this, url, formMap, 3);
    }


    /**
     * 未读消息
     */
    private void messageNoRead() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.MY_MESSAGEN_NO_READ;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 4);
        }catch (Exception e) {
            Log.e("messageNoRead",e.toString());
        }
    }



    public void initPaging(JSONObject json) throws JSONException {
        String pageSizeString = json.getString("count");
        String countPageString = json.getString("countPage");
        String pageString = json.getString("page");
        if (pageSizeString != null && countPageString != null && pageString != null) {
            countPage = Integer.parseInt(countPageString);
            if (countPage <=1) {
                pullToRefreshLayout.stopLoadMore();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int arg1, Intent data) {
        // TODO Auto-generated method stub
        super.onActivityResult(requestCode, arg1, data);
        switch (requestCode) {
            case 101:// 相机
                if (data != null) {
                    if (data.getExtras() != null) {
                        Bundle bundle = data.getExtras();
                        Bitmap bitmap = bundle.getParcelable("data");
                        picture(bitmap);
                    } else {
                        U.Toast(getActivity(),getResources().getString(R.string.system_no_getpic));
                    }
                } else {
                    if (loadingDalog.getPhotoUrihotoUri() != null) {
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), loadingDalog.getPhotoUrihotoUri());
                            if (null != bitmap) {
                                picture(DownloadPicture.smallOneFifth(bitmap));
                            } else {
                                U.Toast(getActivity(),getResources().getString(R.string.system_no_getpic));
                            }
                        }catch (Exception e) {
                        }
                    } else {
                        U.Toast(getActivity(),getResources().getString(R.string.system_no_getpic));
                    }
                }
                break;
            case 102:// 相册
                Uri uri = null;
                if (data != null) {
                    if (data.getData() != null) {
                        uri = data.getData();
                        try {
                            Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                            if (null != bitmap) {
                                picture(DownloadPicture.smallOneFifth(bitmap));
                            }
                        }catch (Exception e) {
                        }
                    }
                }
                break;
        }
    }

    public void picture(Bitmap photo) {
        BufferedOutputStream bos = null;
        if (photo != null) {
            File dirFile = new File(Setting.IMAGE_ROOTPATH);
            if (!dirFile.exists())
                dirFile.mkdirs();
            SimpleDateFormat sDateFormat = new SimpleDateFormat(
                    "yyyyMMddhhmmss", Locale.ENGLISH);
            File uploadFile = new File(dirFile + "/"
                    + sDateFormat.format(new java.util.Date())
                    + ".jpg");

            char[] chars = "0123456789abcdef".toCharArray();
            StringBuilder sb = new StringBuilder("");
            int bit;
            try {
                uploadFile.createNewFile();
                bos = new BufferedOutputStream(new FileOutputStream(uploadFile));
                photo.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                FileInputStream fis = new FileInputStream(uploadFile);
                byte[] b = new byte[fis.available()];
                fis.read(b);
                for (int i = 0; i < b.length; i++) {
                    bit = (b[i] & 0x0f0) >> 4;
                    sb.append(chars[bit]);
                    bit = b[i] & 0x0f;
                    sb.append(chars[bit]);
                }

                fis.close();
//                String photoFile = uploadFile.getAbsolutePath();
                Intent  intent = new Intent(getActivity(),SearchActivity.class);
                intent.putExtra("photoFile","222");
                SearchActivity.photoFile = sb.toString();
                startActivity(intent);
            } catch (IOException e) {
                U.Toast(getActivity(), e.getLocalizedMessage());
            } finally {
                if (bos != null) {
                    try {
                        bos.flush();
                        bos.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
