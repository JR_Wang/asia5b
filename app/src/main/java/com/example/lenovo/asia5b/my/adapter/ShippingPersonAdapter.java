package com.example.lenovo.asia5b.my.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.activity.AddressActivity;
import com.example.lenovo.asia5b.my.activity.EditShippingPersonActivity;
import com.example.lenovo.asia5b.my.bean.ShippingPersonBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.PublicDialog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.rad_is_default;


/**
 * Created by lenovo on 2017/7/25.
 */

public class ShippingPersonAdapter extends BaseAdapter implements ICallBack {
    private LayoutInflater mInflater;
    private List<ShippingPersonBean> lists = null;
    private LoadingDalog loadingDalog;
    private Context context;
    private  int position1;

    public ShippingPersonAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<ShippingPersonBean>();
        loadingDalog = new LoadingDalog(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_address_list, null);
            holder.ll_address_list = (LinearLayout) convertView.findViewById(R.id.ll_address_list);
            holder.rad_is_default = (TextView) convertView.findViewById(rad_is_default);
            holder.txt_del = (TextView) convertView.findViewById(R.id.txt_del);
            holder.txt_name = (TextView) convertView.findViewById(R.id.txt_name);
            holder.txt_phone = (TextView) convertView.findViewById(R.id.txt_phone);
            holder.txt_edit = (TextView) convertView.findViewById(R.id.txt_edit);
            holder.ll_person = (LinearLayout)convertView.findViewById(R.id.ll_person);
            holder.ll_address = (LinearLayout)convertView.findViewById(R.id.ll_address);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        ShippingPersonBean bean = lists.get(position);
        holder.txt_del.setOnClickListener(new DelOnClickListener(position));
        holder.txt_edit.setOnClickListener(new StartOnClickListener(position));
        holder.ll_address_list.setOnClickListener(new AddressFinishOnClickListener(position));
        if (bean.is_default.equals("0")) {
            holder.rad_is_default.setVisibility(View.INVISIBLE);
        } else {
            holder.rad_is_default.setVisibility(View.VISIBLE);
        }
        holder.rad_is_default.setText(context.getResources().getString(R.string.mrshr));
        holder.txt_name.setText(context.getResources().getString(R.string.shr)+":\t" + bean.consignee);
        holder.txt_phone.setText(context.getResources().getString(R.string.mobile_phone2)+":\t" + bean.mobile);
        holder.ll_person.setVisibility(View.VISIBLE);
        holder.ll_address.setVisibility(View.GONE);
        return convertView;
    }

    private class ViewHolder {
        LinearLayout ll_address_list,ll_person,ll_address;
        TextView txt_del, txt_name, txt_phone, txt_edit,rad_is_default;
//        AppCompatRadioButton rad_is_default;
    }

    public void setLists(List<ShippingPersonBean> lists) {
        this.lists = lists;
    }

    public List<ShippingPersonBean> getLists() {
        return lists;
    }

    public class DelOnClickListener implements View.OnClickListener {
        int position;
        private DelOnClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            isDeleteDialog(position);
        }
    }

    public class StartOnClickListener implements View.OnClickListener {
        int position;

        private StartOnClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, EditShippingPersonActivity.class);
            intent.putExtra("bean", lists.get(position));
            ((AddressActivity)context).startActivityForResult(intent, 2);
        }
    }

    public class AddressFinishOnClickListener implements View.OnClickListener {
        int position;

        private AddressFinishOnClickListener(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.putExtra("ShippingPersonBean", lists.get(position));
            //设置返回数据
            ((Activity) context).setResult(0, intent);
            ((Activity) context).finish();
        }
    }

    public void delAddressData(int position) {
        loadingDalog.show();
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.DEL_RESS;
            String user_id = getUserBean().user_id;
            ShippingPersonBean bean = lists.get(position);
            String contact_id = bean.contact_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id" + user_id, "type"+2, "time" + time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("contact_id", contact_id);
            parmaMap.put("type","2");
            parmaMap.put("time", time);
            parmaMap.put("sign", md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
            position1 = position;
        } catch (Exception e) {

        }
    }
    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            lists.remove(position1);
                            notifyDataSetChanged();
                        } else {
                            U.Toast(context, context.getResources().getString(R.string.scsb_1));
                        }
                    } catch (Exception e) {

                    }
                }
            }
            loadingDalog.dismiss();
        }
    };


    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 弹出删除消息
     */
    public void isDeleteDialog(final  int position) {
        final PublicDialog dialog = new PublicDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.xtts));// 设置title
        dialog.setContent(context.getResources().getString(R.string.scshr));// 设置内容
        dialog.setLeftButton(context.getResources().getString(R.string.qx));// 设置按钮
        dialog.setRightButton(context.getResources().getString(R.string.confirm));
        dialog.setLeftButtonVisible(true); // true显示 false隐藏
        dialog.setRightButtonVisible(true);// true显示 false隐藏
        dialog.setRightButtonClick(new PublicDialog.OnClickListener() {

            @Override
            public void onClick(View v) {
                //调用删除消息接口
                delAddressData( position);
            }
        });
        dialog.setLeftButtonClick(new PublicDialog.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismissDialog();
            }
        });
        dialog.showDialog();
    }
}
