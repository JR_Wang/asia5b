package com.example.lenovo.asia5b.my.myWallet.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;

import com.example.lenovo.asia5b.my.myWallet.bean.RecordTopUpDetBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/6/29.
 */

public class RecordTopUpDetActivity extends BaseActivity implements ICallBack {
    private  String id;
    private LoadingDalog loadingDalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_record_top_up_det);
        initView();
        recordData();
    }

    public void initView() {
        loadingDalog = new LoadingDalog(RecordTopUpDetActivity.this);
        loadingDalog.show();
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.record_top_up));
        F.id(public_btn_left).clicked(this);
        if (getIntent().hasExtra("id")) {
            id = getIntent().getStringExtra("id");
        }
    }

    /**
     * 充值或者取现接口
     */
    public void recordData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.DIARY_INFO;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"jl_id"+id,"time"+time,"type1"};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("jl_id",""+id);
            parmaMap.put("time",time);
            parmaMap.put("type","1");
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                finish();
                break;
            default:
                break;
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json =  (JSONObject)result;
                        int state = (int)json.getInt("State");
                        if (state == 0) {
                            JSONObject account = json.getJSONObject("account");
                            RecordTopUpDetBean bean = (RecordTopUpDetBean)Logic.getBeanToJSONObject(account,new RecordTopUpDetBean());
                            F.id(R.id.txt_amount_tit).text("RM\t"+bean.amount);
                            F.id(R.id.txt_amount_account).text("RM\t"+bean.amount_account);
                            F.id(R.id.txt_type).text(bean.type);
                            F.id(R.id.txt_bank).text(bean.bank);
                            F.id(R.id.txt_add_time).text(DateUtils.timedate(bean.add_time));
                            if (!TextUtils.isEmpty(bean.paid_time)) {
                                F.id(R.id.txt_paid_time).text(DateUtils.timedate(bean.paid_time));
                            }
                            F.id(R.id.txt_serial).text(bean.serial);
                            String is_paid = "";
                            if (bean.is_paid.equals("0")) {
                                is_paid = getResources().getString(R.string.wait_affirm);
                            } else if (bean.is_paid.equals("1")) {
                                is_paid = getResources().getString(R.string.top_up_succeed);
                            } else if (bean.is_paid.equals("2")) {
                                is_paid = getResources().getString(R.string.czsb);
                            }
                            F.id(R.id.txt_process_type).text(is_paid);
                        }  else if (state == 1) {
                            U.Toast(RecordTopUpDetActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(RecordTopUpDetActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(RecordTopUpDetActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(RecordTopUpDetActivity.this, getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
                loadingDalog.dismiss();
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }
}