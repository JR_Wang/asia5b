package com.example.lenovo.asia5b.util;

import android.text.TextUtils;

import com.example.lenovo.asia5b.home.bean.UserBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lenovo on 2017/7/14.
 */

public class PayPassword {

    /**
     * 修改支付密码或设置支付密码，
     * 参数used_pass = null设置支付密码，used_pass != null修改支付密码，
     * @param oldpass 旧密码
     * @param newpass 新密码
     * @param code 验证码
     */
    public static void savePayPassword(ICallBack iCallBack,String oldpass,String newpass,String code) throws Exception{
        getPostAPI(iCallBack,oldpass,newpass,code);
    }

    private static void getPostAPI(ICallBack iCallBack,String oldpass,String newpass,String code) throws Exception{
        UserBean user = (UserBean) SharedPreferencesUtils.getObjectFromShare("user");
        String user_id = user.user_id;
        String payPasswordURL = Setting.UPDAPAYPASS;
        Map<String,String> map = new HashMap<String,String>();
        map.put("user_id",user_id);
        String[] sortStr;
        Date date = new Date();
        String time = String.valueOf(date.getTime());
        if(!TextUtils.isEmpty(oldpass)){
            map.put("used_paypass",oldpass);
            sortStr = new String[]{"user_id"+user_id,"new_paypass"+newpass,"code"+code,"time"+time,"used_paypass"+oldpass};
        } else {
            sortStr = new String[]{"user_id"+user_id,"new_paypass"+newpass,"code"+code,"time"+time};
        }
        map.put("new_paypass",newpass);
        map.put("code",code);
        map.put("time",time);
        String sort = Logic.sortToString(sortStr);
        String md5 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sort));
        map.put("sign",md5);

        JsonTools.getJsonAll(iCallBack,payPasswordURL,map,1);
    }
}
