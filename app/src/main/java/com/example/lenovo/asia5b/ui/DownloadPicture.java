package com.example.lenovo.asia5b.ui;

import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.os.Build;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.wushi.lenovo.asia5b.R;

/**
 * 下载图片和 对图片进行压缩操作
 * Created by lenovo on 2017/7/6.
 */

public class DownloadPicture {
    private static DisplayImageOptions options = null;
    private static DisplayImageOptions optionsHear = null;

    /**
     * 加载网络图片
     */
    public static void  loadNetwork (String imageUrl,ImageView mImageView) {
        ImageLoader.getInstance().displayImage(imageUrl, mImageView, getOptions());
    }

    /**
     * 加载个人头像图片
     */
    public static void  loadHearNetwork (String imageUrl,ImageView mImageView) {
        ImageLoader.getInstance().displayImage(imageUrl, mImageView, getHearOptions());
    }

    /**
     * 加载本地SD卡图片
     */
    public static void  loadThisLocality (String imageUrl,ImageView mImageView) {
        String locality = "file://" + imageUrl;
        ImageLoader.getInstance().displayImage(locality, mImageView, getOptions());
    }
    private static DisplayImageOptions getOptions() {
        if (options == null) {
            options = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.main_remaishangpin)            // 设置图片Uri为空或是错误的时候显示的图片
                    .showImageOnFail(R.drawable.main_remaishangpin)                  // 设置图片加载或解码过程中发生错误显示的图片
                    .cacheInMemory(true)                                          // 设置下载的图片是否缓存在内存中
                    .cacheOnDisk(true)                                            // 设置下载的图片是否缓存在SD卡中
                    .bitmapConfig(Bitmap.Config.RGB_565)                          //设置图片质量高低
                    .imageScaleType(ImageScaleType.IN_SAMPLE_INT)                //设置图片以如何的编码方式显示,现在是图像将被二次采样的整数倍
//                  .displayer(new RoundedBitmapDisplayer(20))                     // 设置成圆角图片,弧度为多少
//                  .showImageOnLoading(R.drawable.e_banking_info)                  //设置图片在下载期间显示的图片
//                  .decodingOptions(BitmapFactory.Options decodingOptions)        //设置图片的解码配置
//                  .delayBeforeLoading(0)                                          //为你设置的下载前的延迟时间
//                  .preProcessor(BitmapProcessor preProcessor)                      //设置图片加入缓存前，对bitmap进行设置
//                  .resetViewBeforeLoading(true)                                    //设置图片在下载前是否重置，复位
//                  .displayer(new FadeInBitmapDisplayer(100))                       //是否图片加载好后渐入的动画时间，可能会出现闪动
                    .build();                                                        //构建完成
        }
        return options;

    }

    private static DisplayImageOptions getHearOptions() {
        if (optionsHear == null) {
            optionsHear = new DisplayImageOptions.Builder()
                    .showImageForEmptyUri(R.drawable.default_head)            // 设置图片Uri为空或是错误的时候显示的图片
                    .showImageOnFail(R.drawable.default_head)                  // 设置图片加载或解码过程中发生错误显示的图片
                    .cacheInMemory(true)                                          // 设置下载的图片是否缓存在内存中
                    .cacheOnDisk(true)                                            // 设置下载的图片是否缓存在SD卡中
                    .bitmapConfig(Bitmap.Config.RGB_565)                          //设置图片质量高低
                    .imageScaleType(ImageScaleType.IN_SAMPLE_INT)                //设置图片以如何的编码方式显示,现在是图像将被二次采样的整数倍
//                  .displayer(new RoundedBitmapDisplayer(20))                     // 设置成圆角图片,弧度为多少
//                  .showImageOnLoading(R.drawable.e_banking_info)                  //设置图片在下载期间显示的图片
//                  .decodingOptions(BitmapFactory.Options decodingOptions)        //设置图片的解码配置
//                  .delayBeforeLoading(0)                                          //为你设置的下载前的延迟时间
//                  .preProcessor(BitmapProcessor preProcessor)                      //设置图片加入缓存前，对bitmap进行设置
//                  .resetViewBeforeLoading(true)                                    //设置图片在下载前是否重置，复位
//                  .displayer(new FadeInBitmapDisplayer(100))                       //是否图片加载好后渐入的动画时间，可能会出现闪动
                    .build();                                                        //构建完成
        }
        return optionsHear;

    }

    /**将Bitmap缩小5分之1的方法*/
    public static Bitmap smallOneFifth(Bitmap bitmap) {
        Matrix matrix = new Matrix();
        matrix.postScale(0.2f,0.2f); //长和宽放大缩小的比例
        Bitmap resizeBmp = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),bitmap.getHeight(),matrix,true);
        return resizeBmp;
    }

    public  static void clearCache() {
        //清除内存
        ImageLoader.getInstance().clearMemoryCache();
        //清除sd卡
        ImageLoader.getInstance().clearDiskCache();
    }

    //获取Bitmap的大小
    public static int getBitmapSize(Bitmap bitmap){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){    //API 19
            return bitmap.getAllocationByteCount();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1){//API 12
            return bitmap.getByteCount();
        }
        return bitmap.getRowBytes() * bitmap.getHeight();                //earlier version
    }
}
