package com.example.lenovo.asia5b.util;

import android.os.Handler;
import android.os.Message;

import com.example.lenovo.asia5b.home.bean.UserBean;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;



/**
 * 获取用户金额和积分
 * Created by lenovo on 2017/8/9.
 */

public class MoneyUtil implements ICallBack {
    private MoneyInterface moneyInterface;

    public MoneyUtil(MoneyInterface moneyInterface) {
        this.moneyInterface = moneyInterface;
    }
    /**
     * 获取用户金额 和积分
     */
    public  void  moneyData() {
        if (null == SharedPreferencesUtils.getUserBean()){
            return;
        }
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.USER_YJ;
            String user_id = SharedPreferencesUtils.getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id" + user_id,  "time" + time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("time", time);
            parmaMap.put("sign", md5_32);
            JsonTools.getJsonAll(MoneyUtil.this, url, parmaMap, 0);
        } catch (Exception e) {
        }
    }

    /**
     * 将金额和积分重新保存起来
     */
    public  void  savaMoney(String user_money,String pay_points) {
        if (null == SharedPreferencesUtils.getUserBean()) {
            return;
        }
        UserBean bean = SharedPreferencesUtils.getUserBean();
        bean.user_money = user_money;
        bean.pay_points = pay_points;
        SharedPreferencesUtils.setObjectToShare(bean, "user");
    }

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    public interface  MoneyInterface {
        public void money(String user_money,String pay_points);
    }


    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            String user_money="0",pay_points="0";
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            JSONObject user = json.getJSONObject("user");
                            user_money = user.getString("user_money");
                            pay_points = user.getString("pay_points");
                        }
                    } catch (Exception e) {

                    }
                }
                savaMoney(user_money,pay_points);
                moneyInterface.money(user_money,pay_points);
            }
        }
    };
}
