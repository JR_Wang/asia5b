package com.example.lenovo.asia5b.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.commodity.bean.ClassifyBean;

import java.util.ArrayList;
import java.util.List;

import app.BaseActivity;

/**
 * Created by lenovo on 2017/7/18.
 */

public class SelectPicPopupWindow extends BaseActivity {
    private   List<ClassifyBean> listCats;
    private   List<ClassifyBean> listBrands;
    private LinearLayout ll_hot_sale;
    private List<TextView> catsViews;
    private List<TextView> brandsViews;
    private EditText edit_min,edit_max;

    private String catsId="0",brandsId="0";
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_dialog);
        initView();

    }

    public void initView() {
        ll_hot_sale = (LinearLayout)findViewById(R.id.pop_layout);
        listCats = (List<ClassifyBean> )getIntent().getSerializableExtra("cat");

        listBrands= (List<ClassifyBean> )getIntent().getSerializableExtra("brand");
        if (null != listCats){
            catsViews = new ArrayList<TextView>();
            brandsViews = new ArrayList<TextView>();
            initTitView2(listBrands);
            TextView textView = new TextView(SelectPicPopupWindow.this);
            textView.setText(getResources().getString(R.string.splm));
            int wind = ((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));;
            textView.setBackgroundResource(R.color.white);
            textView.setPadding(wind, wind, wind, wind);
            textView.setTextColor(ContextCompat.getColor(SelectPicPopupWindow.this,R.color.my_333));
            ll_hot_sale.addView(textView,new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT));
            initTitView(listCats);
            View quantity =  LayoutInflater.from(SelectPicPopupWindow.this).inflate(R.layout.item_alert_dialog, null);
            edit_min = (EditText) quantity.findViewById(R.id.edit_min);
            edit_max = (EditText) quantity.findViewById(R.id.edit_max);
            ll_hot_sale.addView(quantity);
            View confirm =  LayoutInflater.from(SelectPicPopupWindow.this).inflate(R.layout.item_alert_dialog_2, null);
            Button btn_confirm = (Button) confirm.findViewById(R.id.btn_confirm);
            btn_confirm.setOnClickListener(this);
            ll_hot_sale.addView(confirm);
        }

    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return true;
    }

    // 实现onTouchEvent触屏函数但点击屏幕时销毁本Activity
    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btn_confirm:
                intent = new Intent();
                intent.putExtra("catsId",catsId);
                intent.putExtra("brandsId",brandsId);
                intent.putExtra("min_price",edit_min.getText().toString());
                intent.putExtra("max_price",edit_max.getText().toString());
                this.setResult(0, intent);
                finish();
                break;
            default:
                break;

        }

    }

    public void initTitView(List<ClassifyBean> lists) {
        LinearLayout  mLayout = null;
        for (int i = 0; i < lists.size(); i ++) {
            ClassifyBean bean = lists.get(i);
            if (i%3 == 0) {
                mLayout = new LinearLayout(SelectPicPopupWindow.this);
                mLayout.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                mLayout.setOrientation(LinearLayout.HORIZONTAL);
                mLayout.setWeightSum(3);
                ll_hot_sale.addView(mLayout);
            }
            View hotSaleView =  LayoutInflater.from(SelectPicPopupWindow.this).inflate(R.layout.item_cat, null);
            hotSaleView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            TextView txt_name = (TextView)hotSaleView.findViewById(R.id.txt_tit);
            txt_name.setTag(false);
            catsViews.add(txt_name);
            hotSaleView.setOnClickListener(new CatsOnClickListener(i));
            txt_name.setText(bean.getCat_name());
            mLayout.addView(hotSaleView);
        }
    }

    public void initTitView2(List<ClassifyBean> lists) {
        LinearLayout  mLayout = null;
        for (int i = 0; i < lists.size(); i ++) {
            ClassifyBean bean = lists.get(i);
            if (i%3 == 0) {
                mLayout = new LinearLayout(SelectPicPopupWindow.this);
                mLayout.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                mLayout.setOrientation(LinearLayout.HORIZONTAL);
                mLayout.setWeightSum(3);
                ll_hot_sale.addView(mLayout);
            }
            View hotSaleView =  LayoutInflater.from(SelectPicPopupWindow.this).inflate(R.layout.item_cat, null);
            hotSaleView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f));
            TextView txt_name = (TextView)hotSaleView.findViewById(R.id.txt_tit);
            txt_name.setTag(false);
            txt_name.setText(bean.getCat_name());
            brandsViews.add(txt_name);
            hotSaleView.setOnClickListener(new BrandsOnClickListener(i));
            mLayout.addView(hotSaleView);
        }
    }

    public class CatsOnClickListener implements View.OnClickListener {
        int i ;
        public CatsOnClickListener(int i) {
            this.i = i;
        }
        @Override
        public void onClick(View view) {
            initCats(i);
        }
    }

    public void initCats(int id) {
        boolean isSelect = (Boolean) catsViews.get(id).getTag();
        if (isSelect) {
            catsViews.get(id).setBackgroundResource(R.drawable.shape_background_gray_btn_2);
            catsViews.get(id).setTextColor(ContextCompat.getColor(SelectPicPopupWindow.this,R.color.my_333));
            catsId = "0";
            isSelect = false;
        } else {
            for (int i = 0; i< catsViews.size();i++) {
                catsViews.get(i).setBackgroundResource(R.drawable.shape_background_gray_btn_2);
                catsViews.get(i).setTextColor(ContextCompat.getColor(SelectPicPopupWindow.this,R.color.my_333));
            }
            catsViews.get(id).setBackgroundResource(R.drawable.shape_background_blue_btn_2);
            catsViews.get(id).setTextColor(ContextCompat.getColor(SelectPicPopupWindow.this,R.color.white));
            catsId = listCats.get(id).getCat_id();
            isSelect = true;
        }
        catsViews.get(id).setTag(isSelect);
    }

    public class BrandsOnClickListener implements View.OnClickListener {
        int i ;
        public BrandsOnClickListener(int i) {
            this.i = i;
        }
        @Override
        public void onClick(View view) {
            initBrands(i);
        }
    }

    public void initBrands(int id) {
        boolean isSelect = (Boolean) brandsViews.get(id).getTag();
        if (isSelect) {
            brandsViews.get(id).setBackgroundResource(R.drawable.shape_background_gray_btn_2);
            brandsViews.get(id).setTextColor(ContextCompat.getColor(SelectPicPopupWindow.this,R.color.my_333));
            brandsId = "0";
            isSelect = false;
        } else {
            for (int i = 0; i< brandsViews.size();i++) {
                brandsViews.get(i).setBackgroundResource(R.drawable.shape_background_gray_btn_2);
                brandsViews.get(i).setTextColor(ContextCompat.getColor(SelectPicPopupWindow.this,R.color.my_333));
            }
            brandsViews.get(id).setBackgroundResource(R.drawable.shape_background_blue_btn_2);
            brandsViews.get(id).setTextColor(ContextCompat.getColor(SelectPicPopupWindow.this,R.color.white));
            brandsId = listBrands.get(id).getCat_id();
            isSelect = true;
        }
        brandsViews.get(id).setTag(isSelect);
    }
}