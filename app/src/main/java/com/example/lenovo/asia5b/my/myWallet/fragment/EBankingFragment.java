package com.example.lenovo.asia5b.my.myWallet.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.home.activity.WebViewActivity;
import com.example.lenovo.asia5b.my.bean.BankBean;
import com.example.lenovo.asia5b.my.myWallet.activity.EBankingRechargeActivity;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import app.BaseFragment;
import fay.frame.ui.U;

/**
 * Created by lenovo on 2017/6/29.
 */

public class EBankingFragment extends BaseFragment implements ICallBack {
    public View fragmentView;
    public Button btn_finish_transfer;
    private List<BankBean> listBanks;
    private LinearLayout ll_bink;
    private LayoutInflater mInflater;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentView = inflater.inflate(R.layout.frame_e_banking, container,
                false);

        initView();
        bankData();
        return fragmentView;
    }

    public void initView() {
        btn_finish_transfer = (Button)fragmentView.findViewById(R.id.btn_finish_transfer);
        btn_finish_transfer.setOnClickListener(this);
        ll_bink = (LinearLayout)fragmentView.findViewById(R.id.ll_bink);
        this.mInflater = LayoutInflater.from(getActivity());
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btn_finish_transfer:
                intent = new Intent(getActivity(), EBankingRechargeActivity.class);
                intent.putExtra("type","3");
                startActivityForResult(intent,0);
                break;
            default:
                break;
        }
    }


    /**
     * 转入银行列表
     */
    public void bankData() {
        Map<String, String> parmaMap = new HashMap<String, String>();
        parmaMap.put("type","1");
        parmaMap.put("entry","0");
        parmaMap.put("dis_bank","1");
        String url = Setting.BANK;
        JsonTools.getJsonAll(this, url, parmaMap, 0);
    }

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            JSONArray bank = json.getJSONArray("bank");
                            listBanks = Logic.getListToBean(bank,new BankBean());
                            addbankViews();
                        } else if (state == 1) {
                            U.Toast(getActivity(), getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(getActivity(), getResources().getString(R.string.cscw));
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
    };

    public void addbankViews() {
        for (int i = 0; i < listBanks.size(); i++) {
            View convertView= mInflater.inflate(R.layout.item_e_banking, null);
            TextView txt_bank_account = (TextView)convertView.findViewById(R.id.txt_bank_account);
            TextView txt_bank_url = (TextView)convertView.findViewById(R.id.txt_bank_url);
            final  BankBean bean = listBanks.get(i);
            txt_bank_account.setText("Asia5B "+bean.bank_name+getResources().getString(R.string.zh_1)+":\t"+bean.bank_account);
            txt_bank_url.setText(bean.bank_name+" "+getActivity().getResources().getString(R.string.public_production_url));
            txt_bank_url.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), WebViewActivity.class);
                    intent.putExtra("url",bean.bank_url);
                    startActivity(intent);
                }
            });
            ll_bink.addView(convertView);
        }
    }

}
