package com.example.lenovo.asia5b.my.bean;

import java.io.Serializable;

/**
 * Created by apple on 2017/11/11.
 */

public class TestBean implements Serializable{

    private String mobile_picture;
    private String title;
    private String hint_en;
    private String identifying;

    private String id;
    private String hint_ch;
    private String add_time;
    private String is_show;

    public String getMobile_picture() {
        return mobile_picture;
    }

    public void setMobile_picture(String mobile_picture) {
        this.mobile_picture = mobile_picture;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHint_en() {
        return hint_en;
    }

    public void setHint_en(String hint_en) {
        this.hint_en = hint_en;
    }

    public String getIdentifying() {
        return identifying;
    }

    public void setIdentifying(String identifying) {
        this.identifying = identifying;
    }

}
