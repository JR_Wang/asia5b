package com.example.lenovo.asia5b.util;

/**
 * 平台名店商品详情过滤
 * Created by lenovo on 2017/7/1.
 */

public class InterceptUrl {
    public static final String TMALL_ONE = "detail.m.tmall.com";
    public static final String TMALL_TWO = "detail.tmall.com";

    public static final String TAOBAO_ONE = "h5.m.taobao.com/awp/core/detail.htm";
    public static final String TAOBAO_TWO = "ju.taobao.com/m/jusp/alone/detailwap";
    public static final String TAOBAO_THREE = "m.intl.taobao.com/detail/detail";
    public static final String TAOBAO_FOUR = "market";
    public static final String TAOBAO_FIVE = "m.taobao.com/tbopen/index.html";
    public static final String TAOBAO_SIX= "market.m.taobao.com/apps/guang/ishopping/detail";
    public static final String TAOBAO_SEVEN = "item.taobao.com/item.htm";
    public static final String TAOBAO_EIGHT = "h5.m.taobao.com";


    public static final String JD_ONE = "item.m.jd.com/ware/view.action";
    public static final String JD_TWO = "item.m.jd.com/product";
    public static final String JD_THREE = "mitem.jd.hk/product";
    public static final String JD_FOUR = "m.joybuy.com/ware/detail/";

    public static final String MOGUJIE_ONE = "m.mogujie.com/x6/napp/detail";
    public static final String MOGUJIE_TWO = "h5.mogujie.com/detail-normal";
    public static final String MOGUJIE_THREE = "m.mogujie.com/rush/seckill";
    public static final String MOGUJIE_FOUR = "m.mogujie.com/ms/x6/detail";
    public static final String MOGUJIE_FIVE = "shop.mogujie.com/detail";
    public static final String MOGUJIE_SIX = "m.mogujie.com/onecent/detail";

    public static final String MEILISHUO_ONE = "m.meilishuo.com/wap/detail";

    public static final String PINDDD_ONE = "mobile.yangkeduo.com/goods.html?goods_id";

    public static final String OurLink = "/goods.php?id";
}
