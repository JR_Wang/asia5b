package com.example.lenovo.asia5b.my.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/8/21.
 */

public class RefundBean implements Serializable {
    public String msg_id;
    public String rec_id;
    public String orderStatu;
    public String msg_title;
    public String content_apply;
    public String msg_price;
    public String msg_time;
    public String order_sn;
    public String pack_fee;
}
