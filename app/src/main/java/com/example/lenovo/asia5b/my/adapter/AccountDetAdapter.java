package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.AccountDetBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.StringUtil;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lenovo on 2017/6/29.
 */

public class AccountDetAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<AccountDetBean> lists = null;
    private Context context;


    public AccountDetAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<AccountDetBean>();
        this.context = context;
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_record_top_up, null);
            holder.ll_account_m = (LinearLayout)convertView.findViewById(R.id.ll_account_m);
            holder.txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txt_account_m = (TextView)convertView.findViewById(R.id.txt_account_m);
            holder.txt_amount = (TextView)convertView.findViewById(R.id.txt_amount);
            holder.txt_process_type = (TextView)convertView.findViewById(R.id.txt_process_type);
            holder.imageView = (ImageView)convertView.findViewById(R.id.imageView);
            holder.txt_invoice_no = (TextView)convertView.findViewById(R.id.txt_invoice_no);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        AccountDetBean bean = lists.get(position);
        String amount = bean.amount;
//        if (bean.amount.indexOf("-") < 0) {
//            holder.txt_amount.setText("RM +"+amount);
//        }else{
//            holder.txt_amount.setText("RM "+amount);
//        }
        holder.ll_account_m.setVisibility(View.VISIBLE);
        holder.txt_amount.setText("RM "+amount);
        holder.txt_account_m.setText(bean.balance);
        String time = DateUtils.timedate(bean.add_time);
        holder.txt_time.setText(time.replace(" ","\n"));
        holder.txt_process_type.setText(StringUtil.walletType(context,bean.process_type));
        holder.imageView.setImageResource(R.drawable.icon_mx_record);
        holder.txt_invoice_no.setText(bean.invoice_no);
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_time,txt_amount,txt_process_type,txt_invoice_no,txt_account_m;
        ImageView imageView;
        LinearLayout ll_account_m;
    }

    public void  setLists (List<AccountDetBean> lists) {
        this.lists = lists;
    }

    public List<AccountDetBean>  getLists () {
        return  lists;
    }


    public void clear() {
        lists.clear();
    }

}
