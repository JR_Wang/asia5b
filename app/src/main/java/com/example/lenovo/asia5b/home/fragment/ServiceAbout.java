package com.example.lenovo.asia5b.home.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.wushi.lenovo.asia5b.R;

import app.BaseFragment;

/**
 * Created by admin on 2017/6/23.
 */

public class ServiceAbout extends BaseFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.frame_service_about,null);
    }
}
