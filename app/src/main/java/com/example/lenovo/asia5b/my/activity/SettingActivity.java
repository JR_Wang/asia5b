package com.example.lenovo.asia5b.my.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

import com.example.lenovo.asia5b.home.activity.MainActivity;
import com.example.lenovo.asia5b.home.activity.WebViewActivity;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.example.lenovo.asia5b.util.SharedPreferencesUtils;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;


/**
 * Created by lenovo on 2017/7/3.
 */

public class SettingActivity extends BaseActivity implements ICallBack {
    LoadingDalog loadingDalog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initView();
    }

    private void initView(){
        loadingDalog =new LoadingDalog(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.setting));
        F.id(R.id.ll_update_phone).clicked(this);
        F.id(R.id.top_layout).clicked(this);
        F.id(R.id.ll_password_modify).clicked(this);
        F.id(R.id.ll_update_address).clicked(this);
        F.id(R.id.ll_language).clicked(this);
        F.id(R.id.btn_login_out).clicked(this);
        F.id(R.id.ll_service_about).clicked(this);
        F.id(R.id.activity_setting_my_data).clicked(this);
        F.id(R.id.ll_service_clear).clicked(this);
        F.id(R.id.ll_share).clicked(this);
        F.id(R.id.ll_help).clicked(this);
    }



    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.top_layout:
                finish();
                break;
            case R.id.ll_password_modify:
                intent = new Intent(this,PasswordModifyActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_update_address:
                Log.e("ccc","收货地址");
                startActivity(new Intent(this,AddressActivity.class));
                break;
            case R.id.ll_language:
                intent = new Intent(this,LanguagcActivity.class);
                startActivity(intent);
                break;
            case R.id.activity_setting_my_data:
                intent = new Intent(this,MyDataActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_service_about:
                //关于我们
                intent = new Intent(this,ServiceAboutActivity.class);

                startActivity(intent);
                break;
            case R.id.ll_help:
                //帮助中心
                intent = new Intent(this,WebViewActivity.class);
                intent.putExtra("bzzx","bzzx");
                SharedPreferences languagePre = getSharedPreferences("language_choice", Context.MODE_PRIVATE);
                int lang_id = languagePre.getInt("id", 1) + 1;
                //+"&lang_id="+lang_id
                intent.putExtra("url", Setting.HELP);
                startActivity(intent);
                break;
            case R.id.btn_login_out:
                loginOutDat();
                break;
            case R.id.ll_update_phone:
                intent = new Intent(this,UpdatePhoneActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_service_clear:
                DownloadPicture.clearCache();
                U.Toast(this,getResources().getString(R.string.cg));
                break;
            case R.id.ll_share:
                loadingDalog.showShare("   ","  ","   ","  ",R.drawable.wechat);
                break;
        }
    }

    /**
     * 退出登录接口
     */
    public void loginOutDat() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.LOG_OUT;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        } catch (Exception e) {

        }

    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            try {
                if (action == 0) {
                    if (null != result && result instanceof JSONObject) {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            finish();
                            //清空数据
                            SharedPreferencesUtils.clear();
                            SharedPreferencesUtils.setObjectToShare(null, "user");
                            finishAll();
                            U.Toast(SettingActivity.this,getResources().getString(R.string.yhyzx));
                            Intent intent = new Intent(SettingActivity.this, MainActivity.class);
                            startActivity(intent);
                        }  else if (state == 1) {
                            U.Toast(SettingActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(SettingActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(SettingActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(SettingActivity.this, getResources().getString(R.string.tcsb));
                        }
                    } else {
                        U.Toast(SettingActivity.this, getResources().getString(R.string.tcsb));
                    }
                }
            } catch (Exception e) {

            }
        }
    };


    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

}
