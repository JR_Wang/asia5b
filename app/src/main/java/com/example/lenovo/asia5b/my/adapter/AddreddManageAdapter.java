package com.example.lenovo.asia5b.my.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.activity.AddressActivity;
import com.example.lenovo.asia5b.my.activity.EditAddressActivity;
import com.example.lenovo.asia5b.my.bean.AddressBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.ui.PublicDialog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;


/**
 * 收货地址列表
 * Created by lenovo on 2017/6/20.
 */

public class AddreddManageAdapter extends BaseAdapter implements ICallBack {
    private LayoutInflater mInflater;
    private List<AddressBean> lists = null;
    private int position1;
    private LoadingDalog loadingDalog;
    private Context context;

    public AddreddManageAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        lists  =new ArrayList<AddressBean>();
        loadingDalog = new LoadingDalog(context);
        this.context = context;
    }

    @Override
    public int getCount() {
        return lists.size();
    }
    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }
    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_address_list, null);
            holder.ll_address_list = (LinearLayout)convertView.findViewById(R.id.ll_address_list);
            holder.txt_address = (TextView)convertView.findViewById(R.id.txt_address);
            holder.rad_is_default = (TextView)convertView.findViewById(R.id.rad_is_default);
            holder.txt_del= (TextView)convertView.findViewById(R.id.txt_del);
            holder.txt_edit= (TextView)convertView.findViewById(R.id.txt_edit);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        AddressBean bean = lists.get(position);
        holder.txt_address.setText(context.getResources().getString(R.string.shipping_address)+":"+bean.address+bean.province+bean.country_2+"\n"+bean.city);
        holder.txt_del.setOnClickListener(new DelOnClickListener(position));
        holder.txt_edit.setOnClickListener(new StartOnClickListener(position));
        holder.ll_address_list.setOnClickListener(new AddressFinishOnClickListener(position));
        if (bean.is_default.equals("0")) {
            holder.rad_is_default.setVisibility(View.INVISIBLE);
        } else {
            holder.rad_is_default.setVisibility(View.VISIBLE);
        }
        return convertView;
    }

    private class ViewHolder {
        LinearLayout ll_address_list;
        TextView txt_address,txt_del,txt_edit,rad_is_default;
//        AppCompatRadioButton  rad_is_default;
    }

    public void setLists(List<AddressBean> lists) {
        this.lists = lists;
    }

    public List<AddressBean> getLists() {
        return lists;
    }

    public class DelOnClickListener implements View.OnClickListener {
        int position;

        private DelOnClickListener(int position) {

            this.position = position;
        }
        @Override
        public void onClick(View v) {
            isDeleteDialog(position);
        }
    }

    public class StartOnClickListener implements View.OnClickListener {
        int position;
        private StartOnClickListener(int position) {
            this.position = position;
        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, EditAddressActivity.class);
            intent.putExtra("bean",lists.get(position));
            ((AddressActivity)context).startActivityForResult(intent,1);
        }
    }

    public class  AddressFinishOnClickListener implements View.OnClickListener {
        int position;
        private AddressFinishOnClickListener(int position) {
            this.position = position;
        }
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            intent.putExtra("AddressBean", lists.get(position));
            //设置返回数据
            ((Activity)context).setResult(0, intent);
            ((Activity)context).finish();
        }
    }

    public void delAddressData(int position) {
        loadingDalog.show();
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.DEL_RESS;
            String user_id = getUserBean().user_id;
            AddressBean bean = lists.get(position);
            String address_id = bean.address_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"type"+1,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("address_id", address_id);
            parmaMap.put("type", "1");
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
            position1 = position;
        }catch (Exception e) {

        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            lists.remove(position1);
                            notifyDataSetChanged();
                        } else  {
                            U.Toast(context,context.getResources().getString(R.string.scsb_1));
                        }
                    }catch (Exception e) {

                    }
                }
            }
            loadingDalog.dismiss();
        }
    };


    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 弹出删除消息
     */
    public void isDeleteDialog(final int position) {
        final PublicDialog dialog = new PublicDialog(context);
        dialog.setTitle(context.getResources().getString(R.string.xtts));// 设置title
        dialog.setContent(context.getResources().getString(R.string.scdz));// 设置内容
        dialog.setLeftButton(context.getResources().getString(R.string.qx));// 设置按钮
        dialog.setRightButton(context.getResources().getString(R.string.confirm));
        dialog.setLeftButtonVisible(true); // true显示 false隐藏
        dialog.setRightButtonVisible(true);// true显示 false隐藏
        dialog.setRightButtonClick(new PublicDialog.OnClickListener() {

            @Override
            public void onClick(View v) {
                //调用删除消息接口
                delAddressData(position);
            }
        });
        dialog.setLeftButtonClick(new PublicDialog.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismissDialog();
            }
        });
        dialog.showDialog();
    }
}



