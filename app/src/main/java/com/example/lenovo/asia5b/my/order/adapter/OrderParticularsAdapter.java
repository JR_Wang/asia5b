package com.example.lenovo.asia5b.my.order.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.order.bean.OrderDetailBean;
import com.example.lenovo.asia5b.ui.DownloadPicture;
import com.example.lenovo.asia5b.util.StringUtil;
import com.wushi.lenovo.asia5b.R;

import java.util.List;

/**
 * Created by lenovo on 2017/6/20.
 */

public class OrderParticularsAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<OrderDetailBean> OrderDetailList;
    private  Context context;
    private String order_status ="";

    public OrderParticularsAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context =context;
    }

    @Override
    public int getCount() {
        return OrderDetailList.size();
    }
    @Override
    public Object getItem(int arg0) {
        return OrderDetailList.get(arg0);
    }
    @Override
    public long getItemId(int arg0) {
        return arg0;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(R.layout.item_orders_particulars, null);
            holder.item_orders_particulars_img = convertView.findViewById(R.id.item_orders_particulars_img);

            holder.item_orders_particulars_goods_name = convertView.findViewById(R.id.item_orders_particulars_goods_name);
            holder.item_orders_particulars_goods_price = convertView.findViewById(R.id.item_orders_particulars_goods_price);
            holder.item_orders_particulars_goods_original_price = convertView.findViewById(R.id.item_orders_particulars_goods_original_price);
            holder.item_orders_particulars_goods_number = convertView.findViewById(R.id.item_orders_particulars_goods_number);
            holder.item_orders_particulars_goods_attr = convertView.findViewById(R.id.item_orders_particulars_goods_attr);
            holder.item_parcel_list_orderStatu = convertView.findViewById(R.id.item_parcel_list_orderStatu);
            holder.txt_china_fee = (TextView)convertView.findViewById(R.id.txt_china_fee);
            holder.ll_china_fee = (LinearLayout)convertView.findViewById(R.id.ll_china_fee);
            holder.txt_weight = (TextView)convertView.findViewById(R.id.txt_weight);
            holder.txt_type = (TextView)convertView.findViewById(R.id.txt_type);
            holder.txt_market_price = (TextView)convertView.findViewById(R.id.txt_market_price);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        OrderDetailBean orderDetailBean = OrderDetailList.get(position);
        DownloadPicture.loadNetwork(orderDetailBean.goods_attr_thumb,holder.item_orders_particulars_img);
        holder.item_orders_particulars_goods_name.setText(orderDetailBean.goods_name);
        holder.item_orders_particulars_goods_price.setText("RM "+orderDetailBean.goods_price);
//        holder.item_orders_particulars_goods_original_price.setText(orderDetailBean);
//        holder.item_orders_particulars_goods_original_price.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG );
        holder.item_orders_particulars_goods_number.setText("x"+orderDetailBean.goods_number);
        holder.item_orders_particulars_goods_attr.setText("："+orderDetailBean.goods_attr);
        holder.txt_weight.setText(orderDetailBean.weight);
        String type = StringUtil.orderStatu(context,orderDetailBean.orderStatu,orderDetailBean.fillStatu,orderDetailBean.cancle_type,order_status,orderDetailBean.express_code);
        holder.item_parcel_list_orderStatu.setText("："+ type);//包裹状态
        //中国运费
        if (OrderDetailList.size()-1 == position) {
            holder.ll_china_fee.setVisibility(View.VISIBLE);
            holder.txt_china_fee.setText("RM "+ orderDetailBean.china_fee);
        } else {
            OrderDetailBean bean2 = OrderDetailList.get(position+1);
            if (orderDetailBean.sign .equals(bean2.sign)) {
                holder.ll_china_fee.setVisibility(View.GONE);
            } else {
                holder.txt_china_fee.setText("RM "+orderDetailBean.china_fee);
                holder.ll_china_fee.setVisibility(View.VISIBLE);
            }
        }
        if (type.equals(context.getResources().getString(R.string.qhqx))||type.equals(context.getResources().getString(R.string.qxdg))||
                type.equals(context.getResources().getString(R.string.th))||
                type.equals(context.getResources().getString(R.string.ngqx))) {
            holder.txt_type.setText(type);
        }  if(TextUtils.equals("7",orderDetailBean.orderStatu)) {
            if (!orderDetailBean.fillStatu.equals("1")&&orderDetailBean.cancle_type.equals("0")){
            } else if (orderDetailBean.fillStatu.equals("1")){
            } else  if (orderDetailBean.fillStatu.equals("1")) {
                 holder.txt_type.setText(context.getResources().getString(R.string.shqx));
            }
        }
        else {
            holder.txt_type.setText("");
        }
        if (!orderDetailBean.goods_price.equals(orderDetailBean.examine_price)) {
            holder.txt_market_price.setText("RM" + orderDetailBean.examine_price);
            holder.txt_market_price.setVisibility(View.VISIBLE);
        } else{
            holder.txt_market_price.setVisibility(View.GONE);
        }
        holder.txt_market_price.getPaint().setFlags(Paint. STRIKE_THRU_TEXT_FLAG );
        return convertView;
    }

    private class ViewHolder {
        CheckBox cb_pro_checkbox;
        ImageView item_orders_particulars_img;
        TextView item_orders_particulars_goods_name,item_orders_particulars_goods_price,item_orders_particulars_goods_original_price,
                item_orders_particulars_goods_number,item_orders_particulars_goods_attr,item_parcel_list_orderStatu,txt_china_fee,txt_weight,txt_type,txt_market_price;
        LinearLayout ll_china_fee;
    }

    public void setOrderDetailBean(List<OrderDetailBean> OrderDetailList){
        this.OrderDetailList = OrderDetailList;
    }

    public void setOrderStatus(String order_status) {
        this.order_status = order_status;
    }

}



