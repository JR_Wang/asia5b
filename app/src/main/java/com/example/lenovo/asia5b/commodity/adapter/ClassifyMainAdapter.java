package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.commodity.bean.ClassifyBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lenovo on 2017/6/23.
 */

public class ClassifyMainAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private int cur_pos = 0;// 当前显示的一行
    private Context context;
    private List<ClassifyBean> lists;


    public ClassifyMainAdapter(Context context){
        this.context = context;
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<ClassifyBean>();
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_classify_main, null);
            holder.txt_tit = (TextView) convertView.findViewById(R.id.txt_tit);
            convertView.setTag(holder);

        }else {

            holder = (ViewHolder)convertView.getTag();
        }
        ClassifyBean bean = lists.get(position);
        holder.txt_tit.setText(bean.getCat_name());
        if (position == cur_pos) {// 如果当前的行就是ListView中选中的一行，就更改显示样式
            convertView.setBackgroundColor(context.getResources().getColor(R.color.my_ebe));
            holder.txt_tit.setTextColor(context.getResources().getColor(R.color.wathet2));
        } else {
            convertView.setBackgroundColor(Color.WHITE);
            holder.txt_tit.setTextColor(context.getResources().getColor(R.color.my_4e4));
        }
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_tit;
    }

    public void setCurPos(int cur_pos) {
        this.cur_pos = cur_pos;
    }

    public void  setLists (List<ClassifyBean> lists) {
        this.lists = lists;
    }

    public List<ClassifyBean>  getLists () {
        return  lists;
    }

}
