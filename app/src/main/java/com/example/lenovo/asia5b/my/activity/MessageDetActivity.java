package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;

import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.JsonTools;
import com.example.lenovo.asia5b.util.Logic;
import com.example.lenovo.asia5b.util.MD5;
import com.example.lenovo.asia5b.util.Setting;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;
import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/7/12.
 */

public class MessageDetActivity extends BaseActivity implements ICallBack {
    private String msg_id="";
    public boolean isSelect;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_det);
        initView();
    }

    public void initView() {
        if (getIntent().hasExtra("serviceAbout")) {
            F.id(R.id.public_title_name).text(getResources().getString(R.string.service_about));
        } else {
            F.id(R.id.public_title_name).text(getResources().getString(R.string.my_message));
        }
        F.id(public_btn_left).clicked(this);
        if (getIntent().hasExtra("content")) {
            F.id(R.id.txt_content).text(getIntent().getStringExtra("content"));
        } else {
            F.id(R.id.txt_content).text("");
        }
        if (getIntent().hasExtra("id")) {
            msg_id = getIntent().getStringExtra("id");
            signReadData();
        }
    }
    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                if (isSelect) {
                    intent = new Intent();
                    intent.putExtra("222",2222);
                    this.setResult(0, intent);
                }
                finish();
                break;
            default:
                break;
        }
    }


    /**
     * 获取我的消息数据
     */
    public void signReadData() {
        try {
            Map<String, String> parmaMap = new HashMap<String, String>();
            String url = Setting.SIGN_READ;
            String user_id = getUserBean().user_id;
            Date date = new Date();
            String time = String.valueOf(date.getTime());
            String[] sort = {"user_id"+user_id,"msg_id"+msg_id,"time"+time};
            String sortStr = Logic.sortToString(sort);
            String md5_32 = MD5.MD5EncryptionFor32(MD5.MD5EncryptionFor32(sortStr));//二次加密
            parmaMap.put("user_id", user_id);
            parmaMap.put("msg_id",""+msg_id);
            parmaMap.put("time",time);
            parmaMap.put("sign",md5_32);
            JsonTools.getJsonAll(this, url, parmaMap, 0);
        }catch (Exception e) {

        }

    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            if (action == 0) {
                if (null != result && result instanceof JSONObject) {
                    try {
                        JSONObject json = (JSONObject) result;
                        int state = (int) json.getInt("State");
                        if (state == 0) {
                            isSelect = true;
                        } else if (state == 1) {
                            U.Toast(MessageDetActivity.this, getResources().getString(R.string.qmsb));
                        } else if (state == 2) {
                            U.Toast(MessageDetActivity.this, getResources().getString(R.string.cscw));
                        } else if (state == 3) {
                            U.Toast(MessageDetActivity.this, getResources().getString(R.string.wdl));
                        } else if (state == 4) {
                            U.Toast(MessageDetActivity.this, getResources().getString(R.string.mysj));
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断用户是否点击的是返回键
        if(keyCode == KeyEvent.KEYCODE_BACK){
            if (isSelect) {
                Intent intent = new Intent();
                intent.putExtra("222",2222);
                this.setResult(0, intent);
            }
            finish();

        }
        return false;
    }


}
