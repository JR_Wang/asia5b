package com.example.lenovo.asia5b.my.order.bean;

/**
 * Created by admin on 2017/7/11.
 */

public class OrderDetailBean {

    public String rec_id;
    public String goods_id;
    public String goods_name;
    public String goods_attr_thumb;
    public String goods_price;
    public String goods_number;
    public String orderStatu;
    public String goods_attr;
    public String fillStatu;
    public String cancle_type;
    public String china_fee;
    public String weight;
    public String sign;
    public String express_code="";
    public String examine_price = "";
}
