package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.lenovo.asia5b.my.myWallet.activity.MyWalletActivity;
import com.example.lenovo.asia5b.util.MoneyUtil;
import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.ll_refund;

/**
 * Created by lenovo on 2017/7/3.
 */

public class MyWalletDetailActivity extends BaseActivity implements  MoneyUtil.MoneyInterface {
    private MoneyUtil moneyUtil = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        initView();
    }

    private void initView(){
        moneyUtil= new MoneyUtil(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.my_wallet));
        F.id(R.id.top_layout).clicked(this);
        F.id(R.id.ll_my_wallet).clicked(this);
        F.id(R.id.ll_access_account).clicked(this);
        F.id(R.id.ll_account_details).clicked(this);
        F.id(R.id.ll_favorable_integral).clicked(this);
        F.id(R.id.ll_chit).clicked(this);
        F.id(R.id.ll_refund).clicked(this);
        moneyUtil.moneyData();
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.top_layout:
                finish();
                break;
            case R.id.ll_my_wallet:
                //账户充值
                intent = new Intent(this, MyWalletActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_access_account:
                //取现账户
                intent = new Intent(this, AccessAccountActivity.class);
                startActivityForResult(intent,0);
                break;
            case R.id.ll_account_details:
                //账户明细
                intent = new Intent(this, AccountDetailsActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_favorable_integral:
                //优惠记录
                intent = new Intent(this, FavorableIntegralActivity.class);
                startActivity(intent);
                break;
            case R.id.ll_chit:
                //代金券
                intent = new Intent(this, ChitActivity.class);
                startActivity(intent);
                break;
            case ll_refund:
                //退货记录
                intent = new Intent(this, RefundActivity.class);
                startActivity(intent);
                break;
        }
    }

    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {
            case 0:
                if (data == null) {
                    return;
                }
                moneyUtil.moneyData();
                break;
            default:
                break;
        }
    }

    @Override
    public void money(String user_money,String pay_points) {
        F.id(R.id.txt_user_money).text(getResources().getString(R.string.account_details2)+":\t"+"RM"+user_money);

    }
}
