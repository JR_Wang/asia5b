package com.example.lenovo.asia5b.util;

public class Setting {
	public final static String SECRETKEY = "asia5b";//密钥
	public final static String appId = "c0470d160a0101104c37f78f2bd9e0b0";
	public static final String hostUrl = "http://47.88.220.11:81";// 测试环境
//	public static final String hostUrl = "http://192.168.2.52";// 强工电脑
//    public static final String hostUrl ="http://192.168.2.80";//常工电脑
//public static final String hostUrl ="http://39.108.1.89:82";//电脑
//	public final static String hostUrl = "https://asia5b.com";//正式环境

	/**
	 *商品翻译
	 */
	public final static String  APPGOODS = "https://m.asia5b.com"+"/appgoods.php?id=";//正式环境
//	public final static String  APPGOODS = "http://192.168.2.80:81"+"/appgoods.php?id=";//常工
//	public final static String  APPGOODS = "http://192.168.2.199"+"/appgoods.php?id=";//吴工
//  public final static String  APPGOODS = "http://47.88.220.11:99"+"/appgoods.php?id=";//测试

	public final static String uploadFiles = "https://www.xy-yunha n.com/palmres/uploadFiles";
	/**
	 *短信接口
	 */
	public final static String sendsms = hostUrl+"/api.php?act=sendsms";

	/**
	 * 热卖系列
	 */
	public final static String hotSeries = hostUrl+"/api/goods.php?act=hot_series";

	/**
	 * 热卖商品
	 */
	public final static String hotGoods = hostUrl+"/api/goods.php?act=hot_goods";

	/**
	 * 名站推荐
	 */
	public final static String WELLKNOWN = hostUrl+"/api/goods.php?act=recommend";

	/**
	 * 分类接口
	 */
	public final static String ACTCATE = hostUrl+"/api/goods.php?act=cate";

	/**
	 * 用户名密码登录
	 */
	public final static String LOGINFORUSERANDPWD = hostUrl+"/api/user.php?act=ap_login";

	/**
	 * 订单列表
	 */
	public final static String ORDERLIST = hostUrl+"/api/order.php?act=my_order";

	/**
	 * 订单包裹
	 */
	public final static String ORDERWRAP = hostUrl+"/api/order.php?act=order_goods";

	/**
	 * 订单详情
	 */
	public final static String ORDERDETAILS = hostUrl+"/api/order.php?act=order_info";

	/**
	 * 我的资料
	 */
	public final static String MY_INF0 = hostUrl+"/api/user.php?act=myinfo";

	/**
	 * 上传图片
	 */
	public final static String UPLODE_IMAGE = hostUrl+"/api/user.php?act=uplode_image";

	/**
	 * 修改我的资料
	 */
	public final static String UPDA_INFO = hostUrl+"/api/user.php?act=upda_info";

	/**
	 * 地址名称
	 */
	public final static String REGION_LIST = hostUrl+"/api/user.php?act=region_list";

	/**
	 * 商品详情
	 */
	public final static String GOODS_DETAILS = hostUrl+"/api/goods.php?act=goods_details";

	/**
	 * 账户明细列表
	 */
	public final static String USER_ACCOUNT = hostUrl+"/api/user.php?act=user_account";

	/**
	 * 充值提现记录接口
	 */
	public final static String CZ_DIARY = hostUrl+"/api/user.php?act=cz_diary";

	/**
	 * 退出接口
	 */
	public final static String LOG_OUT = hostUrl+"/api/user.php?act=logout";

	/**
	 *我的消息
	 */
	public final static String MY_MESSAGE = hostUrl+"/api/user.php?act=mymessage";

	/**
	 *充值提现记录详情接口
	 */
	public final static String DIARY_INFO = hostUrl+"/api/user.php?act=diary_info";

	/**
	 * 注册
	 */
	public final static String REGISTER = hostUrl+"/api/user.php?act=register";

	/**
	 * 修改密码
	 */
	public final static String UPDATEPASSWORD = hostUrl+"/api/user.php?act=upda_pass";

	/**
	 *加入购物车
	 */
	public final static String ADD_CART = hostUrl+"/api/user.php?act=add_cart";
	/**
	 *购物车列表
	 */
	public final static String CART_LIST = hostUrl+"/api/user.php?act=cart_list";
	/**
	 *删除购物车
	 */
	public final static String DEL_CART = hostUrl+"/api/user.php?act=del_cart";

	/**
	 *未读消息次数
	 */
	public final static String MY_MESSAGEN_NO_READ = hostUrl+"/api/user.php?act=user_message";
	/**
	 *删除我的消息
	 */
	public final static String ONE_REMOMSG = hostUrl+"/api/user.php?act=one_remomsg";
	/**
	 *清空我的消息
	 */
	public final static String REMO_MESSAGE = hostUrl+"/api/user.php?act=remo_message";
	/**
	 *商品详情里的推荐商品
	 */
	public final static String GOODS_PROMOTE = hostUrl+"/api/goods.php?act=goods_promote";
	/**
	 *提现
	 */
	public final static String WITHDRAWALS = hostUrl+"/api/user.php?act=withdrawals";
	/**
	 *银行列表
	 */
	public final static String BANK = hostUrl+"/api/user.php?act=bank";
	/**
	 *账号类型
	 */
	public final static String BANK_TYPE = hostUrl+"/api/user.php?act=bank_type";
	/**
	 *充值接口
	 */
	public final static String RECHARGE = hostUrl+"/api/user.php?act=recharge";

	/**
	 *我的积分接口
	 */
	public final static String INTEGRAL = hostUrl+"/api/user.php?act=integral";

	/**
	 * 手机短信登录
	 */
	public final static String LOGINTOPHONE = hostUrl+"/api/user.php?act=login";

	/**
	 * 忘记密码
	 */
	public final static String BACKPASS = hostUrl+"/api/user.php?act=back_pass";

	/**
	 * 修改支付密码
	 */
	public final static String UPDAPAYPASS = hostUrl+"/api/user.php?act=upda_paypass";

	/**
	 *我的代金券
	 */
	public final static String VOUCHER = hostUrl+"/api/user.php?act=voucher";

	/**
	 *显示确认订单界面
	 */
	public final static String PAY_ORDER = hostUrl+"/api/order.php?act=pay_order";

	/**
	 *修改购物车数量界面
	 */
	public final static String UPDA_CART = hostUrl+"/api/user.php?act=upda_cart";

	/**
	 * 支付密码找回
	 */
	public final static String BACKPAYPASS = hostUrl+"/api/user.php?act=back_paypass";

	/**
	 * 搜索
	 */
	public final static String SEARCH = hostUrl+"/api/goods.php?act=search";
	/**
	 * 分类列表对应的商品列表、热卖分类商品列表
	 */
	public final static String CAT_GOODS = hostUrl+"/api/goods.php?act=cat_goods";
    /**
     * 轮播图片、通栏广告
     */
    public final static String INDEX_FLASH = hostUrl+"/api/ads.php?";
	/**
	 * 确认付款
	 */
	public final static String DROP_PAY = hostUrl+"/api/order.php?act=drop_pay";

	/**
	 * 收藏
	 */
	public final static String COLLECT = hostUrl+"/api/user.php?act=collect";

	/**
	 * 取消收藏
	 */
	public final static String UN_COLLECT = hostUrl+"/api/user.php?act=uncollect";

    /**
     * 收货地址列表
     */
    public final static String RESS_LIST = hostUrl+"/api/user.php?act=ress_list";
	/**
	 * 添加收货地址
	 */
	public final static String ADD_RESS = hostUrl+"/api/user.php?act=add_ress";
	/**
	 * 删除收货地址
	 */
	public final static String DEL_RESS = hostUrl+"/api/user.php?act=del_ress";
	/**
	 * 本地运输方式
	 */
	public final static String BD_SHIPPING = hostUrl+"/api/user.php?act=bd_shipping";
	/**
	 * 国际运输方式
	 */
	public final static String SHIPPING = hostUrl+"/api/user.php?act=shipping";

    /**
     * 提交订单
     */
    public final static String  DONE = hostUrl+"/api/order.php?act=done";

	/**
	 * 新品推荐、精品推荐
	 */
	public final static String  GOODS_NEW = hostUrl+"/api/goods.php?act=goods_new";
	/**
	 * 收藏列表
	 */
	public final static String  COLLECTLIST = hostUrl+"/api/user.php?act=collectlist";
	/**
	 * 清空收藏、清空历史记录
	 */
	public final static String  CLEAR = hostUrl+"/api/user.php?act=clear";

	/**
	 * 我的足迹列表
	 */
	public final static String  HISTORYLIST = hostUrl+"/api/user.php?act=historylist";

	/**
	 * 我的物流列表
	 */
	public final static String  ORDER_SHIPPING = hostUrl+"/api/order.php?act=order_shipping";

	/**
	 * 信用卡充值
	 */
	public final static String  CREDIT_PAY = hostUrl+"/api/user.php?act=creditPay";
	/**
	 * 修改收货地址
	 */
	public final static String  UPDA_RESS = hostUrl+"/api/user.php?act=upda_ress";
	/**
	 * 合并付款和特殊商品付款界面
	 */
	public final static String  GO_MERGE_ORDER = hostUrl+"/api/order.php?act=go_merge_order";
	/**
	 * 取消订购
	 */
	public final static String  CANCEL_ORDER = hostUrl+"/api/order.php?act=cancel_order";
	/**
	 * 重新提交审核
	 */
	public final static String  REVIEWED = hostUrl+"/api/order.php?act=reviewed";
	/**
	 * 合并付款和特殊商品付款提交
	 */
	public final static String  GO_MERGE_CHULI = hostUrl+"/api/order.php?act=go_merge_chuli";
	/**
	 * 补差价界面
	 */
	public final static String  DIFF_LIST = hostUrl+"/api/order.php?act=diff_list";
	/**
	 * 申请退货
	 */
	public final static String  APPLY_REFUND = hostUrl+"/api/user.php?act=apply_refund";
	/**
	 * 查看退货
	 */
	public final static String  CHECK_REFUND = hostUrl+"/api/user.php?act=check_refund";
	/**
	 * 补差价付款
	 */
	public final static String  GO_TO_FIFFE = hostUrl+"/api/order.php?act=go_to_fiffe";
	/**
	 * 第三方绑定
	 */
	public final static String  THIRD_REG = hostUrl+"/api/user.php?act=third_reg";
	/**
	 * 第三方登录
	 */
	public final static String  THIRD_LOGIN = hostUrl+"/api/user.php?act=third_login";
	/**
	 *补差价时的取消订购
	 */
	public final static String  CANCEL_PAY_GOODS = hostUrl+"/api/order.php?act=cancel_pay_goods";
	/**
	 *评论列表
	 */
	public final static String  GOODS_COMMENLIST = hostUrl+"/api/goods.php?act=goods_commentlist";
	/**
	 *点赞
	 */
	public final static String  COMMENT_LIKE = hostUrl+"/api/goods.php?act=comment_like";
	/**
	 *评论回复列表
	 */
	public final static String  GOODS_RECOMMLIST = hostUrl+"/api/goods.php?act=goods_recommlist";
	/**
	 *提交评论
	 */
	public final static String  GOODS_COMMENT = hostUrl+"/api/goods.php?act=goods_comment";
	/**
	 *订单完成后提交商品评论
	 */
	public final static String  ORDER_COMMENT = hostUrl+"/api/order.php?act=order_comment";
	/**
	 *获取金额和积分
	 */
	public final static String  USER_YJ = hostUrl+"/api/user.php?act=user_yj";
	/**
	 *FPX充值
	 */
	public final static String  FPXBANKPAY = hostUrl+"/api/user.php?act=fpxbankpay";
	/**
	 *各种协议
	 */
	public final static String  GETURL = hostUrl+"/api/ads.php?adtype=geturl";
	/**
	 *各种协议
	 */
	public final static String  FPXBANKPAYBACK = hostUrl+"/api/user.php?act=fpxbankpayback";
	/**
	 *付款订单票据
	 */
	public final static String  ORDER_BILL = hostUrl+"/api/order.php?act=billnote";
	/**
	 *付款订单票据
	 */
	public final static String  UPDATE = hostUrl+"/api/ads.php?adtype=update";
	/**
	 *拍图搜索
	 */
	public final static String  PAI_SEARCH = hostUrl+"/api/goods.php?act=pai_search";

	/**
	 *退货记录
	 */
	public final static String  REFUND_LIST = hostUrl+"/api/user.php?act=refund_list";

	/**
	 *订单详情里的补差价
	 */
	public final static String  ORDER_DIFFER = hostUrl+"/api/order.php?act=order_differ";
	/**
	 *付款时选择商品的费用计算
	 */
	public final static String  JS_FEE = hostUrl+"/api/order.php?act=js_fee";
	/**
	 *标记用户消息已读
	 */
	public final static String  SIGN_READ = hostUrl+"/api/user.php?act=sign_read";
	/**
	 *更换手机号码
	 */
	public final static String  UPDATE_PHONE = hostUrl+"/api/user.php?act=update_phone";
	/**
	 *确认收货
	 */
	public final static String  AFFIRM_RECEIVED = hostUrl+"/api/order.php?act=affirm_received";
	/**
	 *获取信用卡充值状态
	 */
	public final static String  getcreditstatus = hostUrl+"/api/user.php?act=getcreditstatus";
	/**
	 *APP是否单独在线
	 */
	public final static String  ISALONE = hostUrl+"/api/user.php?act=isalone";

	/**
	 *绑定手机号码
	 */
	public final static String  SET_PHONE = hostUrl+"/api/user.php?act=set_phone";
	/**
	 *获取FPX充值银行列表
	 */
	public final static String  FPXBANK_LIST = hostUrl+"/api/user.php?act=fpxbanklist";

	/**
	 *帮助中心	正式环境
	 */
	public final static String  HELP = "https://m.asia5b.com"+"/help.php?type=1";
	/**
	 *帮助中心	测试环境
	 */
	public final static String  HELP_Test = "https://47.88.220.11:81"+"/help.php?type=1";
	/**
	 *订单对应的所有票据
	 */
	public final static String  ORDER_NOTE = hostUrl+"/api/order.php?act=order_note";
	/**
	 *活动展示
	 */
	public final static String  HINT = hostUrl+"/api/goods.php?act=hint";
	/**
	 *c撤销退货
	 */
	public final static String  CANCEL_REFUND = hostUrl+"/api/user.php?act=cancel_refund";
	/**
	 *遗失直接退款
	 */
	public final static String  LOSS_RETURN = hostUrl+"/api/order.php?act=loss_return";
	/**
	 *遗失重新订购
	 */
	public final static String  LOSS_REORDER = hostUrl+"/api/order.php?act=loss_reorder";
	/**
	 *付款时重新选择信息
	 */
	public final static String  RESELECTION = hostUrl+"/api/order.php?act=reselection";
	/**
	 *联系客服
	 */
	public final static String  JS_UDESK = hostUrl+"/api/user.php?act=js_udesk ";
	/**
	 *
	 */
	public final static String  Test_Url = hostUrl+"/appapi/article/carriage";
	/**
	 * 完善资料第2
	 */
	public final static String  PERFECT_INFO = hostUrl+"/api/user.php?act=perfect_info ";

	/**
	 *
	 * 2.8.1上传文件
	 *
	 * @param appid
	 * @param userid
	 * @param signature
	 * @return
	 * @return String
	 * @throws
	 */
	public static String uploadPhoto(String appid, String userid,
			String signature) {
		return Setting.hostUrl + "/api/service/upload-photo?appid=" + appid
				+ "&userid=" + userid + "&signature=" + signature;
	}

	/**
	 * 上传图片保存的图片路径
	 */
	public static String pictureHost;
	public static final String filePath = "/sdcard/istudy/temp/";
	public static final String smsFilePath = "/sdcard/istudy/sms/";
	public static final String voiceFilePath = "/mnt/sdcard/istudy/voice/";
	public final static String pictureDirectory = "/sdcard/istudy/mic_portal/new_picture/";
	public final static String pictureDirectory_manage_userInfo = "/sdcard/istudy/mic_manage/picture/userInfo/";
	public final static String pictureDirectory_manage_smsInfo = "/sdcard/istudy/mic_manage/picture/smsInfo/";
	public final static String schoolSongdirectory = "/sdcard/istudy/mic_portal/schoolSong/";
	public final static String picturelearns = "/sdcard/istudy/learns/";
	public final static String pictureDirectory_manage_reply = "/sdcard/istudy/mic_manage/picture/reply/";
	public static final String SMSSENDKEY = "==SMSSENDKEY===9126==";
	public static final String SENDLLOCAL = "===local===";
	public static final String SENDLLOCALKEY = "SENDLLOCALKEY";
	public static final String filePathName = "temp.jpg";
	public static final String videoFileName = "temp.3pg";
	public static final String videoFileNameFormatCode = ".3pg";
	public static final String imageFileNameFormatCode = ".jpg";
	public static final String SYSCONTACT = "SYSCONTACT";
	public static final String dowloadfilePath = "/sdcard/";
	public final static String pictureName = "_user_small.jpg";
	public static String IMAGE_ROOTPATH = FileUtil.getSdPath()
			+ "/asia5b/image/";
	public static String IMAGE_ROOTPATH_2 = "/asia5b/image/";
	public static String CRASH_ROOTPATH = FileUtil.getSdPath()
			+ "/asia5b/crash/";
	public static String SHARE_IMAG_NAME ="image_test.png";


	public static String serviceItem = Setting.hostUrl
			+ "/cms/publisharticleMobile.do";

	public static String sdFilePath = "/kuaiyi/DownLoad";
}
