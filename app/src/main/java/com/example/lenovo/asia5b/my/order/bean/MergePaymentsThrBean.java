package com.example.lenovo.asia5b.my.order.bean;

/**
 * Created by lenovo on 2017/7/26.
 */

public class MergePaymentsThrBean {
    public String rec_id;
    public String order_id;
    public String goods_name;
    public String goods_attr_thumb;
    public String goods_number;
    public String market_price;
    public String goods_price;
    public String goods_attr;
    public String market_china_fee;
    public String net_weight;
    public String volume_weight;
    public String goods_price_count;
    public String weight;
}
