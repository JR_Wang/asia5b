package com.example.lenovo.asia5b.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;

/**
 * Created by lenovo on 2017/7/8.
 */

public class WritePayPassword extends BaseActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_password);
        initView();
    }

    private void initView(){
        F.id(R.id.activity_pay_password_finish).clicked(this);//下一步
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()){
            case R.id.activity_pay_password_finish://下一步
                intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                break;
        }
    }
}
