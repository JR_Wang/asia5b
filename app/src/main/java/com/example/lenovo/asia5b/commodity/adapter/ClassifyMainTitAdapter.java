package com.example.lenovo.asia5b.commodity.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.lenovo.asia5b.commodity.bean.ClassifyBean;
import com.example.lenovo.asia5b.commodity.fragment.ClassifyMainFragment;

import java.util.List;

/**
 * Created by lenovo on 2017/7/19.
 */

public class ClassifyMainTitAdapter extends FragmentStatePagerAdapter {

    private  List<ClassifyBean> lists;

    public ClassifyMainTitAdapter(FragmentManager fm, List<ClassifyBean> lists) {
        super(fm);
        this.lists =lists;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return lists.get(position).getCat_name();
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Fragment getItem(int position) {

        return ClassifyMainFragment.newInstance(position,lists.get(position));
    }
}
