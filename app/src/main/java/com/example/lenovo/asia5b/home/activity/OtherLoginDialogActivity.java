package com.example.lenovo.asia5b.home.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;

import com.example.lenovo.asia5b.ui.PublicDialog;
import com.example.lenovo.asia5b.util.SharedPreferencesUtils;
import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;

/**
 * Created by lenovo on 2017/9/11.
 */

public class OtherLoginDialogActivity extends BaseActivity {
    public  boolean isSel ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //清空数据
        SharedPreferencesUtils.clear();
        SharedPreferencesUtils.setObjectToShare(null, "user");

        PublicDialog dialog = new PublicDialog(this);
        dialog.setTitle(getResources().getString(R.string.xtts));// 设置title
        dialog.setContent(getResources().getString(R.string.zhzqtsbdl));// 设置内容
        dialog.setLeftButton(getResources().getString(R.string.qx));// 设置按钮
        dialog.setRightButton(getResources().getString(R.string.confirm));
        dialog.setLeftButtonVisible(false); // true显示 false隐藏
        dialog.setRightButtonVisible(true);// true显示 false隐藏
        dialog.setRightButtonClick(new PublicDialog.OnClickListener() {
            @Override
            public void onClick(View v) {
                isSel = true;
                try {
                    finishAll();
                    finish();
                    Intent i2 = new Intent(OtherLoginDialogActivity.this, LoginActivity.class);
                    Intent  i3 =  new Intent(OtherLoginDialogActivity.this, MainActivity.class);
                    startActivity(i3);
                    startActivity(i2);
                } catch (Exception e) {
                }
            }
        });
        dialog.getAlertDialog().setCanceledOnTouchOutside(false);
        dialog.showDialog();

    }

    @Override
    public void onPause(){
        super.onPause();
        if (!isSel) {
            finishAll();
            finish();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        //判断用户是否点击的是返回键
        if(keyCode == KeyEvent.KEYCODE_BACK){

        }
        return false;
    }
}
