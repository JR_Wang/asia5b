package com.example.lenovo.asia5b.my.order.bean;

/**
 * Created by lenovo on 2017/7/15.
 */

public class ConfirmOrderBean {
    public String rec_id;
    public String goods_name;
    public String goods_attr_thumb;
    public String goods_price;
    public String goods_number;
    public String goods_attr;
    public String china_fee;
    public String weight;
    public String sign;
    public String market_price ="";
    public String examine_price= "";
    public boolean isSelected = true;
}
