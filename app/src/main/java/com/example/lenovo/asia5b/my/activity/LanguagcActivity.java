package com.example.lenovo.asia5b.my.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.DisplayMetrics;
import android.view.View;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.home.activity.MainActivity;

import java.util.Locale;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * 语言设置
 * Created by lenovo on 2017/6/24.
 */

public class LanguagcActivity extends BaseActivity {
    private AppCompatRadioButton btn_chinese,btn_english,btn_malay;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_languagc);
        initView();
        read();
    }

    public void initView() {
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text(getResources().getString(R.string.languagc));
        F.id(public_btn_left).clicked(this);
        F.id(R.id.public_txt_righe).clicked(this).text(getResources().getString(R.string.qd));//btn_setting
        btn_chinese = (AppCompatRadioButton)findViewById(R.id.btn_chinese);
        btn_english = (AppCompatRadioButton)findViewById(R.id.btn_english);
        btn_malay = (AppCompatRadioButton)findViewById(R.id.btn_malay);
        SharedPreferences languagePre = getSharedPreferences("language_choice",
                Context.MODE_PRIVATE);
        int id =languagePre.getInt("id", 1);
        switch (id) {
            case 0:
                btn_chinese.setChecked(true);
                break;
            case 1:
                btn_english.setChecked(true);
                break;
            case 2:
                btn_malay.setChecked(true);
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                finish();
                break;
            case  R.id.public_txt_righe:
                save();
                read();
                finishAll();
                intent = new Intent(LanguagcActivity.this, MainActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }

    //需要语言马来西亚或者中国
    private void read() {
        SharedPreferences languagePre = getSharedPreferences("language_choice",
                Context.MODE_PRIVATE);
        int id =languagePre.getInt("id", 1);
        // 应用内配置语言
        Resources resources = getResources();// 获得res资源对象
        Configuration config = resources.getConfiguration();// 获得设置对象
        DisplayMetrics dm = resources.getDisplayMetrics();// 获得屏幕参数：主要是分辨率，像素等。
        switch (id) {
            case 0:
//              config.locale = Locale.getDefault(); // 系统默认语言
                config.locale = Locale.SIMPLIFIED_CHINESE; // 简体中文
                break;
            case 1:
                config.locale = new Locale("en","US");// 美国
                break;
            case 2:
                config.locale = new Locale("ms","MY"); // 马来西亚
                break;
            default:
                config.locale = Locale.getDefault();
                break;
        }
        resources.updateConfiguration(config, dm);
    }
    //保存
    private void save() {
        final SharedPreferences languagePre = LanguagcActivity.this.getSharedPreferences(
                "language_choice", LanguagcActivity.this.MODE_PRIVATE);
        int id = 0;
        if (btn_chinese.isChecked()) {
            // 简体中文
            id = 0;
        } else if (btn_english.isChecked()) {
//            //美国
            id = 1;
        } else if (btn_malay.isChecked()) {
            // 马来西亚
            id = 2;
        }
        languagePre.edit().putInt("id", id).commit();
    }
}