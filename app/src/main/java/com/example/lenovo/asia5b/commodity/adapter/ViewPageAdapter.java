package com.example.lenovo.asia5b.commodity.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.TestBean;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;

import com.example.lenovo.asia5b.ui.DownloadPicture;

import static com.wushi.lenovo.asia5b.R.id.ll_showtitle;

/**
 * Created by apple on 2017/11/27.
 * 这里自己去写一个包含图片与文字的adapter
 */

public class ViewPageAdapter extends PagerAdapter {
    private Context context;
    private List<TestBean> data;

    public ViewPageAdapter(Context context,ArrayList<TestBean> data){
        this.context = context;
        this.data = data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = null;
        ViewHolder mViewHolder;
        if (view == null) {
            mViewHolder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.test_mes, null);
            mViewHolder.ll_showtitle = (LinearLayout) view.findViewById(ll_showtitle);
            mViewHolder.public_dialog_title = (TextView) view.findViewById(R.id.public_dialog_title);
            mViewHolder.public_dialog_content = (TextView) view.findViewById(R.id.public_dialog_content);
            mViewHolder.public_dialog_content.setMovementMethod(ScrollingMovementMethod.getInstance());
            mViewHolder.public_dialog_line = (View) view.findViewById(R.id.public_dialog_line);
            mViewHolder.iv_img = (ImageView) view.findViewById(R.id.iv_img);
            view.setTag(mViewHolder);
        } else {
            mViewHolder = (ViewHolder) view.getTag();
        }
        TestBean bean = data.get(position);
        //展示文字
        if (bean.getIdentifying().equals("3")){
            //textview加载bean.getTextStr

            mViewHolder.iv_img.setVisibility(View.GONE);
            mViewHolder.ll_showtitle.setVisibility(View.VISIBLE);
            mViewHolder.public_dialog_title.setVisibility(View.VISIBLE);
            mViewHolder.public_dialog_content.setVisibility(View.VISIBLE);
            mViewHolder.public_dialog_line.setVisibility(View.VISIBLE);

            mViewHolder.public_dialog_title.setText(bean.getTitle());
            mViewHolder.public_dialog_content.setText(bean.getHint_en());
        }
        //展示图片
        else if (bean.getIdentifying().equals("1") || bean.getIdentifying().equals("0")){
            mViewHolder.iv_img.setVisibility(View.VISIBLE);
            mViewHolder.ll_showtitle.setVisibility(View.GONE);
            mViewHolder.public_dialog_title.setVisibility(View.GONE);
            mViewHolder.public_dialog_content.setVisibility(View.GONE);
            mViewHolder.public_dialog_line.setVisibility(View.GONE);

            DownloadPicture.loadNetwork(bean.getMobile_picture(),mViewHolder.iv_img);

        }
        container.addView(view);
        return view;
    }


    class ViewHolder {
        TextView public_dialog_title,public_dialog_content;
        View public_dialog_line;
        ImageView iv_img;
        LinearLayout ll_showtitle;
    }
    public void setLists(List<TestBean> data) {
        this.data = data;
    }

    public List<TestBean> getLists() {
        return data;
    }

}
