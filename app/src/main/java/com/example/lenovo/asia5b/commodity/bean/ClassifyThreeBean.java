package com.example.lenovo.asia5b.commodity.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/7/5.
 */

public class ClassifyThreeBean implements Serializable {
    private  String cat_id;
    private String cat_name;
    private String fileimg;
    private String fileico;
    private String parent_id="";

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getFileimg() {
        return fileimg;
    }

    public void setFileimg(String fileimg) {
        this.fileimg = fileimg;
    }

    public String getFileico() {
        return fileico;
    }

    public void setFileico(String fileico) {
        this.fileico = fileico;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    @Override
    public String toString() {
        return "ClassifyThreeBean{" +
                "cat_id='" + cat_id + '\'' +
                ", cat_name='" + cat_name + '\'' +
                ", fileimg='" + fileimg + '\'' +
                ", fileico='" + fileico + '\'' +
                ", parent_id='" + parent_id + '\'' +
                '}';
    }
}
