package com.example.lenovo.asia5b.my.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/7/12.
 */

public class AccountDetBean implements Serializable {
    public String id;
    public String add_time;
    public String amount;
    public String process_type;
    public String order_sn;
    public String invoice_no;
    public String rec_id;
    public String balance;
    public String bill_image;
    public String is_paid;
}
