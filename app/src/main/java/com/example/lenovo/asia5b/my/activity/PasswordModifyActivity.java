package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.my.fragment.PayPWDFragment;
import com.example.lenovo.asia5b.my.fragment.UpdatePWDFragment;

import java.util.ArrayList;
import java.util.List;

import app.BaseActivity;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/6/21.
 */

public class PasswordModifyActivity extends BaseActivity{

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private List<Fragment> fragmentList;
    private List<String> titleList;

    private boolean isForgotPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_modify);
        initViewPager();
    }

    private void initViewPager(){
        F.id(R.id.public_title_name).text(getResources().getString(R.string.password_manager));
        F.id(public_btn_left).clicked(this);
        titleList = new ArrayList<String>();
        titleList.add(getResources().getString(R.string.dlmmgl));
        titleList.add(getResources().getString(R.string.zfmmgl));

        fragmentList = new ArrayList< Fragment>();
        fragmentList.add(new UpdatePWDFragment());
        fragmentList.add(new PayPWDFragment());

        tabLayout = (TabLayout)findViewById(R.id.activity_password_modify_tl);
        viewPager = (ViewPager)findViewById(R.id.activity_password_modify_vp);
        viewPager.setOffscreenPageLimit(fragmentList.size());
        viewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return fragmentList.get(position);
            }

            @Override
            public int getCount() {
                return fragmentList.size();
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return titleList.get(position);
            }
        });
        tabLayout.setupWithViewPager(viewPager);
    }



    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                finish();
                break;
            default:
                break;
        }
    }
}

