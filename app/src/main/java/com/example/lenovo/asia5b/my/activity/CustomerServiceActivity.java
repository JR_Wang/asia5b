package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Button;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.home.fragment.ServiceAbout;
import com.example.lenovo.asia5b.home.fragment.ServiceFavorable;
import com.example.lenovo.asia5b.home.fragment.ServiceHelp;
import com.example.lenovo.asia5b.home.fragment.ServiceNew;

import app.BaseActivity;

/**
 * 客服联系
 * Created by lenovo on 2017/6/20.
 */

public class CustomerServiceActivity extends BaseActivity{

    private Button aboutButton,helpButton,favorableButton,newButton;
    private ServiceAbout serviceAbout;
    private ServiceFavorable serviceFavorable;
    private ServiceHelp serviceHelp;
    private ServiceNew serviceNew;
    private FragmentManager fragmentManager;
    private int currentIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_customer_service);
        initView();
    }

    private void initView() {
        F.id(R.id.public_title_name).text("客服联系");
        F.id(R.id.top_layout).clicked(this);

        aboutButton = (Button)findViewById(R.id.btn_about);
        helpButton = (Button)findViewById(R.id.btn_help);
        favorableButton = (Button)findViewById(R.id.btn_favorable);
        newButton = (Button)findViewById(R.id.btn_new);

        aboutButton.setOnClickListener(this);
        helpButton.setOnClickListener(this);
        favorableButton.setOnClickListener(this);
        newButton.setOnClickListener(this);

        fragmentManager = getSupportFragmentManager();
        setTabSelection(0);
    }

    @Override
    public void onClick(View v) {
        Intent intent0 = new Intent();
        System.out.print(v);
        switch (v.getId()){
            case R.id.top_layout:
                finish();
                break;
            case R.id.btn_about:
                currentIndex = 0;
                setTabSelection(0);
                break;
            case R.id.btn_help:
                currentIndex = 1;
                setTabSelection(1);
                break;
            case R.id.btn_favorable:
                currentIndex  = 2;
                setTabSelection(2);
                break;
            case R.id.btn_new:
                currentIndex = 3;
                setTabSelection(3);
                break;
            default:
                break;
        }
    }

    private void setTabSelection(int index){
        resetBtn();
        changBootUI();
        // 开启一个Fragment事务
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // 先隐藏掉所有的Fragment，以防止有多个Fragment显示在界面上的情况
        hideFragments(transaction);
        switch (index) {
            case 0:
                if (serviceAbout == null) {
                    // 如果MessageFragment为空，则创建一个并添加到界面上
                    serviceAbout = new ServiceAbout();
                    transaction.add(R.id.id_conten_f, serviceAbout);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(serviceAbout);
                }
                break;
            case 1:
                if (serviceHelp == null) {
                    // 如果MessageFragment为空，则创建一个并添加到界面上
                    serviceHelp = new ServiceHelp();
                    transaction.add(R.id.id_conten_f, serviceHelp);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(serviceHelp);
                }
                break;
            case 2:
                if (serviceFavorable == null) {
                    // 如果MessageFragment为空，则创建一个并添加到界面上
                    serviceFavorable = new ServiceFavorable();
                    transaction.add(R.id.id_conten_f, serviceFavorable);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(serviceFavorable);
                }
                break;
            case 3:
                if (serviceNew == null) {
                    // 如果MessageFragment为空，则创建一个并添加到界面上
                    serviceNew = new ServiceNew();
                    transaction.add(R.id.id_conten_f, serviceNew);
                } else {
                    // 如果MessageFragment不为空，则直接将它显示出来
                    transaction.show(serviceNew);
                }
                break;
        }
        transaction.commitAllowingStateLoss();
    }

    /**
     * 清除掉所有的选中状态。
     */
    private void resetBtn() {
        aboutButton.setTextColor(getResources().getColor(R.color.my_333));
        aboutButton.setBackgroundColor(getResources().getColor(R.color.white));

        helpButton.setTextColor(getResources().getColor(R.color.my_333));
        helpButton.setBackgroundColor(getResources().getColor(R.color.white));

        favorableButton.setTextColor(getResources().getColor(R.color.my_333));
        favorableButton.setBackgroundColor(getResources().getColor(R.color.white));

        newButton.setTextColor(getResources().getColor(R.color.my_333));
        newButton.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void changBootUI() {
        switch (currentIndex) {
            case 0:
                aboutButton.setBackgroundColor(getResources().getColor(R.color.my_999));
                aboutButton.setTextColor(getResources().getColor(R.color.white));
                break;
            case 1:
                helpButton.setBackgroundColor(getResources().getColor(R.color.my_999));
                helpButton.setTextColor(getResources().getColor(R.color.white));
                break;
            case 2:
                favorableButton.setBackgroundColor(getResources().getColor(R.color.my_999));
                favorableButton.setTextColor(getResources().getColor(R.color.white));
                break;
            case 3:
                newButton.setBackgroundColor(getResources().getColor(R.color.my_999));
                newButton.setTextColor(getResources().getColor(R.color.white));
                break;
            default:
                break;
        }
    }

    /**
     * 将所有的Fragment都置为隐藏状态。
     *
     * @param transaction
     *            用于对Fragment执行操作的事务
     */
    private void hideFragments(FragmentTransaction transaction) {
        if (serviceAbout != null) {
            transaction.hide(serviceAbout);
        }
        if (serviceHelp != null) {
            transaction.hide(serviceHelp);
        }
        if (serviceFavorable != null) {
            transaction.hide(serviceFavorable);
        }
        if (serviceNew != null) {
            transaction.hide(serviceNew);
        }

    }
}
