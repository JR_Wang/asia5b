package com.example.lenovo.asia5b.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;


/**
 * @Title: PublicDialog.java
 * @Package com.sie.mp.widget
 * @company： xuanyuan
 * @Description: 自定义公共Dialog
 * @author ： linzhan
 * @modify:
 * @date 2014-9-1
 * @version V1.0
 */
public class PublicDialog {
	private Button leftBT = null;
	private Button rightBT = null;
	private TextView titleTV = null;
	private TextView quit=null;
	private TextView contentTV = null;
	private View lineView1 = null, lineView2 = null,lineView4=null;
	private View view = null,public_dialog_line3;
	private OnClickListener mLeftOl = null;
	private OnClickListener mRightOl = null;
	private Dialog alertDialog = null;
	private LinearLayout content_layout = null,layout_bottom;
	private boolean isCustomContentView = false;

	public PublicDialog(Context context) {
		this(context, 0, 0);
	}

	/**
	 * 带标题和内容构造函数
	 * 
	 * @param context
	 * @param titleId
	 * @param contentId
	 */
	public PublicDialog(Context context, int titleId, int contentId) {
		this(context, titleId, contentId, 0, 0);
	}

	/**
	 * 带标题和内容构造函数
	 * 
	 * @param context
	 * @param title
	 * @param content
	 */
	public PublicDialog(Context context, String title, String content) {
		this(context, title, content, "", "");
	}

	/**
	 * 带标题，内容，左右按钮文字资源构造函数
	 * 
	 * @param context
	 * @param titleId
	 * @param contentId
	 * @param leftButtonId
	 * @param rightButtonId
	 */
	public PublicDialog(Context context, int titleId, int contentId,
                        int leftButtonId, int rightButtonId) {
		this(context, titleId, contentId, leftButtonId, rightButtonId, null,
				null);
	}

	/**
	 * 带标题，内容，左右按钮文字资源构造函数
	 * 
	 * @param context
	 * @param title
	 * @param content
	 * @param leftButton
	 * @param rightButton
	 */
	public PublicDialog(Context context, String title, String content,
                        String leftButton, String rightButton) {
		this(context, title, content, leftButton, rightButton, null, null);
	}

	/**
	 * 带标题，内容，左右按钮文字资源,左右按钮事件构造函数
	 * 
	 * @param context
	 * @param titleId
	 * @param contentId
	 * @param leftButtonId
	 * @param rightButtonId
	 * @param leftOl
	 * @param rightOl
	 */
	public PublicDialog(Context context, int titleId, int contentId,
                        int leftButtonId, int rightButtonId, OnClickListener leftOl,
                        OnClickListener rightOl) {
		init(context, (titleId != 0 ? context.getString(titleId) : ""),
				(contentId != 0 ? context.getString(contentId) : ""),
				(leftButtonId != 0 ? context.getString(leftButtonId) : ""),
				(rightButtonId != 0 ? context.getString(rightButtonId) : ""),
				leftOl, rightOl);
	}

	/**
	 * 带标题，内容，左右按钮文字资源,左右按钮事件构造函数
	 * 
	 * @param context
	 * @param title
	 * @param content
	 * @param leftButton
	 * @param rightButton
	 * @param leftOl
	 * @param rightOl
	 */
	public PublicDialog(Context context, String title, String content,
                        String leftButton, String rightButton, OnClickListener leftOl,
                        OnClickListener rightOl) {
		init(context, title, content, leftButton, rightButton, leftOl, rightOl);
	}

	private void init(Context context, String title, String content,
			String leftButton, String rightButton, OnClickListener leftOl,
			OnClickListener rightOl) {
		alertDialog = new Dialog(context, R.style.CustomDialogStyle);
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view = inflater.inflate(R.layout.frame_public_dialog, null);
		leftBT = (Button) view.findViewById(R.id.public_dialog_left_bt);
		layout_bottom = (LinearLayout) view.findViewById(R.id.layout_bottom);
		public_dialog_line3 = view.findViewById(R.id.public_dialog_line3);
		quit = (TextView) view.findViewById(R.id.public_dialog_exit);
		rightBT = (Button) view.findViewById(R.id.public_dialog_right_bt);
		titleTV = (TextView) view.findViewById(R.id.public_dialog_title);
		lineView2 = view.findViewById(R.id.public_dialog_line2);
		lineView4 = view.findViewById(R.id.public_dialog_line4);
		content_layout = (LinearLayout) view
				.findViewById(R.id.public_dialog_content_layout);
		if (!isCustomContentView) {
			contentTV = (TextView) view
					.findViewById(R.id.public_dialog_content);
			setContent(content.equals("") ? contentTV.getText().toString()
					: content);
		}
		lineView1 = view.findViewById(R.id.public_dialog_line);
		leftBT.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mLeftOl != null)
					mLeftOl.onClick(v);
				alertDialog.dismiss();
			}
		});
		rightBT.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (mRightOl != null)
					mRightOl.onClick(v);
				alertDialog.dismiss();
			}
		});
		quit.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				alertDialog.dismiss();
			}
		});
		// alertDialog = new AlertDialog.Builder(context).create();
		// 返回按键
		alertDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				if (mLeftOl != null)
					mLeftOl.onClick(leftBT);
			}
		});
		alertDialog.setCancelable(true);
		setTitle(title.equals("") ? titleTV.getText().toString() : title);
		setLeftButton(leftButton.equals("") ? leftBT.getText().toString()
				: leftButton);
		setRightButton(rightButton.equals("") ? rightBT.getText().toString()
				: rightButton);
		setLeftButtonClick(leftOl);
		setRightButtonClick(rightOl);
	}

	/**
	 * 显示自定对话框
	 */
	public void showDialog() {
		// alertDialog.setView(view);
		alertDialog.show();
		alertDialog.getWindow().setContentView(view);
	}

	/**
	 * 关闭自定对话框
	 */
	public void dismissDialog() {
		alertDialog.dismiss();
	}

	/**
	 * 标题
	 * 
	 * @param title
	 */
	public void setTitle(String title) {
		titleTV.setText(title);
	}

	public void setTitleFontSize(float fontsize) {
		titleTV.setTextSize(fontsize);
	}

	/**
	 * 标题
	 * 
	 * @param stringId
	 */
	public void setTitle(int stringId) {
		titleTV.setText(stringId);
	}

	/**
	 * 设置标题背景颜色
	 * 
	 * @param color
	 */
	public void setTitleBackground(int color) {
		titleTV.setBackgroundColor(color);
	}

	/**
	 * 设置标题背景颜色
	 * 
	 * @param background
	 */
	public void setTitleBackground(Drawable background) {
		titleTV.setBackgroundDrawable(background);
	}

	/**
	 * 设置标题的文字位置
	 * 
	 * @param gravity
	 */
	public void setTitleGravity(int gravity) {
		titleTV.setGravity(gravity);
	}

	/**
	 * 标题颜色
	 * 
	 * @param color
	 */
	public void setTitleColor(int color) {
		titleTV.setTextColor(color);
	}

	/**
	 * 标题颜色
	 * 
	 * @param colors
	 */
	public void setTitleColor(ColorStateList colors) {
		titleTV.setTextColor(colors);
	}

	/**
	 * 设置标题可见性
	 * 
	 * @param isVisible
	 */
	public void setTitle(boolean isVisible) {
		if (!isVisible) {
			titleTV.setVisibility(View.GONE);
			lineView1.setVisibility(View.GONE);
		} else {
			titleTV.setVisibility(View.VISIBLE);
			lineView1.setVisibility(View.VISIBLE);
		}
	}
	/**
	 * 设置底部可见性
	 *
	 * @param isVisible
	 */
	public void setBottom(boolean isVisible) {
		if (!isVisible) {
			layout_bottom.setVisibility(View.GONE);
			public_dialog_line3.setVisibility(View.GONE);
		} else {
			layout_bottom.setVisibility(View.VISIBLE);
			public_dialog_line3.setVisibility(View.VISIBLE);
		}
	}
	/**
	 * 内容
	 * 
	 * @param content
	 */
	public void setContent(View view) {
		content_layout.removeView(contentTV);
		content_layout.addView(view);
		isCustomContentView = true;
	}

	/**
	 * 内容
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		contentTV.setText(content);
	}
	public void setContentFontSize(float fontsize) {
		contentTV.setTextSize(fontsize);
	}
	public void setContent(CharSequence content) {
		contentTV.setText(content);
	}
	/**
	 *  内容颜色
	 * 
	 * @param color
	 */
	public void setContentColor(int color) {
		contentTV.setTextColor(color);
	}

	/**
	 * 内容
	 * 
	 * @param stringId
	 */
	public void setContent(int stringId) {
		contentTV.setText(stringId);
	}

	/**
	 * 左按钮
	 * 
	 * @param confirm
	 */
	public void setLeftButton(String leftButton) {
		leftBT.setText(leftButton);
	}

	/**
	 * 左按钮
	 * 
	 * @param stringId
	 */
	public void setLeftButton(int stringId) {
		leftBT.setText(stringId);
	}

	/**
	 * 左按钮文字颜色
	 * 
	 * @param colors
	 */
	public void setLeftButtonTextColor(ColorStateList colors) {
		leftBT.setTextColor(colors);
	}

	/**
	 * 左按钮文字颜色
	 * 
	 * @param color
	 */
	public void setLeftButtonTextColor(int color) {
		leftBT.setTextColor(color);
	}

	/**
	 * 修改左按钮背景
	 * 
	 * @param background
	 */
	@SuppressLint("NewApi")
	public void setLeftButtonBacground(Drawable background) {
		leftBT.setBackground(background);
	}

	/**
	 * 修改左按钮背景
	 * 
	 * @param color
	 */
	public void setLeftButtonBackground(int color) {
		leftBT.setBackgroundColor(color);
	}

	/**
	 * 左按钮可见性
	 * 
	 * @param isVisible
	 */
	public void setLeftButtonVisible(boolean isVisible) {
		if (isVisible)
			leftBT.setVisibility(View.VISIBLE);
		else
			leftBT.setVisibility(View.GONE);
	}
	public void setLeftButtonVisible(int v) {

			leftBT.setVisibility(v);

	}
	public void setToprightVisible(boolean isVisible) {
		if (isVisible)
			quit.setVisibility(View.VISIBLE);
		else
			quit.setVisibility(View.GONE);
	}
	public void setLineView4(boolean isVisible) {
		if (isVisible)
			lineView4.setVisibility(View.VISIBLE);
		else
			lineView4.setVisibility(View.GONE);
	}
	public void setToprightDrawable(int res) {

		quit.setBackgroundResource(res);
	}

	/**
	 * 右按钮
	 * 
	 * @param cancel
	 */
	public void setRightButton(String rightButton) {
		rightBT.setText(rightButton);
	}

	/**
	 * 右按钮
	 * 
	 * @param stringId
	 */
	public void setRightButton(int stringId) {
		rightBT.setText(stringId);
	}

	/**
	 * 右按钮文字颜色
	 * 
	 * @param colors
	 */
	public void setRightButtonTextColor(ColorStateList colors) {
		rightBT.setTextColor(colors);
	}

	/**
	 * 右按钮文字颜色
	 * 
	 * @param color
	 */
	public void setRightButtonTextColor(int color) {
		rightBT.setTextColor(color);
	}

	/**
	 * 修改右按钮背景
	 * 
	 * @param background
	 */
	@SuppressLint("NewApi")
	public void setRightButtonBacground(Drawable background) {
		rightBT.setBackground(background);
	}

	/**
	 * 修改右按钮背景
	 * 
	 * @param color
	 */
	public void setRightButtonBackground(int color) {
		rightBT.setBackgroundColor(color);
	}

	/**
	 * 右按钮可见性
	 * 
	 * @param isVisible
	 */
	public void setRightButtonVisible(boolean isVisible) {
		if (isVisible)
			rightBT.setVisibility(View.VISIBLE);
		else
			rightBT.setVisibility(View.GONE);
	}
	public void setRightButtonVisible(int v) {
			rightBT.setVisibility(v);
	}
	/**
	 * title可见性
	 * 
	 * @param isVisible
	 */
	public void setTitleVisible(boolean isVisible) {
		if (isVisible) {
			titleTV.setVisibility(View.VISIBLE);
			lineView1.setVisibility(View.VISIBLE);
		} else {
			titleTV.setVisibility(View.GONE);
			lineView1.setVisibility(View.GONE);
			content_layout.setGravity(Gravity.CENTER);
		}
	}

	/**
	 * 点击左按钮
	 * 
	 * @param ol
	 */
	public void setLeftButtonClick(OnClickListener ol) {
		mLeftOl = ol;
	}

	/**
	 * 点击右按钮
	 * 
	 * @param ol
	 */
	public void setRightButtonClick(OnClickListener ol) {
		mRightOl = ol;
	}

	/**
	 * 设置第一天分割线隐藏与否
	 * 
	 * @param isVisible
	 */
	public void setLineOneVisible(boolean isVisible) {
		if (isVisible) {
			lineView1.setVisibility(View.VISIBLE);
		} else {
			lineView1.setVisibility(View.GONE);
		}
	}

	/**
	 * 设置第二条分割线隐藏与否
	 * 
	 * @param isVisible
	 */
	public void setLinetowVisible(boolean isVisible) {
		if (isVisible) {
			lineView2.setVisibility(View.VISIBLE);
		} else {
			lineView2.setVisibility(View.GONE);
		}
	}

	
	public interface OnClickListener {
		void onClick(View v);
	}

	public Dialog getAlertDialog() {
		return alertDialog;
	}

}
