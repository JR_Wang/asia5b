package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.TestBean;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 2017/11/11.
 */

public class TestAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<TestBean> lists ;
    private  Context context;

    public TestAdapter(Context context){

        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<TestBean>();
        this.context = context;

    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int i) {
        return lists.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        TestAdapter.ViewHolder holder = null;
        if (view == null) {

            holder=new TestAdapter.ViewHolder();

            view = mInflater.inflate(R.layout.test_mes, null);

            view.setTag(holder);

        }else {
            holder = (TestAdapter.ViewHolder)view.getTag();
        }
        TestBean bean = lists.get(i);



        return view;

    }

    private  class  ViewHolder {
        TextView txt_first,txt_second,txt_third,txt_four;
    }

    public void setLists(List<TestBean> lists) {
        this.lists = lists;
    }
}
