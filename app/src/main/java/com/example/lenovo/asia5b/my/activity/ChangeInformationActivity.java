package com.example.lenovo.asia5b.my.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.wushi.lenovo.asia5b.R;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * Created by lenovo on 2017/6/21.
 */

public class ChangeInformationActivity  extends BaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_information);
        initView();
    }
    public void initView() {
        F.id(R.id.btn_change_information).clicked(this);
        F.id(R.id.public_title_name).text("我的资料");
        F.id(public_btn_left).clicked(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case R.id.btn_change_information:
                U.Toast(this,"修改成功");
                finish();
                break;
            case public_btn_left:
                finish();
                break;
            default:
                break;
        }
    }

}
