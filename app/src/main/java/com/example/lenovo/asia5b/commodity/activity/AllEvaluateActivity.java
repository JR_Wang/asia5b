package com.example.lenovo.asia5b.commodity.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.wushi.lenovo.asia5b.R;
import com.example.lenovo.asia5b.commodity.fragment.AllEvaluateFragment;
import com.example.lenovo.asia5b.commodity.fragment.AllEvaluateTwoFragment;

import java.util.ArrayList;
import java.util.List;

import app.BaseActivity;
import app.FragAdapter;

import static com.wushi.lenovo.asia5b.R.id.public_btn_left;

/**
 * 查看全部评价
 * Created by lenovo on 2017/7/1.
 */

public class AllEvaluateActivity  extends BaseActivity {
    private String goods_id="";
    private AllEvaluateFragment fragment;
    private AllEvaluateTwoFragment fragmentTwo;

    private ViewPager vp_content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_evaluate);
        initView();
    }

    public void initView() {
        if (getIntent().hasExtra("goods_id")) {
            goods_id = getIntent().getStringExtra("goods_id");
        }
        F.id(R.id.public_title_name).text(getResources().getString(R.string.all_evaluate));
        F.id(public_btn_left).clicked(this);
        vp_content =  (ViewPager)findViewById(R.id.vp_content);
        List<Fragment> fragments=new ArrayList<Fragment>();
        fragment = new AllEvaluateFragment();
        fragment.setGooodId(goods_id);
        fragmentTwo = new AllEvaluateTwoFragment();
        fragmentTwo.setGooodId(goods_id);
        fragments.add(fragment);
        fragments.add(fragmentTwo);
        FragAdapter adapter = new FragAdapter(getSupportFragmentManager(), fragments);
        vp_content.setOnPageChangeListener(new MyPagerOnPageChangeListener());
        vp_content.setAdapter(adapter);
        setTabSelection(0);
        F.id(R.id.btn_fpx).clicked(this);
        F.id(R.id.btn_credit_card).clicked(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;
        switch (v.getId()) {
            case public_btn_left:
                finish();
                break;
            case R.id.btn_fpx:
                setTabSelection(0);
                break;
            case  R.id.btn_credit_card:
                setTabSelection(1);
                break;
            default:
                break;
        }
    }

    private void resetBtn() {
        F.id(R.id.btn_fpx).backgroundColor( ContextCompat.getColor(AllEvaluateActivity.this,R.color.white));
        F.id(R.id.btn_fpx).textColor( ContextCompat.getColor(AllEvaluateActivity.this,R.color.my_333));

        F.id(R.id.btn_credit_card).backgroundColor( ContextCompat.getColor(AllEvaluateActivity.this,R.color.white));
        F.id(R.id.btn_credit_card).textColor( ContextCompat.getColor(AllEvaluateActivity.this,R.color.my_333));

    }


    public void setTabSelection(int index) {
        resetBtn();
        vp_content.setCurrentItem(index);
        switch (index) {
            case 0:
                F.id(R.id.btn_fpx).backgroundColor(ContextCompat.getColor(AllEvaluateActivity.this, R.color.my_999));
                F.id(R.id.btn_fpx).textColor(ContextCompat.getColor(AllEvaluateActivity.this, R.color.white));
                break;
            case 1:
                F.id(R.id.btn_credit_card).backgroundColor(ContextCompat.getColor(AllEvaluateActivity.this, R.color.my_999));
                F.id(R.id.btn_credit_card).textColor(ContextCompat.getColor(AllEvaluateActivity.this, R.color.white));
                break;
        }
    }


    // 回调方法，从第二个页面回来的时候会执行这个方法
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch (resultCode) {
            case 0:
                if (data == null) {
                    return;
                }
                fragment.onActivityResult(requestCode,resultCode,data);
                fragmentTwo.onActivityResult(requestCode,resultCode,data);
                break;
            default:
                break;
        }
    }

    /**
     * ViewPager的PageChangeListener(页面改变的监听器)
     */
    private class MyPagerOnPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int arg0) {
        }
        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }
        /**
         */
        @Override
        public void onPageSelected(int position) {
            setTabSelection(position);
        }
    }
}
