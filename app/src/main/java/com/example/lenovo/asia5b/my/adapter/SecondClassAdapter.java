package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.wushi.lenovo.asia5b.R;

import java.util.List;

/**
 * Created by apple on 2017/12/8.
 */

public class SecondClassAdapter extends BaseAdapter {

    private Context context;
    private List<String> list;
    //            选中二级联动的时候记录一级联动的位置
    private int mFirstPosition = 0;
    //        一级联动 现在被选中的位置
    private int mFirstCurrentPosition;

    public SecondClassAdapter(Context context, List<String > list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list == null ? 0 : list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.activity_nametem, null);
            holder = new ViewHolder();

            holder.txt_name = (TextView) view.findViewById(R.id.txt_name);
            holder.txt_name.setTextColor(Color.BLACK);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //选中和没选中时，设置不同的颜色
        if (position == selectedPosition) {
//            view.setBackgroundColor(Color.WHITE);
//            holder.txt_name.setTextColor(context.getResources().getColor(R.color.gray));
            holder.txt_name.setBackgroundColor(context.getResources().getColor(R.color.gray));
        } else {
            holder.txt_name.setBackgroundColor(context.getResources().getColor(R.color.white));
//            view.setBackgroundColor(context.getResources().getColor(R.color.gray));

        }
//        holder.txt_name.setText(list.get(position).getName());
        holder.txt_name.setText(list.get(position));

        return view;
    }

    private int selectedPosition = 0;

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public void setFirstPosition(int firstPosition) {
        mFirstPosition = firstPosition;
    }

    public void setFirstCurrentPosition(int firstCurrentPosition) {
        mFirstCurrentPosition = firstCurrentPosition;
    }

    private class ViewHolder {
        TextView txt_name;
    }
}
