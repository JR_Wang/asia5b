package com.example.lenovo.asia5b.my.order.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/7/11.
 */

public class ParcelListBean implements Serializable{

    public String rec_id;
    public String goods_id;
    public String goods_name;
    public String goods_attr_thumb;
    public String goods_price;
    public String goods_number;
    public String orderStatu;
    public String price_difference;
    public String is_give_difference;
    public String fillStatu;
    public String cancle_type="";
    public String is_comment= "";
    public String loss_num="";
    public String loss_state="";
    public String express_code="";

}
