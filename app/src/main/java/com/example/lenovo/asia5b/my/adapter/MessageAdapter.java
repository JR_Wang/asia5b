package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.MessageBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lenovo on 2017/6/21.
 */

public class MessageAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<MessageBean> lists ;
    private  Context context;


    public MessageAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<MessageBean>();
        this.context = context;
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_message, null);
            holder.txt_title = (TextView)convertView.findViewById(R.id.txt_title);
            holder.txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txt_content = (TextView)convertView.findViewById(R.id.txt_content);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        MessageBean bean = lists.get(position);

//        holder.txt_title.setText(bean.title);
        holder.txt_time.setText(DateUtils.timedate(bean.add_time));
        if (bean.is_view.equals("0")) {
            holder.txt_content.setTextColor(context.getResources().getColor(R.color.my_333));
        } else {
            holder.txt_content.setTextColor(context.getResources().getColor(R.color.my_999));
        }
        holder.txt_content.setText(bean.content);
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_title,txt_time,txt_content;
    }

    public void setLists(List<MessageBean> lists) {
        this.lists = lists;
    }

    public List<MessageBean> getLists() {
        return lists;
    }
}
