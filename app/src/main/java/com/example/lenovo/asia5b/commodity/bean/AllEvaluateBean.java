package com.example.lenovo.asia5b.commodity.bean;

import java.io.Serializable;

/**
 * Created by lenovo on 2017/8/7.
 */

public class AllEvaluateBean implements Serializable {
    public String comment_id;
    public String user_id;
    public String add_time;
    public String user_name;
    public String avatar;
    public String goods_attr;
    public String content;
    public String cmmt_img;
    public String id_value;
    public String ishave;
    public String cmmt_nums;
}
