package com.example.lenovo.asia5b.my.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.lenovo.asia5b.my.bean.RefundBean;
import com.example.lenovo.asia5b.util.DateUtils;
import com.example.lenovo.asia5b.util.DensityUtil;
import com.wushi.lenovo.asia5b.R;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by lenovo on 2017/8/21.
 */

public class RefundAdapter extends BaseAdapter

{

    private LayoutInflater mInflater;
    private List<RefundBean> lists = null;
    private Context context;


    public RefundAdapter(Context context){
        this.mInflater = LayoutInflater.from(context);
        lists = new ArrayList<RefundBean>();
        this.context = context;
    }
    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public Object getItem(int arg0) {
        return lists.get(arg0);
    }

    @Override
    public long getItemId(int arg0) {
        return arg0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;
        if (convertView == null) {

            holder=new ViewHolder();

            convertView = mInflater.inflate(R.layout.item_record_top_up, null);
            holder.txt_time = (TextView)convertView.findViewById(R.id.txt_time);
            holder.txt_amount = (TextView)convertView.findViewById(R.id.txt_amount);
            holder.txt_process_type = (TextView)convertView.findViewById(R.id.txt_process_type);
            holder.ll_pjh = (LinearLayout)convertView.findViewById(R.id.ll_pjh);
            holder.ll_state = (LinearLayout)convertView.findViewById(R.id.ll_state);
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageView);
            holder.txt_jdh = (TextView)convertView.findViewById(R.id.txt_jdh);
            holder.txt_invoice_no = (TextView)convertView.findViewById(R.id.txt_invoice_no);
            holder.txt_state = (TextView)convertView.findViewById(R.id.txt_state);
            convertView.setTag(holder);

        }else {
            holder = (ViewHolder)convertView.getTag();
        }
        RefundBean bean = lists.get(position);
        holder.txt_amount.setText("RM"+bean.msg_price);
        String time = DateUtils.timedate(bean.msg_time);
        holder.txt_time.setText(time.replace(" ","\n"));
        holder.ll_state.setPadding(0, DensityUtil.dip2px(context,8),0,0);
        holder.txt_state.setText(context.getResources().getString(R.string.state));
        String orderStatu = "";
        if(bean.orderStatu.equals("9")) {
            orderStatu = context.getResources().getString(R.string.ds);
        } else if (bean.orderStatu.equals("15")) {
            orderStatu = context.getResources().getString(R.string.ytg);
        } else if (bean.orderStatu.equals("17")){
            orderStatu = context.getResources().getString(R.string.btg);
        }
        holder.txt_process_type.setText(orderStatu);
        holder.txt_invoice_no.setText(bean.order_sn);
        holder.txt_jdh.setText(context.getResources().getString(R.string.order_number));
        holder.imageView.setImageResource(R.drawable.icon_mx_record);
        return convertView;
    }
    private  class  ViewHolder {
        TextView txt_time,txt_amount,txt_process_type,txt_jdh,txt_invoice_no,txt_state;
        LinearLayout ll_pjh,ll_state;
        ImageView imageView;
    }

    public void  setLists (List<RefundBean> lists) {
        this.lists = lists;
    }

    public List<RefundBean>  getLists () {
        return  lists;
    }

}