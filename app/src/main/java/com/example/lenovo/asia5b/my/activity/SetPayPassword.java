package com.example.lenovo.asia5b.my.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.lenovo.asia5b.home.bean.UserBean;
import com.example.lenovo.asia5b.ui.LoadingDalog;
import com.example.lenovo.asia5b.util.ICallBack;
import com.example.lenovo.asia5b.util.PayPassword;
import com.example.lenovo.asia5b.util.SMSCode;
import com.example.lenovo.asia5b.util.SharedPreferencesUtils;
import com.wushi.lenovo.asia5b.R;

import org.json.JSONObject;

import app.BaseActivity;
import fay.frame.ui.U;

import static com.example.lenovo.asia5b.util.SharedPreferencesUtils.getUserBean;


/**
 * Created by admin on 2017/7/14.
 */

public class SetPayPassword extends BaseActivity implements ICallBack {
    private LoadingDalog loadingDalog;

    private TimeCount timeCount;
    private long timeOut;
    private UserBean user;
    private String phone;

    private Button update_pay_password_code;
    private EditText activity_set_pay_password_one,activity_set_pay_password_two,activity_set_pay_password_code;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_pay_password);
        initView();
    }

    private void initView(){
        loadingDalog = new LoadingDalog(this);

        user = (UserBean) SharedPreferencesUtils.getObjectFromShare("user");

        F.id(R.id.public_title_name).text(getString(R.string.setPayPassword));
        F.id(R.id.public_btn_left).clicked(this);
        F.id(R.id.update_pay_password_code).clicked(this);//验证码
        F.id(R.id.activity_set_pay_password_finish).clicked(this);//完成

        update_pay_password_code = findViewById(R.id.update_pay_password_code);//验证码
        activity_set_pay_password_one = findViewById(R.id.activity_set_pay_password_one);//支付密码
        activity_set_pay_password_two = findViewById(R.id.activity_set_pay_password_two);//确认支付密码
        activity_set_pay_password_code = findViewById(R.id.activity_set_pay_password_code);//验证码
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.public_btn_left://返回
                finish();
                break;
            case R.id.update_pay_password_code://验证码
                loadingDalog.show();
                phone = user.mobile_phone;
                SMSCode.getCode(this,phone);
                break;
            case R.id.activity_set_pay_password_finish://完成
                finishSet();
                break;
        }
    }

    /**
     * 完成设置
     */
    private void finishSet(){
        try{
            String new_pass = activity_set_pay_password_one.getText().toString().trim();
            String counter_sign_new_pass = activity_set_pay_password_two.getText().toString().trim();
            String code = activity_set_pay_password_code.getText().toString().trim();
            if(!TextUtils.isEmpty(new_pass)){
                if(new_pass.length() == 6){
                    if(!TextUtils.isEmpty(counter_sign_new_pass)){
                        if(TextUtils.equals(new_pass,counter_sign_new_pass)){
                            if(!TextUtils.isEmpty(code)){
                                loadingDalog.show();
                                PayPassword.savePayPassword(this,null,new_pass,code);
                            }else{
                                U.Toast(this,getString(R.string.yzmbnwk));
                            }
                        }else{
                            U.Toast(this,getString(R.string.lcsrdmmbxd));
                        }
                    }else{
                        U.Toast(this,getString(R.string.confirm_the_payment_password_cannot_be_empty));
                    }
                }else {
                    U.Toast(this,getString(R.string.pay_the_password_length_must_be_6_digits));
                }
            }else{
                U.Toast(this,getString(R.string.pay_the_pa_sword_cannot_be_empty));
            }
        }catch (Exception e){
            Log.e("error：",e.getMessage());
        }
    }

    private Handler handler2 = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            JSONObject result= null;
            try {
                result = new JSONObject(msg.obj.toString());
            } catch (Exception e) {
            }
            int action = msg.what;
            try{
                if(result != null){
                    JSONObject json = (JSONObject) result;
                    if (action == 0) {
                        String msgText = json.getString("MsgText");
                        if(TextUtils.equals("true",msgText)){
                            timeCount = new TimeCount(60000, 1000);
                            timeCount.start();
                            U.Toast(SetPayPassword.this,getString(R.string.send_the_successful));
                        }else if(TextUtils.equals("false",msgText)){
                            U.Toast(SetPayPassword.this,json.getString("State"));
                        }
                        loadingDalog.dismiss();
                    }if (action == 1) {
                        int state = json.getInt("State");
                        if(state == 0){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.cg));
                            finish();
                            UserBean bean = getUserBean();
                            if (null != bean) {
                                bean.paypass = "1";
                                SharedPreferencesUtils.setObjectToShare(bean,"user");
                            }
                        }else if(state == 1){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.qmsb));
                        }else if(state == 2){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.cscw));
                        }else if(state == 3){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.mmcdbxdy6));
                        }else if(state == 4){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.wdl));
                        }else if(state == 5){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.sjyzmcw));
                        }else if(state == 6){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.mmcw));
                        }else if(state == 7){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.xjmmbnyz));
                        }else if(state == 15){
                            U.Toast(SetPayPassword.this,getResources().getString(R.string.yzmsx));
                        }
                        loadingDalog.dismiss();
                    }
                } else {
//                U.Toast(this,getString(R.string.connect_server_fail)+result);
                }
            }catch (Exception e){
                Log.e("设置支付密码error：",e.getMessage());
            }

        }
    };

    @Override
    public void logicFinish(Object result, int action) {
        Message msg = new Message();
        msg.what = action;
        msg.obj = result.toString();
        handler2.sendMessage(msg);
    }

    /**
     * 获取验证码倒计时
     */
    class TimeCount extends CountDownTimer {

        public TimeCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            if(SetPayPassword.this.isFinishing()){
                return;
            }
            update_pay_password_code.setClickable(false);
            timeOut = millisUntilFinished / 1000;
            update_pay_password_code.setText(getString(R.string.message_has_been_sent)+"(" + timeOut + ")");
        }

        @Override
        public void onFinish() {
            update_pay_password_code.setText(R.string.to_obtain_the_verification_code);
            update_pay_password_code.setClickable(true);
        }
    }
}
