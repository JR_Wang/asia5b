package app;

import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.util.Log;

import com.example.lenovo.asia5b.home.bean.MSGbean;
import com.google.gson.Gson;
import com.mob.MobApplication;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.umeng.message.IUmengRegisterCallback;
import com.umeng.message.PushAgent;
import com.umeng.message.UmengMessageHandler;
import com.umeng.message.entity.UMessage;
import com.umeng.socialize.Config;
import com.umeng.socialize.PlatformConfig;
import com.umeng.socialize.UMShareAPI;
import com.umeng.socialize.bean.SHARE_MEDIA;
import com.umeng.socialize.common.QueuedWork;
import com.wushi.lenovo.asia5b.R;

import java.util.Locale;

import me.leolin.shortcutbadger.ShortcutBadger;

import static com.umeng.socialize.PlatformConfig.configs;

public class MyApplication extends MobApplication {

	private  static MyApplication myApplication;
	private static Context context;
	public static String  registrationId="";
	public static String  goodsId = "";
//	public int bagerNumber = 0;
	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();

		//友盟推送==================================================================
		PushAgent mPushAgent = PushAgent.getInstance(this);
		//注册推送服务，每次调用register方法都会回调该接口
		mPushAgent.register(new IUmengRegisterCallback() {

			@Override
			public void onSuccess(String deviceToken) {
				//注册成功会返回device token
				Log.e("deviceToken", "====" + deviceToken);
				registrationId=deviceToken;
			}

			@Override
			public void onFailure(String s, String s1) {
				Log.e("deviceToken2","友盟注册失败"+"s====="+s+",,,"+"s1===="+s1);
			}
		});
		mPushAgent.setDisplayNotificationNumber(1);
		UmengMessageHandler messageHandler = new UmengMessageHandler() {
			@Override
			public Notification getNotification(Context context, UMessage msg) {
				Log.e("接收到的推送消息",msg.getRaw().toString());
//				bagerNumber = SharedPreferencesUtils.getInt("bagerNumber", 0) + 1;
//				SharedPreferencesUtils.setInt("bagerNumber", bagerNumber);
				Gson gson = new Gson();
				MSGbean msGbean = gson.fromJson(msg.getRaw().toString(),MSGbean.class);
				Log.e("接收到的推送消息条数",msGbean.getExtra().getMsgcount()+"");
				ShortcutBadger.applyCount(context,msGbean.getExtra().getMsgcount());
//				Intent intent = new Intent(MyApplication.this, BadgeIntentService.class);
//				intent.putExtra("bagerNumber",msGbean.getExtra().getMsgcount());
//				startService(intent);
				return super.getNotification(context, msg);
			}
		};
		mPushAgent.setMessageHandler(messageHandler);

		//友盟推送==================================================================

		DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder() //
				.showImageForEmptyUri(R.mipmap.ic_launcher) //
				.showImageOnFail(R.mipmap.ic_launcher) //
				.cacheInMemory(true) //
				.cacheOnDisk(true) //
				.build();//
		ImageLoaderConfiguration config = new ImageLoaderConfiguration//
		.Builder(getApplicationContext())//
				.defaultDisplayImageOptions(defaultOptions)//
				.discCacheSize(50 * 1024 * 1024)//
				.discCacheFileCount(100)// 缓存一百张图片
				.writeDebugLogs()//
				.build();//
		ImageLoader.getInstance().init(config);

		myApplication = this;

		//开启debug模式，方便定位错误，具体错误检查方式可以查看http://dev.umeng.com/social/android/quick-integration的报错必看，正式发布，请关闭该模式
		Config.DEBUG = false;
		QueuedWork.isUseThreadPool = false;
		//开始注册友盟
		UMShareAPI.get(this);
		read();
	}

	public static  MyApplication getInstance() {
		return myApplication;
	}

	//各个平台的配置，建议放在全局Application或者程序入口
	{
		PlatformConfig.setWeixin("wxeb9cbbdcf7846c37", "e44a92a991add73d13a60ca269e57a81");
//		//豆瓣RENREN平台目前只能在服务器端配置
		PlatformConfig.setSinaWeibo("2576394452", "52520bc9a034cadeeef75e483974a764","http://www.asia5b.com");
//		PlatformConfig.setYixin("yxc0614e80c9304c11b0391514d09f13bf");
		PlatformConfig.setQQZone("1106351732", "aFpd22jenPdsSgRs");
//		PlatformConfig.setTwitter("3aIN7fuF685MuZ7jtXkQxalyi", "MK6FEYG63eWcpDFgRYw4w9puJhzDl0tyuqWjZ3M7XJuuG7mMbO");
//		PlatformConfig.setAlipay("2015111700822536");
//		PlatformConfig.setLaiwang("laiwangd497e70d4", "d497e70d4c3e4efeab1381476bac4c5e");
//		PlatformConfig.setPinterest("1439206");
//		PlatformConfig.setKakao("e4f60e065048eb031e235c806b31c70f");
//		PlatformConfig.setDing("dingoalmlnohc0wggfedpk");
//		PlatformConfig.setVKontakte("5764965","5My6SNliAaLxEm3Lyd9J");
//		PlatformConfig.setDropbox("oz8v5apet3arcdy","h7p2pjbzkkxt02a");
//		PlatformConfig.setYnote("9c82bf470cba7bd2f1819b0ee26f86c6ce670e9b");
		setFacebook("1475748862539872","e5bcf3784d25df7340279fdd3a446cc2");
		setInstagram("041e728afb3948a0b840491f1e3a99c2","4376cc0779f94516b1f3150d504ee0af ");
	}

	public static void setFacebook(String id, String secret) {
		PlatformConfig.CustomPlatform laiwang = (PlatformConfig.CustomPlatform)configs.get(SHARE_MEDIA.FACEBOOK);
		laiwang.appId = id;
		laiwang.appkey = secret;
		PlatformConfig.CustomPlatform laiwang2 = (PlatformConfig.CustomPlatform)configs.get(SHARE_MEDIA.FACEBOOK_MESSAGER);
		laiwang2.appId = id;
		laiwang2.appkey = secret;
	}
	public static void setInstagram(String id, String secret) {
		PlatformConfig.CustomPlatform laiwang = (PlatformConfig.CustomPlatform)configs.get(SHARE_MEDIA.INSTAGRAM);
		laiwang.appId = id;
		laiwang.appkey = secret;
	}


	//需要语言马来西亚或者中国
	private void read() {
		SharedPreferences languagePre = getSharedPreferences("language_choice",
				Context.MODE_PRIVATE);
		int id =languagePre.getInt("id", 1);
		// 应用内配置语言
		Resources resources = getResources();// 获得res资源对象
		Configuration config = resources.getConfiguration();// 获得设置对象
		DisplayMetrics dm = resources.getDisplayMetrics();// 获得屏幕参数：主要是分辨率，像素等。
		switch (id) {
			case 0:
//              config.locale = Locale.getDefault(); // 系统默认语言
				config.locale = Locale.SIMPLIFIED_CHINESE; // 简体中文
				break;
			case 1:
				config.locale = new Locale("en","US");// 美国
				break;
			case 2:
				config.locale = new Locale("ms","MY"); // 马来西亚
				break;
			default:
				config.locale = Locale.getDefault();
				break;
		}
		resources.updateConfiguration(config, dm);
	}


	//返回
	public static Context getContextObject(){
		return context;
	}
}
