package app;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.NumberKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import fay.frame.fast.F;

/**
 * Created by lenovo on 2017/6/26.
 */

public class BaseFragment extends Fragment implements
        View.OnClickListener {
    public F F;

    public BaseFragment() {
        F = new F(getActivity());
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
//        S.init(getActivity());
        return null;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }

    public  class EditChangedListener implements TextWatcher {
        private EditText edit_other_amount;
        public EditChangedListener(EditText edit_other_amount) {
            this.edit_other_amount = edit_other_amount;
        }
        private CharSequence temp;//监听前的文本
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        @Override
        public void afterTextChanged(Editable s) {
            String  aa = edit_other_amount.getText().toString();
            if (!TextUtils.isEmpty(aa)) {
                if (aa.substring(0,1).equals(".")) {
                    edit_other_amount.setText("");
                }
                else if (aa.indexOf(".") != -1) {
                    edit_other_amount.setKeyListener(new NumberKeyListener() {
                        @Override
                        protected char[] getAcceptedChars() {
                            return new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
                        }
                        @Override
                        public int getInputType() {
                            return android.text.InputType.TYPE_CLASS_PHONE;
                        }
                    });
                    if (aa.length()>3) {
                        String[] bb = aa.split("\\.");
                        if (bb.length > 1) {
                            if (!TextUtils.isEmpty(bb[1])) {
                                if (bb[1].length() > 2) {
                                    edit_other_amount.setText(aa.substring(0, aa.length() - 1));
                                    edit_other_amount.setSelection( edit_other_amount.getText().toString().length());
                                }
                            }
                        }
                    }
                } else if  (aa.substring(0,1).equals("0")&& aa.length()==1) {
                    edit_other_amount.setKeyListener(new NumberKeyListener() {
                        @Override
                        protected char[] getAcceptedChars() {
                            return new char[]{'.'};
                        }
                        @Override
                        public int getInputType() {
                            return android.text.InputType.TYPE_CLASS_PHONE;
                        }
                    });
                }
                else {
                    edit_other_amount.setKeyListener(new NumberKeyListener() {
                        @Override
                        protected char[] getAcceptedChars() {
                            return new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0','.'};
                        }
                        @Override
                        public int getInputType() {
                            return android.text.InputType.TYPE_CLASS_PHONE;
                        }
                    });
                }
            }  else {
                edit_other_amount.setKeyListener(new NumberKeyListener() {
                    @Override
                    protected char[] getAcceptedChars() {
                        return new char[]{'1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.'};
                    }

                    @Override
                    public int getInputType() {
                        return android.text.InputType.TYPE_CLASS_PHONE;
                    }
                });
            }
        }
    };

}
